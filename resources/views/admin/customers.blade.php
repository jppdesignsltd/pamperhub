{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Admin | Customers')

@section('stylesheets')

@endsection

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-users mr-10"></i> Customers
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $customers->count() === 1 ? 'is' : 'are' }} <span class="badge">{{ $customers->count() }}</span> registered customer{{ $customers->count() === 1 ? '' : 's' }}.</h5>

        @if(Entrust::hasRole('super-admin'))
            <a href="{{ route('admin.users.create') }}" class="btn btn-sm btn-dark-gray">add new customer</a>
        @endif
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            @if($customers->count() === 0)
                <div class="row">
                    <div class="col-sm-12">
                        <p>You don't have any registered customers</p>
                    </div>
                </div>

            @else
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-body panel-default bg-white npadding">
                            <div class="table-responsive">
                                <table id="customers-index" class="table table-bordered">
                                    <thead class="">
                                    <th style="width:220px;">Full Name</th>
                                    <th>D.O.B</th>
                                    <th>Contact No.</th>
                                    <th>City</th>
                                    <th>No. of Appointments</th>
                                    <th>Status</th>
                                    @if(Entrust::hasRole('super-admin'))
                                        <th></th>
                                    @endif
                                    </thead>

                                    <tbody>
                                    @foreach($customersPaginate as $customer)

                                        <tr id="customer-{{ $customer->id }}">
                                            <td>
                                                <a href="{{ route('admin.customers.show', $customer->id) }}">{{ $customer->user->firstname . ' ' . $customer->user->lastname }}</a>
                                            </td>
                                            <td>
                                                @if($customer->dob === '' || $customer->dob === null)

                                                @else
                                                    {{ date('jS, F Y', strtotime($customer->dob)) }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ $customer->contact_number }}
                                            </td>
                                            <td>
                                                {{ $customer->city }}
                                            </td>
                                            <td>
                                                {{ $customer->appointments->count() }}
                                            </td>
                                            <td class="status-label text-center">
                                                @if( $customer->active === 0 )
                                                    <label class="label label-danger {{-- nbradius --}}">in-active</label>
                                                @else
                                                    <label class="label label-success {{-- nbradius --}}">active</label>
                                                @endif
                                            </td>
                                            @if(Entrust::hasRole('super-admin') && $customer->active !== 0)
                                                <td class="text-center">
                                                    <a role="button" data-toggle="collapse" href="#deactivateCustomer-{{ $customer->id }}" aria-expanded="false" aria-controls="deactivateCustomer-{{ $customer->id }}" data-parent="#customers-index" class="text-danger small">[deactivate]</a>
                                                </td>
                                            @elseif(Entrust::hasRole('super-admin') && $customer->active === 0)
                                                <td class="text-center">
                                                    <a role="button" data-toggle="collapse" href="#activateCustomer-{{ $customer->id }}" aria-expanded="false" aria-controls="activateCustomer-{{ $customer->id }}" data-parent="#customers-index" class="text-success small">[activate]</a>
                                                </td>
                                            @endif

                                        </tr>

                                        <tr class="bg-danger npadding">
                                            <td class="npadding" colspan="{{ Entrust::hasRole('super-admin') ? '7' : '6' }}">
                                                <div id="deactivateCustomer-{{ $customer->id }}" class="collapse">
                                                    <div class="pl-10 pt-20 pb-20 pr-10 text-center">
                                                        <p>
                                                            Are you sure you would like to deactivate this users account?
                                                        </p>
                                                        {{ Form::open(['url' => ['customer' . '?page=deactivate', $customer->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}
                                                        {{ Form::text('active', 0, ['class' => 'hidden']) }}
                                                        {{ Form::button('<i class="fa fa-user mr-10"></i> deactivate', ['class' => 'btn btn btn-danger btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="bg-success npadding">
                                            <td class="npadding" colspan="{{ Entrust::hasRole('super-admin') ? '7' : '6' }}">
                                                <div id="activateCustomer-{{ $customer->id }}" class="collapse">
                                                    <div class="pl-10 pt-20 pb-20 pr-10 text-center">
                                                        <p>
                                                            Enable this customer again?
                                                        </p>
                                                        {{ Form::open(['url' => ['customer' . '?page=deactivate', $customer->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}
                                                        {{ Form::text('active', 1, ['class' => 'hidden']) }}
                                                        {{ Form::button('<i class="fa fa-user mr-10"></i> activate', ['class' => 'btn btn btn-success btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="pl-15 pull-left pt-5">
                        Page <strong>{!! $customersPaginate->currentPage() !!}</strong> of {!! $customersPaginate->lastPage() !!}
                    </div>

                    <div class="pull-right pr-15">
                        {!! $customersPaginate->links() !!}
                    </div>
                </div>
            @endif

                <div class="row mt-30">
                    <div class="col-sm-12">
                        <a href="{{ route('admin') }}" class="btn btn-md btn-beige">back</a>
                    </div>
                </div>
        </div>
    </section>


@stop

@section('scripts')

@stop
