@extends('layouts.admin')

@section('title', 'Admin Appointments')

@section('stylesheets')

@endsection

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-calendar mr-10"></i> Appointments
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $appointments->count() === 1 ? 'is' : 'are' }} <span
                    class="badge">{{ $appointments->count() }}</span>
            appointment{{ $appointments->count() === 1 ? '' : 's' }}.</h5>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            @if($appointments->count() === 0)

                <div class="row">
                    <div class="col-sm-12">
                        <p>There are no appointments listed.</p>
                    </div>
                </div>

            @else

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-body panel-default bg-white npadding">
                            <div class="table-responsive">
                                <table id="customers-index" class="table table-bordered">
                                    <thead class="">
                                    <th>
                                        Order No.
                                    </th>
                                    <th>Custom No.</th>
                                    <th>Supplier No.</th>
                                    <th>Duration</th>
                                    <th>Total Price</th>
                                    <th>Date</th>
                                    <th>Time of Day</th>
                                    <th>Paid</th>
                                    <th>Confirmed</th>
                                    <th>Canceled</th>
                                    </thead>

                                    <tbody>
                                    @foreach($appointmentsPaginate as $appointment)

                                        <tr id="appointment-{{ $appointment->id }}">
                                            <td>
                                                <a href="{{ route('admin.appointment.edit', $appointment->id) }}">
                                                    <strong>
                                                        #ORD-0{{ $appointment->id }}
                                                    </strong>
                                                </a>
                                            </td>
                                            <td>
                                                <strong>
                                                    #00{{ $appointment->customer->id }}

                                                </strong>
                                            </td>
                                            <td>
                                                <strong>
                                                    #SUP-0{{ $appointment->supplier->id }}

                                                </strong>
                                            </td>
                                            <td>
                                                {{ $appointment->duration > 1 ? "$appointment->duration hrs" : "$appointment->duration hr" }}
                                            </td>
                                            <td>
                                                &pound;{{ $appointment->price }}
                                            </td>
                                            <td>
                                                {{ date('d/m/Y' , strtotime($appointment->date)) }}
                                            </td>
                                            <td>
                                                {{ $appointment->time_of_day }}
                                            </td>
                                            <td class="text-center">
                                                @if($appointment->payment_received === 0)
                                                    <span class="label label-xs label-danger">No</span>
                                                @else
                                                    <span class="label label-xs label-success">Yes</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($appointment->supplier_confirmed === 0)
                                                    <span class="label label-xs label-danger">No</span>
                                                @else
                                                    <span class="label label-xs label-success">Yes</span>
                                                @endif
                                            </td>

                                            <td class="text-center">
                                                @if($appointment->canceled === 0)
                                                    <span class="label label-xs label-danger">No</span>
                                                @else
                                                    <span class="label label-xs label-success">Yes</span>
                                                @endif
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="pl-15 pull-left pt-5">
                        Page <strong>{!! $appointmentsPaginate->currentPage() !!}</strong>
                        of {!! $appointmentsPaginate->lastPage() !!}
                    </div>

                    <div class="pull-right pr-15">
                        {!! $appointmentsPaginate->links() !!}
                    </div>
                </div>

            @endif

            <div class="row mt-20">
                <div class="col-sm-12">
                    <a href="{{ route('admin') }}" class="btn btn-md btn-beige">back</a>
                </div>
            </div>
        </div>
    </section>

@stop

@section('scripts')

@stop
