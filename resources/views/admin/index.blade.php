{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Admin')

@section('stylesheets')

@endsection

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-th mr-10"></i> Dashboard
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-default bg-light-blue">
                        <div class="panel-body">
                            <h3 class="nmargin">Recent Suppliers
                                @if($activeSuppliers->count() > 0)
                                    <span class="badge">{{ $activeSuppliers->count() }}</span>
                                @endif
                            </h3>

                            <hr class="mt-10 mb-10">

                            @if($suppliers->count() === 0)
                                <p class="nmbottom">
                                    You do not have any suppliers registered
                                </p>
                            @else
                                <div class="clearfix"></div>
                                <ul class="pl-20 mt-5 nmbottom">
                                    @foreach($suppliersPaginate as $supplier)

                                        <li class="medium mb-5">
                                            <a class="text-white" href="{{ route('admin.suppliers.show', $supplier->id) }}">
                                                <span class="visible-lg-inline visible-md-inline">{{ $supplier->company }}</span>
                                                <span class="visible-sm-inline visible-xs-inline">{{ substr(strip_tags($supplier->company), 0, 15) }}{{ strlen(strip_tags($supplier->company)) > 15 ? "..." : "" }}</span>
                                            </a>


                                            @if($supplier->active === 0)
                                                <span class="pull-right label label-danger" style="font-size: 90%;">Not Active</span>
                                            @else
                                                <span class="pull-right label label-success" style="font-size: 90%;">Active</span>
                                            @endif

                                        </li>

                                    @endforeach
                                </ul>
                            @endif
                        </div>

                        <div class="panel-footer pt-15 pb-15 bg-dark-blue nborder nb-radius">
                            <h6 class="nmbottom xs-text-center">
                                You have <a href="{{ route('admin.suppliers') }}" class=""><span class="badge">{{ $inActiveSuppliers->count() }}</span></a> in-activated suppliers.

                                <span class="pull-right">
                                    <a href="{{ route('admin.suppliers') }}" class="text-white">(View all suppliers)</a>
                                </span>

                                <div class="clearfix"></div>
                            </h6>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-default bg-teal">
                        <div class="panel-body">
                            <h3 class="">Recent Customers
                                @if($customers->count() > 0)
                                    <span class="badge">{{ $customers->count() }}</span>
                                @endif
                            </h3>

                            <hr class="mt-10 mb-10">

                            @if($customers->count() === 0)
                                <p class="nmbottom">
                                    You do not have any customers registered
                                </p>
                            @else
                                <div class="clearfix"></div>
                                <ul class="pl-20 mt-5 nmbottom">
                                    @foreach($customersPaginate as $customer)

                                        <li class="medium mb-5">
                                            <a class="text-white" href="{{ route('admin.customers.show', $customer->id) }}">
                                                <span class="visible-lg-inline visible-md-inline">
                                                    {{ $customer->user->firstname . ' ' . $customer->user->lastname }}
                                                </span>
                                                <span class="visible-sm-inline visible-xs-inline">
                                                    {{ substr(strip_tags($customer->user->firstname . ' ' . $customer->user->lastname), 0, 15) }}{{ strlen(strip_tags($customer->user->firstname . ' ' . $customer->user->lastname)) > 15 ? "..." : "" }}
                                                </span>
                                            </a>

                                            @if($customer->active === 0)
                                                <span class="pull-right label label-danger" style="font-size: 90%;">Not Active</span>
                                            @else
                                                <span class="pull-right label label-success" style="font-size: 90%;">Active</span>
                                            @endif
                                        </li>

                                    @endforeach
                                </ul>
                            @endif
                        </div>

                        <div class="panel-footer pt-15 pb-15 bg-dark-teal nborder nb-radius">
                            <h6 class="nmbottom text-right xs-text-center">
                                <a href="{{ route('admin.suppliers') }}" class="text-white">(View all customers)</a>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-10">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading pt-20 pb-20">
                            <h3 class="text-dark-gray nmbottom">Recently Added Services</h3>
                        </div>
                        <div class="panel-body text-dark-gray npadding" style="background-color: #fcfcfc;">

                            @if($services->count() === 0)
                                <p class="nmbottom pl-15 mt-10 mb-10">
                                    There are no listed services.
                                </p>
                            @else
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table id="services-index" class="table pl-10 nmbottom">
                                        <thead class="bg-light-gray nb-radius">
                                            <th class="nbleft" style="width:200px;">Title</th>
                                            <th style="width:250px;">Company</th>
                                            <th style="width:100px;">Service</th>
                                            <th>Price p/h</th>
                                            <th>Sale</th>
                                            <th>Sale Price</th>
                                            <th class="nbright" style="width:80px;">Status</th>
                                        </thead>

                                        <tbody class="">
                                        @foreach($servicesPaginate as $service)

                                            <tr id="service-{{ $service->id }}">
                                                <td class="nbleft">
                                                    <a href="{{ route('service.show', $service->id) }}"><span class="">{{ $service->title }}</span></a>
                                                </td>
                                                <td>
                                                    {{ $service->supplier->company }}
                                                </td>
                                                <td>
                                                    <span class="hidden">{!! $treat = $treatments->where('id', $service->service_type)->first() !!}</span>
                                                    {{ $treat->name }}
                                                </td>
                                                <td class="slug">
                                                    <strong>&pound;{{ $service->price }}</strong>
                                                </td>
                                                <td class="promotion-label">
                                                    @if( $service->promotion === 0 )
                                                        <label class="label label-danger {{-- nbradius --}}">Off</label>
                                                    @else
                                                        <label class="label label-success {{-- nbradius --}}">on</label>
                                                    @endif
                                                </td>
                                                <td class="promotion_price">
                                                    {{ $service->promotion_price !== null ? "&pound;$service->promotion_price" : 'N/A' }}
                                                </td>
                                                <td class="status-label nbright">
                                                    @if( $service->status == 'draft' )
                                                        <label class="label label-danger {{-- nbradius --}}">{{ $service->status }}</label>
                                                    @else
                                                        <label class="label label-success {{-- nbradius --}}">{{ $service->status }}</label>
                                                    @endif
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif

                        </div>
                        <div class="panel-footer pt-15 pb-15 bg-light-gray nborder nb-radius">
                            <h6 class="nmbottom text-right xs-text-center">
                                <a href="{{ route('admin.services') }}" class="text-dark-gray">(View all services)</a>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop

@section('scripts')

@stop
