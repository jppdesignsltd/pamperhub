@extends('layouts.admin')

@section('title', 'Admin | Users')

@section('stylesheets')

@endsection

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-users mr-10"></i> Users
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $users->count() === 1 ? 'is' : 'are' }} <span class="badge">{{ $users->count() }}</span> user{{ $users->count() === 1 ? '' : 's' }}.</h5>

        <a href="{{ route('admin.users.create') }}" class="btn btn-sm btn-dark-gray">add new user</a>

        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            @if($users->count() === 0)
                <div class="row">
                    <div class="col-sm-12">
                        <p>You don't have any registered customers</p>
                    </div>
                </div>

            @else
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-body panel-default bg-white npadding">
                            <div class="table-responsive">
                                <table id="users-index" class="table table-bordered">
                                    <thead class="">
                                    <th style="width:220px;">Full Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    @if(Entrust::hasRole('super-admin'))
                                        <th style="width: 100px"></th>
                                    @endif
                                    </thead>

                                    <tbody>
                                        @foreach($usersPaginate as $user)

                                        <tr id="customer-{{ $user->id }}">
                                            <td>
                                                <a href="{{ route('admin.users.edit', $user->id) }}">{{ $user->firstname . ' ' . $user->lastname }}</a>
                                            </td>
                                            <td>
                                                {{ $user->email }}
                                            </td>
                                            <td>
                                                @if(!empty($user->roles))
                                                    @foreach($user->roles as $role)
                                                        <label for="label label-success">{{ $role->display_name }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            @if(Entrust::hasRole('super-admin'))
                                            <td class="text-center">
                                                <a href="{{ route('admin.users.edit', $user->id) }}" class="medium text-dark-gray">[edit]</a>
                                            </td>
                                            @endif

                                        </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="pl-15 pull-left pt-5">
                        Page <strong>{!! $usersPaginate->currentPage() !!}</strong> of {!! $usersPaginate->lastPage() !!}
                    </div>

                    <div class="pull-right pr-15">
                        {!! $usersPaginate->links() !!}
                    </div>
                </div>
            @endif

                <div class="row mt-30">
                    <div class="col-sm-12">
                        <a href="{{ route('admin') }}" class="btn btn-md btn-beige">back</a>
                    </div>
                </div>
        </div>
    </section>


@stop

@section('scripts')

@stop
