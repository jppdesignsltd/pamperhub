{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'View Supplier')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {!! Html::style('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
@stop

@section('header-scripts')
    {!! Html::script('bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-cube mr-10"></i> View Supplier
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>

    </div>

    <div id="action_panel" class="bg-beige">
        <h3>{{ $supplier->company }}
            <span class="ml-20 label label-{{ $supplier->active === 1 ? 'success' : 'danger' }}">{{ $supplier->active === 1 ? 'Active' : 'Inactive' }} Supplier</span>
            <a href="{{ route('admin.suppliers.edit', $supplier->id) }}" class="btn btn-md btn-dark-gray">edit</a>
        </h3>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Basic Information</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>First Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $supplier->user->firstname }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Last Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $supplier->user->lastname }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>D.O.B</label>
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ date('jS F, Y', strtotime($supplier->dob)) }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('contact_number', 'Contact Number') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->contact_number }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('company', 'Company') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->company }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('website', 'Website') }}
                                    </div>
                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->website }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default bg-light-gray">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Contact Information</h3>
                        </div>
                        <div class="panel-body npleft npright">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address1', 'Address Line 1') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->address1 }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address2', 'Address Line 2') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->address2 }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('city', 'City') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->city }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('postal_code', 'Post Code') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->postal_code }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="{{ route('admin.suppliers') }}" class="btn btn-md btn-beige mt-10">back</a>
                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Supplier Experience / Availability</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('years_exp', 'Years Exp.') }}
                                    </div>
                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->years_exp }} year{{ $supplier->years_exp > 1 ? 's' : '' }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('treatments', 'Services') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        @foreach($supplier->treatments as $treatment)
                                            <span class="label bg-blue text-dark-gray" style="font-size: 80%;">{{ $treatment->name }}</span>
                                        @endforeach
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('days', 'Availability') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        @foreach($supplier->days as $day)
                                            <span class="label bg-beige text-dark-gray" style="font-size: 80%;">{{ $day->name }}</span>
                                        @endforeach
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('opening_time', 'Day', ['class' => '']) }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->opening_time }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('closing_time', 'Closing Time', ['class' => '']) }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        {{ $supplier->closing_time }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Work Eligibility</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('work_eligibility', 'Work Eligibility', ['class' => 'npadding']) }}
                                    </div>
                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                        @if($supplier->work_eligibility === 1)
                                            Yes
                                        @else
                                            No
                                        @endif
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee1_name', 'Referee 1 Name') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">

                                        {{ $supplier->referee1_name }}
                                        </span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee1_contact_number', 'Referee 1 No.') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">

                                        {{ $supplier->referee1_contact_number }}
                                        </span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee1_email', 'Referee 1 Email') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">

                                        {{ $supplier->referee1_email }}
                                        </span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee2_name', '') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">

                                        {{ $supplier->referee2_name }}
                                        </span>

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee2_contact_number', 'Referee 2 No.') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">

                                        {{ $supplier->referee2_contact_number }}
                                        </span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee2_email', 'Referee 2 Email')}}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">

                                        {{ $supplier->referee2_email }}
                                        </span>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@stop

@section('scripts')

@endsection
