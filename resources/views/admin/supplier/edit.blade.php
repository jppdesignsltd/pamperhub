@extends('layouts.admin')

@section('title', 'Edit Supplier')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {!! Html::style('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
@stop

@section('header-scripts')
    {!! Html::script('bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-cube mr-10"></i> Edit Supplier
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>

    </div>

    <div id="action_panel" class="bg-beige">
        <h3>{{ $supplier->company }}</h3>
        @if($supplier->active === 1)
            {{ Form::open(['url' => ['supplier' . '?page=deactivate', $supplier->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}
            {{ Form::text('active', 0, ['class' => 'hidden']) }}
            {{ Form::button('<i class="fa fa-user mr-10"></i> deactivate user', ['class' => 'btn btn btn-danger btn-sm', 'role' => 'button', 'type' => 'submit']) }}

            {!! Form::close() !!}
        @else
            {{ Form::open(['url' => ['supplier' . '?page=deactivate', $supplier->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}
            {{ Form::text('active', 1, ['class' => 'hidden']) }}
            {{ Form::button('<i class="fa fa-user mr-10"></i> activate user', ['class' => 'btn btn btn-success btn-sm', 'role' => 'button', 'type' => 'submit']) }}

            {!! Form::close() !!}
        @endif
    </div>

    <section>
        <div class="container-fluid">

            {{--<div class="row mb-20">--}}
                {{--<div class="col-sm-12">--}}
                    {{----}}
                {{--</div>--}}
            {{--</div>--}}


            {{ Form::model($supplier, ['route' => ['supplier.update', $supplier->id, 'page=admin'], 'method' => 'PUT', 'data-parsley-validate' => '', 'id' => 'supplierBasicProfile']) }}
            <div class="row">

                <div class="col-sm-6">

                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Basic Information</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>First Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                    <span class="form-control disabled">
                                        {{ $supplier->user->firstname }}
                                    </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Last Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                    <span class="form-control disabled">
                                        {{ $supplier->user->lastname }}
                                    </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>D.O.B</label>
                                    </div>

                                    <div class="col-sm-8">
                                    <span class="form-control disabled">
                                        {{ date('jS F, Y', strtotime($supplier->dob)) }}
                                    </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('contact_number', 'Contact Number') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('contact_number', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('company', 'Company') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('company', null,
                                        ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('website', 'Website') }}
                                    </div>
                                    <div class="col-sm-8">
                                        {{ Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Website (optional)']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Supplier Experience / Availability</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('years_exp', 'Years Exp.') }}
                                    </div>
                                    <div class="col-sm-8">
                                        {{ Form::number('years_exp', null, ['class' => 'form-control', 'placeholder' => 'Years Experience', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('treatments', 'Services') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::select('treatments[]', $treatment, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple', 'id' => 'treatments', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('days', 'Availability') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::select('days[]', $day, null, ['class' => 'form-control select2-multi', 'id' => 'days', 'multiple' => 'multiple', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('opening_time', 'Opening Time', ['class' => '']) }}
                                    </div>

                                    <div class="col-sm-8">
                                        <div class='input-group date' id='opening_time'>

                                            {{ Form::text('opening_time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Day']) }}
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>

                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#opening_time').datetimepicker({
                                                        format: 'LT'
                                                    });
                                                });
                                            </script>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('closing_time', 'Closing Time', ['class' => '']) }}
                                    </div>

                                    <div class="col-sm-8">
                                        <div class='input-group date' id='closing_time'>

                                            {{ Form::text('closing_time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Closing Time']) }}
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>

                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#closing_time').datetimepicker({
                                                        format: 'LT'
                                                    });
                                                });
                                            </script>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <a href="{{ route('admin.suppliers') }}" class="btn btn-md btn-beige mt-10">back</a>

                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default bg-light-gray">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Contact Information</h3>
                        </div>
                        <div class="panel-body npleft npright">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address1', 'Address Line 1') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('address1', $supplier->address1, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address2', 'Address Line 2') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('address2', $supplier->address2, ['class' => 'form-control', 'placeholder' => 'optional']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('city', 'City') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('city', $supplier->city, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('postal_code', 'Post Code') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('postal_code', $supplier->postal_code, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Work Eligibility</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('work_eligibility', 'Work Eligibility', ['class' => 'npadding']) }}
                                    </div>
                                    <div class="col-sm-8">
                                        Yes {{ Form::radio('work_eligibility', '1', true) }}
                                        <span class="ml-20">
                                            No {{ Form::radio('work_eligibility', '0') }}
                                        </span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee1_name', 'Referee 1 Name') }}
                                    </div>

                                    <div class="col-sm-8">

                                        {{ Form::text('referee1_name', null, ['class' => 'form-control', 'placeholder' => 'Referee 1 Name', 'required' => '']) }}

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee1_contact_number', 'Referee 1 No.') }}
                                    </div>

                                    <div class="col-sm-8">

                                        {{ Form::text('referee1_contact_number', null, ['class' => 'form-control', 'placeholder' => 'Referee 1 Number', 'required' => '']) }}

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee1_email', 'Referee 1 Email') }}
                                    </div>

                                    <div class="col-sm-8">

                                        {{ Form::text('referee1_email', null, ['class' => 'form-control', 'placeholder' => 'Referee 1 Email', 'required' => '']) }}

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee2_name', '') }}
                                    </div>

                                    <div class="col-sm-8">

                                        {{ Form::text('referee2_name', null, ['class' => 'form-control', 'placeholder' => 'Referee 2 Name', 'required' => '']) }}

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee2_contact_number', 'Referee 2 No.') }}
                                    </div>

                                    <div class="col-sm-8">

                                        {{ Form::text('referee2_contact_number', null, ['class' => 'form-control', 'placeholder' => 'Referee 2 Number', 'required' => '']) }}

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="col-sm-4">
                                        {{ Form::label('referee2_email', 'Referee 2 Email')}}
                                    </div>

                                    <div class="col-sm-8">

                                        {{ Form::text('referee2_email', null, ['class' => 'form-control', 'placeholder' => 'Referee 2 Email', 'required' => '']) }}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <p class="text-right">
                    {{ Form::submit('update supplier info', ['class'=>'btn btn-md btn-blue']) }}
                    <div class="clearfix"></div>
                    </p>

                </div>

            </div>
        </div>
    </section>
    {{ Form::close() }}


@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}

    <script type="text/javascript">
        $("#treatments").select2({
            width: '100%',
            placeholder: 'Please select your services'
        });

        $("#days").select2({
            width: '100%',
            placeholder: 'Please select your availability'
        });

        $("#treatments").select2().val( {!! json_encode($supplier->treatments()->allRelatedIds()) !!} ).trigger('change');
        $("#days").select2().val( {!! json_encode($supplier->days()->allRelatedIds()) !!} ).trigger('change');
    </script>
@endsection
