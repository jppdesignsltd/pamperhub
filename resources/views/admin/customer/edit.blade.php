@extends('layouts.admin')

@section('title', 'Edit Customer')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
@stop

@section('header-scripts')
    {!! Html::script('bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-user mr-10"></i> Edit Customer
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>

    </div>

    {{ Form::model($customer, ['route' => ['customer.update', $customer->id, 'page=admin'], 'method' => 'PUT', 'data-parsley-validate' => '', 'id' => 'supplierBasicProfile']) }}

    <div id="action_panel" class="bg-beige">
        <h3><span class="label label-{{ $customer->active === 1 ? 'success' : 'danger' }}">{{ $customer->active === 1 ? 'Active' : 'Inactive' }} User</span></h3>
        {{ Form::submit('update listing', ['class'=>'btn btn-md btn-dark-gray']) }}
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Basic Information</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>First Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                    <span class="form-control disabled">
                                        {{ $customer->user->firstname }}
                                    </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Last Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                    <span class="form-control disabled">
                                        {{ $customer->user->lastname }}
                                    </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('dob', 'Date of Birth') }}
                                    </div>
                                    <div class="col-sm-8">
                                        <div class='input-group date' id='dob'>
                                            {{ Form::text('dob', null, ['class' => 'form-control', 'required' => '']) }}

                                            <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function () {
                                            $('#dob').datetimepicker({
                                                viewMode: 'years',
                                                format: 'YYYY/MM/DD'
                                            });
                                        });
                                    </script>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('contact_number', 'Contact Number') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('contact_number', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>




                    <a href="{{ route('admin.customers') }}" class="btn btn-md btn-beige mt-10">back</a>

                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default bg-light-gray">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Contact Information</h3>
                        </div>
                        <div class="panel-body npleft npright">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address1', 'Address Line 1') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('address1', $customer->address1, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address2', 'Address Line 2') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('address2', $customer->address2, ['class' => 'form-control', 'placeholder' => 'optional']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('city', 'City') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('city', $customer->city, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('postal_code', 'Post Code') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::text('postal_code', $customer->postal_code, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {{ Form::close() }}


@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
@endsection
