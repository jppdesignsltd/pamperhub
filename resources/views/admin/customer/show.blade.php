{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'View Customer')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {!! Html::style('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
@stop

@section('header-scripts')
    {!! Html::script('bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-cube mr-10"></i> View Supplier
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>

    </div>

    <div id="action_panel" class="bg-beige">
        <h3>
            <span class="label label-{{ $customer->active === 1 ? 'success' : 'danger' }}">{{ $customer->active === 1 ? 'Active' : 'Inactive' }} Customer</span>
            <a href="{{ route('admin.customers.edit', $customer->id) }}" class="btn btn-md btn-dark-gray">edit</a>
        </h3>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Basic Information</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>First Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $customer->user->firstname }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Last Name</label>
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $customer->user->lastname }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('dob', 'Date of Birth') }}
                                    </div>
                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            @if($customer->dob === '' || $customer->dob === null)

                                            @else
                                                {{ date('jS, F Y', strtotime($customer->dob)) }}
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('contact_number', 'Contact Number') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $customer->contact_number }}
                                        </span>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 npadding">
                            <a href="{{ route('admin.customers') }}" class="btn btn-md btn-beige">back</a>
                        </div>
                    </div>

                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default bg-light-gray">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Contact Information</h3>
                        </div>
                        <div class="panel-body npleft npright">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address1', 'Address Line 1') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $customer->address1 }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('address2', 'Address Line 2') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $customer->address2 }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('city', 'City') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $customer->city }}
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('postal_code', 'Post Code') }}
                                    </div>

                                    <div class="col-sm-8">
                                        <span class="form-control disabled">
                                            {{ $customer->postal_code }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 npadding text-right">
                            @if($customer->active === 1)
                                {{ Form::open(['url' => ['customer' . '?page=deactivate', $customer->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'class' => '']) }}
                                {{ Form::text('active', 0, ['class' => 'hidden']) }}
                                {{ Form::button('<i class="fa fa-user mr-10"></i> deactivate customer', ['class' => 'btn btn btn-danger btn-md', 'role' => 'button', 'type' => 'submit']) }}

                                {!! Form::close() !!}
                            @else
                                {{ Form::open(['url' => ['customer' . '?page=deactivate', $customer->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'class' => '']) }}
                                {{ Form::text('active', 1, ['class' => 'hidden']) }}
                                {{ Form::button('<i class="fa fa-user mr-10"></i> activate customer', ['class' => 'btn btn-success btn-md', 'role' => 'button', 'type' => 'submit']) }}

                                {!! Form::close() !!}
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@stop

@section('scripts')

@endsection
