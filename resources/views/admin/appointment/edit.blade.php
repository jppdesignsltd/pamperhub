@extends('layouts.admin')

@section('title', 'Edit Appointment')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    {!! Html::style('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
@endsection

@section('header-scripts')
    {!! Html::script('bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
@endsection

@section('heading', 'Edit Appointment')

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-calendar-o mr-10"></i> Edit Appointment
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>

    </div>

    <section class="">
        <div class="container-fluid">
            <div class="row">

                {{ Form::model($appointment, ['route' => ['booking.update', $appointment->id . '?m=admin'], 'method' => 'PUT', 'id' => 'editAppointment', 'data-parsley-validate' => '']) }}

                <div class="col-lg-12 col-md-12 col-sm-12">

                    <div class="row">

                        <div class="col-sm-12">
                            <div class="clearfix"></div>

                            @if($appointment->canceled === 0)

                            <p class="mb-20">
                                You can only change the time of day of any appointment. Unless the day is fully booked.

                                <a href="{{ route('admin.appointments') }}" class="btn btn-md btn-beige pull-right ml-20">
                                    back
                                </a>

                                <a data-toggle="modal" data-target="#appointment-edit-modal"
                                   class="btn btn-blue btn-md pull-right">
                                    save

                                </a>


                            </p>

                            @endif

                            <div class="clearfix"></div>
                        </div>

                    </div>

                    <div class="row mt-30">
                        <div class="col-md-6 {{--pl-30 pr-30--}}">
                            <div class="service_post nb-radius" style="height: auto !important;">

                                <div class="service_post-container nb-radius" style="overflow: visible !important;">
                                    <div class="service_post-header">
                                        <img
                                                @if($appointment->service->cover_image !== '')
                                                src="{{ asset('uploads/services/' . $appointment->service->cover_image) }}"
                                                alt=""
                                                @else
                                                src="{{ asset('uploads/services/bg-' . strtolower($appointment->service->treatment->name) . '.jpg') }}"
                                                onerror="this.src='{{ asset('dist/img/default-service-bg.png') }}'"
                                                alt=""
                                                @endif

                                        >
                                        <div class="image-overlay"></div>
                                        <h3>
                                            {{ $appointment->service->supplier->company }}
                                            {{ Form::text('company', $appointment->service->supplier->company, ['class' => 'hidden', 'readonly' => '']) }}
                                            {{ Form::text('service_title', $appointment->service->title, ['class' => 'hidden', 'readonly' => '']) }}
                                            {{ Form::number('service_id', $appointment->service->id, ['class' => 'hidden', 'readonly' => '']) }}
                                            <div class="clearfix"></div>
                                            <span class="small">
                                        {{ $appointment->service->supplier->city }}
                                    </span>
                                        </h3>

                                        {{--<p class="availability">--}}
                                        {{--<span class="clearfix"></span>--}}
                                        {{--<span class="text-bold mr-5 small">Open:</span>--}}
                                        {{--@foreach($appointment->service->supplier->days as $dayName)--}}
                                        {{--<span class="label label-xs bg-white text-dark-gray">{{ $dayName->name }}</span>--}}
                                        {{--@endforeach--}}
                                        {{--</p>--}}

                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="service_post-body pb-20">
                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="service" class="nptop">
                                                        Service Type:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p class="nmbottom">
                                                        {{ $appointment->treatment->name }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="service" class="nptop">
                                                        Price (p/h):
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p class="nmbottom">&pound;{{ $appointment->service->price }}</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="duration" class="pt-5">
                                                        Duration:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">

                                                    <p class="nmbottom pt-5">
                                                        {{ $appointment->duration }}{{ $appointment->duration === '1' ? 'hr' : 'hrs' }}
                                                    </p>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="date" class="">
                                                        Date:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p id="date_placeholder" class="nmbottom pt-5">
                                                        {{ date('jS, F Y', strtotime($appointment->date)) }}

                                                        {{--<a onclick="revealInputAppointment(date_placeholder, date)">--}}
                                                        {{--<i class="fa fa-pencil ml-10"></i>--}}
                                                        {{--</a>--}}
                                                    </p>

                                                    <div id="date" class='input-group date hidden'>

                                                        {{ Form::text('date', null, ['class' => 'form-control bg-white', 'disabled' => '', 'placeholder' => 'Choose Date']) }}

                                                        <span class="input-group-addon nb-radius">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>

                                                    </div>

                                                    <script type="text/javascript">
                                                        $(function () {

                                                            var today = new Date();

                                                            var year = today.getFullYear();
                                                            var month = today.getMonth();
                                                            var date = today.getDate();
                                                            var dateArray = [];
                                                            var deleteArray = [];
                                                            var startDate;
                                                            var endDate;
                                                            var dateMove;
                                                            var disableHoliday;

                                                            for (var j = 0; j < 30; j++) {
                                                                var day = new Date(year, month, date + j);

                                                                dateArray.push(day);
                                                            }

//                                                            console.log(dateArray);

                                                            var supplierDays = [];
                                                            var disabledDays = [0, 1, 2, 3, 4, 5, 6];

                                                            @foreach($appointment->service->supplier->days as $day)
                                                                supplierDays.push({{ $day->time_ref }});
                                                            @endforeach

                                                                    @if($holidays)
                                                                    @foreach($holidays as $holidayDate)
                                                                startDate = "{{ $holidayDate->date_from }}";
                                                            endDate = "{{ $holidayDate->date_to }}";
                                                            dateMove = new Date(startDate);


                                                            while (startDate < endDate) {
                                                                startDate = dateMove.toISOString().slice(0, 10);
                                                                disableHoliday = new Date(startDate).setHours(0, 0, 0, 0);
                                                                deleteArray.push(disableHoliday);
                                                                dateMove.setDate(dateMove.getDate() + 1);
                                                            }
                                                                    @endforeach

                                                            var l = deleteArray.length;
                                                            while (l--) {

                                                                var dh = deleteArray.indexOf(deleteArray[l]);

                                                                if (dh !== -1) {
                                                                    console.log('Delete input');
                                                                    dateArray.splice(dh, 1);
                                                                }
                                                            }

                                                            @endif

                                                            //                                                            console.log(dateArray);
                                                            //                                                            console.log(deleteArray);

                                                            for (var i = supplierDays.length - 1; i >= 0; i--)
                                                                disabledDays.splice(disabledDays.indexOf(supplierDays[i]), 1);

                                                            $('#date').datetimepicker({
                                                                daysOfWeekDisabled: disabledDays,
                                                                useCurrent: false,
                                                                enabledDates: dateArray,
                                                                viewMode: 'days',
                                                                format: 'DD/MM/YYYY'
                                                            });
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="time" class="pt-10">
                                                        Time:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6 ">
                                                    <p class="nmbottom {{ $appointment->canceled === 1 ? '' : 'hidden' }} pt-10">
                                                        Appointment canceled
                                                    </p>

                                                    <p id="time_placeholder" class="nmbottom pt-5 text-capitalize {{ !$appointment->time ? 'hidden' : '' }}">
                                                        @if($appointment->time)
                                                            {{ date('h:i A', strtotime($appointment->time)) }}

                                                            <a onclick="revealInputAppointment(time_placeholder, time)">
                                                                <i class="fa fa-pencil ml-10"></i>
                                                            </a>
                                                        @endif
                                                    </p>

                                                    <div class='input-group date {{ !$appointment->time ? '' : 'hidden' }} {{ $appointment->canceled === 1 ? 'hidden' : '' }}'
                                                         id='time'>

                                                        @if($appointment->time)
                                                            {{ Form::text('time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Opening Time', 'id' => 'time_input', 'disabled' => '']) }}
                                                        @else
                                                            {{ Form::text('time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Opening Time', 'id' => 'time_input']) }}
                                                        @endif


                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>

                                                        <script type="text/javascript">
                                                            $(function () {
                                                                var timeArray = [];



                                                                @if(count($tod) > 0)
                                                                    console.log('{{ $appointment->time_of_day }}');

                                                                    @if($appointment->time_of_day === 'morning')
                                                                        timeArray = [7, 8, 9, 10, 11, 12];
                                                                    @elseif($appointment->time_of_day === 'afternoon')
                                                                        timeArray = [13, 14, 15, 16, 17, 18];
                                                                    @elseif($appointment->time_of_day === 'evening')
                                                                        timeArray = [19, 20, 21, 22];
                                                                @endif

                                                                @else

//                                                                    timeArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]

                                                                @endif

                                                                console.log(timeArray);

                                                                $('#time').datetimepicker({
                                                                    format: 'LT',
                                                                    enabledHours: timeArray,
                                                                    widgetPositioning: {
                                                                        horizontal: 'left',
                                                                        vertical: 'auto'
                                                                    }
                                                                });
                                                            });
                                                        </script>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group nmbottom">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="time_of_day" class="pt-5">
                                                        Time of Day:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p id="time_of_day_placeholder"
                                                       class="nmbottom pt-5 text-capitalize">
                                                        {{ $appointment->time_of_day }}

                                                        @if(!count($tod) >= 3)
                                                            <a onclick="revealInputAppointment(time_of_day_placeholder, time_of_day)">
                                                                <i class="fa fa-pencil ml-10"></i>
                                                            </a>
                                                        @endif
                                                    </p>

                                                    <select name="time_of_day" id="time_of_day"
                                                            class="form-control select2-standard hidden"
                                                            disabled>
                                                        <option value="" disabled selected>Please Choose</option>
                                                        @if(!in_array("morning", $tod))
                                                            <option value="morning">Morning</option>
                                                        @endif

                                                        @if(!in_array("afternoon", $tod))
                                                            <option value="afternoon">Afternoon</option>
                                                        @endif

                                                        @if(!in_array("evening", $tod))
                                                            <option value="evening">Evening</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-6">

                            @if($appointment->canceled === 1)

                                <div class="alert alert-warning mb-70 text-center">
                                    <p class="">
                                        Was this appointment canceled incorrectly?
                                    </p>

                                    <a data-toggle="modal" data-target="#appointment-remove-cancel-modal"
                                       class="btn btn-warning btn-md mt-10">
                                        Restore Appointment
                                    </a>
                                </div>

                            @else

                                <div class="alert alert-danger mb-70 text-center">
                                    <p class="">
                                        Need to cancel this appointment? Press below.
                                    </p>

                                    <a data-toggle="modal" data-target="#appointment-cancel-modal"
                                       class="btn btn-danger btn-md mt-10">
                                        Cancel
                                    </a>
                                </div>

                            @endif

                        </div>

                        <div class="clearfix"></div>


                    </div>


                </div>
            </div>

        </div>
    </section>

    <!-- Modal -->
    <div id="appointment-edit-modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="appointment-edit-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pt-30 pb-30">

                    <h3 class="text-danger">Are you sure?</h3>

                    <p class="text-dark-gray">
                        Once this has been saved and changed all parties will be notified via email.
                    </p>


                    <div class="clearfix"></div>

                    {{ Form::submit('save', ['class' => 'btn btn-md btn-blue mt-10']) }}

                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

    <!-- Modal -->
    <div id="appointment-cancel-modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="appointment-cancel-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pt-30 pb-30">

                    <h3 class="text-danger">Are you sure?</h3>

                    {{ Form::open(['route' => ['booking.update', $appointment->id . '?m=cancel'], 'method' => 'PUT', 'id' => 'cancelAppointment', 'data-parsley-validate' => '']) }}

                    <p class="text-dark-gray">
                        All parties will be notified immediately. Are you sure you wish to cancel this appointment?
                    </p>

                    {{ Form::submit('Cancel Now', ['class' => 'btn btn-md btn-danger mt-10']) }}

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="appointment-remove-cancel-modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="appointment-cancel-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pt-30 pb-30">

                    <h3 class="text-warning">Are you sure?</h3>

                    {{ Form::open(['route' => ['booking.update', $appointment->id . '?m=remove_cancel'], 'method' => 'PUT', 'id' => 'cancelAppointment', 'data-parsley-validate' => '']) }}

                    <p class="text-dark-gray">
                        All parties will be notified immediately. Are you sure you wish to restore this appointment?
                    </p>

                    {{ Form::submit('Restore Now', ['class' => 'btn btn-md btn-warning mt-10']) }}

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        function revealInputAppointment(hide, show) {
            $(show).toggleClass('hidden').removeAttr('disabled');
            $('#' + show.id + ' :input').removeAttr('disabled');
            $(hide).toggleClass('hidden');

            // console.log(show.id);
        }
    </script>

    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
@endsection
