{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Admin')

@section('stylesheets')

@endsection

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-cubes mr-10"></i> Suppliers
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $suppliers->count() === 1 ? 'is' : 'are' }} <span class="badge">{{ $suppliers->count() }}</span> registered supplier{{ $suppliers->count() === 1 ? '' : 's' }}.</h5>

        @if(Entrust::hasRole('super-admin'))
            <a href="{{ route('admin.users.create') }}" class="btn btn-sm btn-dark-gray">add new supplier</a>
        @endif

        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            @if($suppliers->count() === 0)
                <div class="row">
                    <div class="col-sm-12">
                        <p>You don't have any registered suppliers</p>
                    </div>
                </div>

            @else
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-body panel-default bg-white npadding">
                            <div class="table-responsive">
                                <table id="post-index" class="table table-bordered">
                                    <thead class="">
                                    <th style="width:250px;">Company</th>
                                    <th>Services</th>
                                    <th>No. Listed Adds</th>
                                    <th>No. of Appointments</th>
                                    <th class="text-center">Status</th>
                                    @if(Entrust::hasRole('super-admin'))
                                        <th></th>
                                    @endif
                                    </thead>

                                    <tbody>
                                    @foreach($suppliersPaginate as $supplier)

                                        <tr id="supplier-{{ $supplier->id }}">
                                            <td>
                                                <a href="{{ route('admin.suppliers.show', $supplier->id) }}" class="hover-dark-gray-1"><span class="title text-dark-gray-1 text-bold">{{ $supplier->company }}</span></a>
                                            </td>
                                            <td>
                                                @foreach($supplier->treatments as $treatment)
                                                    <span class="label bg-beige text-dark-gray">{{ $treatment->name }}</span>
                                                @endforeach
                                            </td>
                                            <td class="text-center">
                                                {{ $supplier->services()->count() }}
                                            </td>
                                            <td class="text-center">
                                                {{  $supplier->appointments()->count() }}
                                            </td>
                                            <td class="status-label text-center">
                                                @if( $supplier->active === 0 )
                                                    <label class="label label-danger {{-- nbradius --}}">in-active</label>
                                                @else
                                                    <label class="label label-success {{-- nbradius --}}">active</label>
                                                @endif
                                            </td>
                                            @if(Entrust::hasRole('super-admin') && $supplier->active !== 0)
                                                <td class="text-center">
                                                    <a role="button" data-toggle="collapse" href="#deactivateCustomer-{{ $supplier->id }}" aria-expanded="false" aria-controls="deactivateCustomer-{{ $supplier->id }}" data-parent="#customers-index" class="text-danger small">[deactivate]</a>
                                                </td>
                                            @elseif(Entrust::hasRole('super-admin') && $supplier->active === 0)
                                                <td class="text-center">
                                                    <a role="button" data-toggle="collapse" href="#activateCustomer-{{ $supplier->id }}" aria-expanded="false" aria-controls="activateCustomer-{{ $supplier->id }}" data-parent="#customers-index" class="text-success small">[activate]</a>
                                                </td>
                                            @endif
                                        </tr>

                                        <tr class="bg-danger npadding">
                                            <td class="npadding" colspan="{{ Entrust::hasRole('super-admin') ? '7' : '6' }}">
                                                <div id="deactivateCustomer-{{ $supplier->id }}" class="collapse">
                                                    <div class="pl-10 pt-20 pb-20 pr-10 text-center">
                                                        <p>
                                                            Are you sure you would like to deactivate this users account?
                                                        </p>
                                                        {{ Form::open(['url' => ['supplier' . '?page=deactivate', $supplier->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}
                                                        {{ Form::text('active', 0, ['class' => 'hidden']) }}
                                                        {{ Form::button('<i class="fa fa-user mr-10"></i> deactivate', ['class' => 'btn btn btn-danger btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="bg-success npadding">
                                            <td class="npadding" colspan="{{ Entrust::hasRole('super-admin') ? '7' : '6' }}">
                                                <div id="activateCustomer-{{ $supplier->id }}" class="collapse">
                                                    <div class="pl-10 pt-20 pb-20 pr-10 text-center">
                                                        <p>
                                                            Enable this customer again?
                                                        </p>
                                                        {{ Form::open(['url' => ['supplier' . '?page=deactivate', $supplier->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}
                                                        {{ Form::text('active', 1, ['class' => 'hidden']) }}
                                                        {{ Form::button('<i class="fa fa-user mr-10"></i> activate', ['class' => 'btn btn btn-success btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>


                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="pl-15 pull-left pt-5">
                        Page <strong>{!! $suppliersPaginate->currentPage() !!}</strong> of {!! $suppliersPaginate->lastPage() !!}
                    </div>

                    <div class="pull-right pr-15">
                        {!! $suppliersPaginate->links() !!}
                    </div>

                </div>
            @endif

                <div class="row mt-30">
                    <div class="col-sm-12">
                        <a href="{{ route('admin') }}" class="btn btn-md btn-beige">back</a>
                    </div>
                </div>
        </div>
    </section>


@stop

@section('scripts')

@stop
