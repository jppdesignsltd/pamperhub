@extends('layouts.admin')

@section('title', 'Edit User')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
@stop

@section('header-scripts')
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-user mr-10"></i> Edit User
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    {{ Form::model($username, ['route' => ['user.update', $username->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'id' => 'supplierBasicProfile']) }}

    <div id="action_panel" class="bg-beige">

        <h3 class="pb-5">
            <span class="label label-{{ $username->active === 1 ? 'success' : 'danger' }}">
                {{ $username->active === 1 ? 'Active' : 'Inactive' }} User
            </span>
        </h3>
        {{ Form::submit('update listing', ['class'=>'btn btn-md btn-dark-gray']) }}
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Basic Information</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">
                            <div class="col-sm-5 npadding">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            {{ Form::label('firstname', 'First Name') }}
                                        </div>

                                        <div class="col-md-8">
                                            {{ Form::text('firstname', null, ['class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            {{ Form::label('lastname', 'Last Name') }}
                                        </div>

                                        <div class="col-md-8">
                                            {{ Form::text('lastname', null, ['class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-7 npadding">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5">
                                            {{ Form::label('email', 'Email') }}
                                        </div>

                                        <div class="col-md-7">
                                            {{ Form::email('email', null, ['class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5">
                                            {{ Form::label('password', 'Password') }}
                                            {{--<div class="clearfix"></div>--}}
                                            <a id="editPassword" class="text-dark-gray ml-10" style="cursor: pointer;"><span>[change]</span></a>
                                        </div>

                                        <div class="col-md-7">
                                            {{ Form::password('password', ['class' => 'form-control password', 'disabled' => '', 'id'=> 'userPassword']) }}
                                        </div>
                                    </div>
                                </div>

                                <script>
                                    $("#editPassword").on("click", function() {
                                        $('#userPassword').removeAttr('disabled');
                                        $('#userPassword').val('');
                                    });
                                </script>
                            </div>

                        </div>
                    </div>




                    <a href="{{ route('admin.users') }}" class="btn btn-md btn-beige mt-10">back</a>

                </div>

            </div>
        </div>
    </section>
    {{ Form::close() }}


@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
@endsection
