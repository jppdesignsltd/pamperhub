@extends('layouts.admin')

@section('title', 'Create New User')

@section('stylesheets')
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
@endsection

@section('header-scripts')
@endsection

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-users mr-10"></i> Users
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>


    <div id="action_panel" class="bg-beige">
        <h3>
            Create New User
        </h3>
        <a href="{{ route('admin.users') }}" class="btn btn-md btn-dark-gray">back</a>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            {{ Form::open(['route' => ['user.store'], 'method' => 'POST', 'data-parsley-validate' => '']) }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="well bg-light-gray">

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-12">
                                    {{ Form::label('firstname', 'First Name') }}
                                    {{ Form::text('firstname', null, ['class' => 'form-control', 'required' => '']) }}
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-12">
                                    {{ Form::label('lastname', 'Last Name') }}
                                    {{ Form::text('lastname', null, ['class' => 'form-control', 'required' => '']) }}
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-12">
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::email('email', null, ['class' => 'form-control', 'required' => '']) }}
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="col-sm-6">
                    <div class="well bg-light-gray">

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-12">
                                    {{ Form::label('password', 'Password') }}
                                    {{ Form::password('password', ['class' => 'form-control', 'required' => '', 'minLength' => '6']) }}
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-12">
                                    {{ Form::label('password_confirmation', 'Confirm Password') }}
                                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => '', 'minLength' => '6']) }}
                                </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-12">
                                    {{ Form::label('user_type', 'Role') }}
                                    <select name="user_type" id="user_type" required="required">
                                        <option value="customer">Customer</option>
                                        <option value="supplier">Supplier</option>
                                        <option value="customer-service-rep">Customer Service Rep</option>
                                        <option value="super-admin">Super Admin</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>

                <div class="col-sm-12">
                    {{ Form::submit('create user', ['class' => 'btn btn-block btn-lg btn-blue']) }}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </section>


@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}

    <script type="text/javascript">
        $("#user_type").select2({
            width: '100%',
        });
    </script>
@stop