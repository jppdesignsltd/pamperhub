@extends('layouts.errors')

@section('title', '404 Error')

@section('content')

<section>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center">
        <h3>This page cannot be found</h3>

        @if(Auth::check())
          <a href="{{ route('admin') }}" class="btn btn-primary btn-md">Return to Admin Panel</a>
        @else
          <a href="{{ route('/') }}" class="btn btn-primary btn-md">Return to Home Page</a>
        @endif
      </div>
    </div>
  </div>
</section>


@stop
