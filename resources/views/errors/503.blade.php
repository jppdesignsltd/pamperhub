@extends('layouts.main')

@section('title', '503 Error')

@section('content')

  <div class="row">
    <div class="col-md-12">
      <div class="dashboard_profile-heading">
        <h3>You do not have permission to view this page.</h3>

        @if(Auth::check())
          <a href="{{ route('admin') }}" class="btn btn-red-alt btn-md">Return to Admin Panel</a>
        @else
          <a href="{{ route('/') }}" class="btn btn-red-alt btn-md">Return to Home Page</a>
        @endif
      </div>
    </div>
  </div>

@stop
