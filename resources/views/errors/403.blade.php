@extends('layouts.errors')

@section('title', '403 Error')

@section('content')
  <section class="padding-lg">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <h3>You do not have permission to view this page.</h3>

            @if(Entrust::hasRole('admin', 'editor'))
              <a href="{{ route('admin') }}" class="btn btn-red-alt btn-md">Return to Admin Panel</a>
            @elseif(Entrust::hasRole('user'))
              <a href="{{ url('/') }}" class="btn btn-red-alt btn-md">Return to Home Page</a>
            @else
              <a href="{{ url('/') }}" class="btn btn-red-alt btn-md">Return to Home Page</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>

@stop
