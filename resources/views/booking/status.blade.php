@extends('layouts.main')

@section('title', 'Booking Status')

@section('stylesheets')
@endsection

@section('header-scripts')
@endsection

@section('content')

    <section id="print-area" class="">
        <div class="container">

            @if($status === 'no-order')

                <div class="row pt-50 pb-50">
                    <div class="col-sm-12">
                        <h2 class="text-center nmbottom">There is no booking please view your account for your
                            bookings.</h2>
                    </div>
                </div>

            @else
                <div class="row mb-10">
                    <div class="col-sm-10 col-sm-offset-1 text-center">
                        @if ($appointment->payment_received === 1)
                            <div class="well nb-radius bg-success">

                                <h2 class="nmbottom text-success">Booking Confirmed</h2>

                                <div class="clearfix"></div>

                            </div>
                        @endif

                        @if ($appointment->payment_received === 0)

                            <div class="well nb-radius bg-danger">

                                <h2 class="nmbottom text-danger">Booking Failed</h2>

                                <div class="clearfix"></div>

                            </div>
                        @endif
                    </div>
                </div>

                <div class="row mb-10">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h4>Customer Details</h4>

                        <div class="table-responsive">
                            <table id="customers-index" class="table table-bordered">
                                <thead class="bg-light-gray">
                                <th>Customer No.</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Address</th>
                                </thead>

                                <tbody>

                                <tr>
                                    <td>
                                        <strong>
                                            #00{{ $customer->id }}
                                        </strong>

                                    </td>
                                    <td>
                                        {{ $customer->user->firstname }}
                                    </td>
                                    <td>
                                        {{ $customer->user->lastname }}
                                    </td>
                                    <td>
                                        {{ $customer->address1 }},
                                        {{ $customer->address2 }}{{ $customer->address2 === '' ? ',' : ''}}
                                        {{ $customer->city }},
                                        {{ $customer->postal_code }}
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h4>Booking Details</h4>

                        <div class="table-responsive">
                            <table id="customers-index" class="table table-bordered">
                                <thead class="bg-light-gray">
                                <th style="width: 110px;">Order No.</th>
                                <th>Duration</th>
                                <th>Total Price</th>
                                <th>Date</th>
                                <th>Time of Day</th>
                                <th>Paid</th>
                                <th>Payment Method</th>
                                </thead>

                                <tbody>

                                <tr>
                                    <td>
                                        <strong>
                                            #ORD-0{{ $appointment->id }}
                                        </strong>
                                    </td>
                                    <td>
                                        {{ $appointment->duration > 1 ? "$appointment->duration hrs" : "$appointment->duration hr" }}
                                    </td>
                                    <td>
                                        &pound;{{ $appointment->price }}
                                    </td>
                                    <td>
                                        {{ date('jS F, Y' , strtotime($appointment->date)) }}
                                    </td>
                                    <td style="width: 120px">
                                        {{ ucfirst($appointment->time_of_day) }}
                                    </td>
                                    <td>
                                        @if($appointment->payment_received === 0)
                                            <span class="label label-sm label-danger">No</span>
                                        @else
                                            <span class="label label-sm label-success">Yes</span>
                                        @endif
                                    </td>
                                    <td>
                                        PayPal
                                    </td>
                                </tr>

                                </tbody>

                                <tr class="clearfix"></tr>

                                <thead class="bg-light-gray">
                                <th style="width: 110px;">Supplier ID</th>
                                <th>Service Name</th>
                                <th style="width: 130px">Treatment</th>
                                <th>Supplier</th>
                                <th>Contact No.</th>
                                <th colspan="2">Address</th>
                                </thead>

                                <tbody>

                                <tr>
                                    <td>
                                        <strong>#SUP-0{{ $appointment->supplier->id }}</strong>
                                    </td>
                                    <td>{{ $appointment->service->title }}</td>
                                    <td>{{ $appointment->treatment->name }}</td>
                                    <td>{{ $appointment->supplier->company }}</td>
                                    <td>{{ $appointment->supplier->contact_number }}</td>
                                    <td colspan="2">
                                        {{ $appointment->supplier->address1 }},
                                        {{ $appointment->supplier->address2 }}{{ $appointment->supplier->address2 === '' ? '' : ','}}
                                        {{ $appointment->supplier->city }},
                                        {{ $appointment->supplier->postal_code }}
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row hidden-print">
                <div class="col-sm-10 col-sm-offset-1 text-right">
                    @if ($appointment->payment_received === 1)
                        <a onclick="print()" class="btn btn-md btn-beige mr-20">
                            print
                        </a>
                    @endif


                    <a href="{{ route('customer.index') }}" class="btn btn-md btn-blue">
                        my account
                    </a>
                </div>
            </div>

        </div>
    </section>

@stop

@section('scripts')

    <script>
        function print() {
            var w = window.open();
            w.document.write(document.getElementById('print-area').innerH‌​TML);
            w.print();
            w.close();
        }
    </script>
@endsection
