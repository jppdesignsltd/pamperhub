@extends('layouts.main')

@section('title', 'Create Booking')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    {!! Html::style('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
@endsection

@section('header-scripts')
    {!! Html::script('/bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
@endsection

@section('content')

    @if($lockdate === false)
        {{ Session::forget('error') }}
    @endif

    <section class="pt-30 pb-30">
        <div class="container">
            <div class="row mb-20">
                <div class="col-sm-12 pl-30 pr-30">
                    <h2 class="nmbottom nmtop text-center">
                        Your Booking
                    </h2>

                    <div class="clearfix"></div>
                    <hr class="bc-blue mt-10">
                </div>
            </div>

            {!! Form::open(array('route' => 'addmoney.paypal', 'id' => 'booking', 'data-parsley-validate' => '')) !!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12 pl-30 pr-30">
                    <div class="service_post">

                        <div class="service_post-container">
                            <div class="service_post-header">
                                <img
                                        @if($service->cover_image !== '')
                                        src="{{ asset('uploads/services/' . $service->cover_image) }}"
                                        alt=""
                                        @else
                                        src="{{ asset('uploads/services/bg-' . strtolower($service->treatment->name) . '.jpg') }}"
                                        onerror="this.src='{{ asset('dist/img/default-service-bg.png') }}'"
                                        alt=""
                                        @endif

                                >
                                <div class="image-overlay"></div>
                                <h3>
                                    {{ $service->supplier->company }}
                                    {{ Form::text('company', $service->supplier->company, ['class' => 'hidden', 'readonly' => '']) }}
                                    {{ Form::text('service_title', $service->title, ['class' => 'hidden', 'readonly' => '']) }}
                                    {{ Form::number('service_id', $service->id, ['class' => 'hidden', 'readonly' => '']) }}
                                    <div class="clearfix"></div>
                                    <span class="small">
                                        {{ $service->supplier->city }}
                                    </span>
                                </h3>

                                <p class="availability">
                                    <span class="clearfix"></span>
                                    <span class="text-bold mr-5 small">Open:</span>
                                    @foreach($service->supplier->days as $dayName)
                                        <span class="label label-xs bg-white text-dark-gray">{{ $dayName->name }}</span>
                                    @endforeach
                                </p>

                                <div class="clearfix"></div>
                            </div>

                            <div class="service_post-body">
                                <div class="form-group mb-10">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="service" class="nptop">
                                                Service Type:
                                            </label>
                                        </div>

                                        <div class="col-xs-6">
                                            <p class="nmbottom">{{ $service->treatment->name }}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group mb-10">
                                    <div class="row">
                                        @if($service->promotion === 1)

                                            <div class="col-xs-6">
                                                <label for="service" class="nptop">
                                                    Sale Price (p/h):
                                                </label>
                                            </div>

                                            <div class="col-xs-6">
                                                <p class="nmbottom">&pound;{{ $service->promotion_price }}</p>
                                            </div>

                                        @else

                                            <div class="col-xs-6">
                                                <label for="service" class="nptop">
                                                    Price (p/h):
                                                </label>
                                            </div>

                                            <div class="col-xs-6">
                                                <p class="nmbottom">&pound;{{ $service->price }}</p>
                                            </div>

                                        @endif
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group mb-10 hidden">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="duration" class="pt-5">
                                                Duration:
                                            </label>
                                        </div>

                                        <div class="col-xs-6">
                                            <select name="duration" id="duration" class="form-control select2-standard"
                                                    onchange="calculateTotal()" required="">
                                                <option value="0.5">30 mins</option>
                                                <option value="1" selected>1 hr</option>
                                                <option value="1.5">1.5 hrs</option>
                                                <option value="2">2 hrs</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group mb-10">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="date" class="{{ $lockdate === true ? 'nptop' : '' }}">
                                                Date:
                                            </label>

                                            @if($lockdate === true)
                                                <a href="{{ url('booking?s=' . $service->id) }}">
                                                    change date
                                                </a>
                                            @endif
                                        </div>

                                        @if($lockdate === true)

                                            <div class="col-xs-6">
                                                <p class="">{{ $dpValue }}</p>
                                                {{ Form::text('date', $dpValue, ['class' => 'hidden', 'readonly' => '']) }}
                                            </div>

                                        @else
                                            <div class="col-xs-6">
                                                <div class='input-group date' id='date'>

                                                    {{ Form::text('date', null, ['class' => 'form-control bg-white', 'required' => '', 'placeholder' => 'Choose Date']) }}
                                                    <span class="input-group-addon nb-radius">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                </div>

                                                <script type="text/javascript">
                                                    $(function () {

                                                        var today = new Date();

                                                        var dpVal = "{{ $dateForTable }}";

//                                                        console.log(dpVal);

                                                        var year = today.getFullYear();
                                                        var month = today.getMonth();
                                                        var date = today.getDate();
                                                        var dateArray = [];
                                                        var deleteArray = [];
                                                        var startDate;
                                                        var endDate;
                                                        var dateMove;
                                                        var disableHoliday;

//                                                        console.log(year);
//                                                        console.log(month);
//                                                        console.log(date);

                                                        for (var j = 0; j < 30; j++) {

//                                                            var day = new Date(year, month, date + j);
                                                            var day = new Date(year, month, date + j).setHours(0,0,0,0);

                                                            dateArray.push(day);
                                                        }

                                                        console.log(dateArray);

                                                        if (dpVal !== '') {
                                                            var disable = new Date(dpVal).setHours(0,0,0,0);

                                                            // Find and remove item from an array
                                                            var d = dateArray.indexOf(disable);
                                                            if(d !== -1) {
                                                                dateArray.splice(d, 1);
                                                            }
                                                        }

                                                        @if($holidays)
                                                            @foreach($holidays as $holidayDate)
                                                                startDate = "{{ $holidayDate->date_from }}";
                                                                endDate = "{{ $holidayDate->date_to }}";
                                                                dateMove = new Date(startDate);



                                                                while (startDate < endDate) {
                                                                    startDate = dateMove.toISOString().slice(0, 10);
//                                                                    disableHoliday = new Date(startDate);
                                                                    disableHoliday = new Date(startDate).setHours(0,0,0,0);
                                                                    deleteArray.push(disableHoliday);
                                                                    dateMove.setDate(dateMove.getDate() + 1);
                                                                }
                                                            @endforeach

                                                            console.log(deleteArray);


//                                                            var p = deleteArray.length;
//                                                            while (p--) {
//                                                                var da = dateArray.indexOf(deleteArray[p]);
//                                                                console.log(da);
//                                                                console.log(deleteArray[p]);
//                                                            }

                                                            var l = deleteArray.length;
                                                            while (l--) {

//                                                              // Get DateArray then grab the deleteArray Values and remove them from the DateArray.
                                                                var dh = dateArray.indexOf(deleteArray[l]);

                                                                if(dh !== -1) {
                                                                    console.log('Delete input ' + dh);
                                                                    dateArray.splice(dh, 1);
                                                                }
                                                                console.log(dh);
                                                            }

                                                        @endif

                                                        console.log(dateArray);


                                                        var supplierDays = [];
                                                        var disabledDays = [0, 1, 2, 3, 4, 5, 6];

                                                        @foreach($service->supplier->days as $day)
                                                            supplierDays.push({{ $day->time_ref }});
                                                        @endforeach

                                                        // console.log(supplierDays);

                                                        for (var i = supplierDays.length - 1; i >= 0; i--)
                                                            // console.log(supplierDays[i]);
                                                            disabledDays.splice(disabledDays.indexOf(supplierDays[i]), 1);
                                                            // console.log(disabledDays);

                                                        $('#date').datetimepicker({
                                                            daysOfWeekDisabled: disabledDays,
                                                            useCurrent: false,
                                                            enabledDates: dateArray,
                                                            viewMode: 'days',
                                                            format: 'DD/MM/YYYY'
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        @endif


                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group nmbottom">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="time_of_day" class="pt-5">
                                                Time:
                                            </label>
                                        </div>

                                        <div class="col-xs-6">
                                            <select name="time_of_day" id="time" class="form-control "
                                                    required="">
                                                <option value="" disabled="" selected>Please Choose</option>

                                                @if(!in_array("morning", $tod))
                                                    <option value="morning">Morning</option>
                                                @endif

                                                @if(!in_array("afternoon", $tod))
                                                    <option value="afternoon">Afternoon</option>
                                                @endif

                                                @if(!in_array("evening", $tod))
                                                    <option value="evening">Evening</option>
                                                @endif

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="clearfix visible-sm mb-10"></div>

                <div class="col-md-4 col-sm-6 col-xs-12 pl-30 pr-30">
                    <h4 class="mb-5">Customer Details</h4>
                    <hr class="bc-blue mt-5 mb-10">

                    <div class="clearfix"></div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6 pr-5">
                                {{ Form::text('firstname', $customer->user->firstname, ['class' => 'form-control', 'placeholder' => 'First Name', 'disabled' => '']) }}
                            </div>

                            <div class="col-xs-6 pl-5">
                                {{ Form::text('lastname', $customer->user->lastname, ['class' => 'form-control', 'placeholder' => 'Last Name', 'disabled' => '']) }}
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::text('address1', $customer->address1, ['class' => 'form-control', 'placeholder' => 'Address Line 1', 'required' => '', 'id' => 'address1', 'onchange' => 'updateCustomer(' . $customer->id . ')']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::text('address2', $customer->address2, ['class' => 'form-control', 'placeholder' => 'Address Line 2', 'id' => 'address2', 'onchange' => 'updateCustomer(' . $customer->id . ')']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::text('city', $customer->city, ['class' => 'form-control', 'placeholder' => 'City', 'required' => '', 'id' => 'city', 'onchange' => 'updateCustomer(' . $customer->id . ')']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::text('postal_code', $customer->postal_code, ['class' => 'form-control', 'placeholder' => 'Post Code', 'required' => '', 'id' => 'postal_code', 'onchange' => 'updateCustomer(' . $customer->id . ')']) }}
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <h4 class="mb-5 mt-10">Additional Information</h4>
                    <hr class="bc-blue mt-5 mb-10">
                    <div class="clearfix"></div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::textarea('additional_information', null, ['class' => 'form-control', 'placeholder' => 'Let us know any extra info.', 'style' => 'height:100px;']) }}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 pl-30 pr-30">

                    <h4 class="mb-5">Payment Details</h4>
                    <hr class="bc-blue mt-5 mb-10">
                    <div class="clearfix"></div>

                    <div id="total-box" class="well bg-light-gray pt-10 pb-10">

                        <div class="row">
                            <div class="col-xs-5">
                                <p class="nmbottom pt-5">
                                    <strong>Total:</strong>
                                </p>
                            </div>

                            <div class="col-xs-7 text-right">
                                <div class="input-group">
                                    <span class="input-group-addon bg-light-gray text-bold">&pound;</span>
                                    {{ Form::number('total_amount', null, ['id' => 'total_amount', 'class' => 'nborder form-control text-right', 'style' => 'font-size:16px;', 'readonly' => '', 'step' => '0.01', 'min' => '0.00']) }}
                                </div>
                            </div>
                        </div>

                    </div>

                    <p class="small mb-20">
                        * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam, at atque blanditiis.
                    </p>

                    <p>
                        <img src="{{ asset('dist/img/paypal.png') }}" alt="Paypal badge" width="150">
                    </p>

                    {{ Form::submit('book now', ['class' => 'btn btn-lg btn-blue btn-block']) }}

                </div>
            </div>


            {!! Form::close() !!}

        </div>
    </section>

@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}

    <script>
        $(".select2-standard").select2({
            width: '100%',
            placeholder: 'Select all options'
        });

        var duration = $('#duration').val();

                @if($service->promotion === 1)
        var price = {{ $service->promotion_price }};
                @else
        var price = {{ $service->price }};
                @endif

        var calc_total = price * duration;
        var total = calc_total.toFixed(2);

        $('#total_amount').val(total);

        function calculateTotal() {
            var duration = $('#duration').val();

                    @if($service->promotion === 1)
            var price = {{ $service->promotion_price }};
                    @else
            var price = {{ $service->price }};
                    @endif

            var calc_total = price * duration;
            var total = calc_total.toFixed(2);

            $('#total_amount').val(total);
        }


        function updateCustomer(id) {

            // Update Customer
            var elem = event.target.id;
            var token = $('#booking input[name=_token]').val();
            var elemValue = $('#' + elem).val();
            var value = elemValue.replace(' ', '+');
            var dataString = '_token=' + token + '&' + elem + '=' + value;
//            var dataString = $('#booking').serialize();
            var url = '/customer/' + id;

//            return console.log(dataString);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var promise = $.ajax({
                type: 'PUT',
                url: url,
                data: dataString
            });

            promise.done(function (response) {
//                console.log(response);
            });

            promise.fail(function (data) {
//                var errors = data.responseJSON;
//                console.log(data);
            });
        }
    </script>
@endsection
