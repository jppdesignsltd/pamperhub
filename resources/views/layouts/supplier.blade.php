<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    @include('partials._head')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    {!! Html::style('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {!! Html::style('bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') !!}

    {!! Html::script('/bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('bower_components/bootstrap-filestyle/src/bootstrap-filestyle.min.js') !!}
</head>

<body>
@include('partials._nav')

<main id="main" class="ie8-hide" role="main">
    @include('partials._messages')

    @if($supplier->active === 0)
        <section id="signup-hero">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 text-center mt-50 mb-80">
                        <div id="stage-complete" class="well bg-transparent">
                            <i class="fa fa-times"></i>
                            <h1 class="text-uppercase">Your account is not yet active</h1>
                            <p>Please wait for a member of the team to get back to you once we have verified your
                                application</p>
                        </div>

                        <a href="{{ route('/') }}" class="btn btn-blue btn-lg mt-20 text-uppercase">Return Home</a>
                    </div>
                </div>
            </div>
        </section>
    @else

        <section class="npbottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="nmbottom">@yield('heading')</h2>
                        <hr class="supplier-hr">
                    </div>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>

        @yield('content')
        <div class="clearfix"></div>
    @endif
</main>

@include('partials._footer')
{!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
{!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}
{!! Html::script('dist/js/supplier.js') !!}
{!! Html::script('bower_components/bootstrap-switch/dist/js/bootstrap-switch.js') !!}

@yield('scripts')
</body>
</html>
