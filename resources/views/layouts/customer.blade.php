<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    @include('partials._head')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    {!! Html::style('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}

    {!! Html::script('/bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('bower_components/bootstrap-filestyle/src/bootstrap-filestyle.min.js') !!}
</head>

<body>
@include('partials._nav')

<main id="main" class="ie8-hide" role="main">
    @include('partials._messages')

    @yield('content')
    <div class="clearfix"></div>
</main>

@include('partials._footer')
{!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
{!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}

@yield('scripts')
</body>
</html>
