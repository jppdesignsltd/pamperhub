<!DOCTYPE html>
<html lang="en" ng-app="myApp">
    <head>
        @include('partials._head')
    </head>

    <body>
        @include('partials._nav')

        <main id="main" class="ie8-hide" role="main">
            @include('partials._messages')

            @yield('content')
            <div class="clearfix"></div>
	    </main>

        @include('partials._footer')
        @yield('scripts')
	</body>
</html>
