<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    @include('partials.admin._head')
</head>

<body>
@include('partials.admin._nav')

<main id="main" class="ie8-hide" role="main">
    <section class="ie8">
        <div class="container">
            <div class="row mt-20 mb-20">
                <div class="col-sm-8 col-sm-offset-2 text-center">
                    <h1 class="nmtop">Oops!</h1>
                    <p>It looks like you are using an outdated version of Internet Explorer.</p>
                    <p>This website is best viewed using IE9 and later. Please update your browser to continue and
                        experience the site and content at its very best.</p>
                    <p>Alternatively open the site in a recommended modern browser such as Firefox or Chrome.</p>
                </div>
            </div>
        </div>
    </section>
    @include('partials._messages')

    @yield('content')
    <div class="clearfix"></div>

    @include('partials.admin._footer')
</main>


@yield('scripts')
</body>
</html>
