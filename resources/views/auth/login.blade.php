@extends('layouts.main')

@section('title', 'Login')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="well bg-light-gray nmbottom">
                    {!! Form::open() !!}

                    <div class="form-group {{ $errors->has('email') ? ' form-error' : '' }}">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', null, ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group" {{ $errors->has('password') ? ' form-error' : '' }}>
                        {{ Form::label('password', 'Password') }}
                        {{ Form::password('password', ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                    </div>

                    <div class="form-group mt-20">
                        {{ Form::submit('Log In', ['class' => 'btn-blue btn btn-md']) }}
                        <div class="clearfix"></div>

                        <a class="btn btn-link npleft mt-10" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default panel-body text-center pt-30 pb-30 pl-40 pr-40">
                    <h3 class="mb-20">Register Today</h3>
                    <p>Haven't signed up yet? Get started today by opening a free account.</p>
                    <a href="{{ route('register') }}" class="btn btn-md btn-blue mt-10">register now</a>
                    <p class="mt-20">
                        <small>
                            Want to register your business? <a href="{{ route('supplier.register') }}">click here</a>
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
