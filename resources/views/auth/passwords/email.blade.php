@extends('layouts.main')

@section('title', 'Reset Password')

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <h2 class="nmbottom nmtop text-center">
                        Reset Password
                    </h2>
                    <div class="clearfix"></div>
                    <hr style="width: 200px;" class="bc-blue mt-10 mb-30">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="well bg-light-gray nmbottom">

                        <form class="l" role="form" method="POST"
                              action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                <label for="email" class="mb-10  control-label">E-Mail Address</label>

                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" required>

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            </div>

                            <div class="clearfix mb-10"></div>

                            <div class="pull-right">
                                <button type="submit" class="btn btn-blue btn-md">
                                    Send Password Reset Link
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
