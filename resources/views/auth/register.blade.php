@extends('layouts.main')

@section('title', 'Register')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <h2 class="nmbottom nmtop text-center">
                    Register
                </h2>
                <p class="text-center">
                    Start benefiting from a mobile therapist near you. Today!</p>

                <div class="clearfix"></div>
                <hr style="width: 200px;" class="bc-blue mt-10 mb-30">

                <div class="well bg-light-gray nmbottom">

                    {!! Form::open() !!}

                    <div class="form-group">

                        {{ Form::label('firstname', 'First Name') }}
                        {{ Form::text('firstname', null, ['class' => 'form-control', 'required' => '']) }}

                    </div>
                    <div class="form-group">

                        {{ Form::label('lastname', 'Last Name') }}
                        {{ Form::text('lastname', null, ['class' => 'form-control', 'required' => '']) }}

                    </div>
                    <div class="form-group">

                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', null, ['class' => 'form-control', 'required' => '']) }}

                    </div>
                    <div class="form-group">

                        {{ Form::label('password', 'Password') }}
                        {{ Form::password('password', ['class' => 'form-control', 'required' => '', 'minLength' => '6']) }}

                    </div>

                    <div class="form-group">

                        {{ Form::label('password_confirmation', 'Confirm Password') }}
                        {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => '', 'minLength' => '6']) }}

                    </div>

                    <div class="form-group hidden">

                        {{ Form::text('user_type', null, ['class' => 'hidden']) }}

                    </div>

                    <div class="form-group mt-30 text-center">

                        {{ Form::submit('Sign Up', ['class' => 'btn-blue btn btn-md']) }}

                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
