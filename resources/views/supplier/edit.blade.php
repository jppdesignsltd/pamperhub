@extends('layouts.supplier')

@section('title', 'Edit Service Listing')

@section('stylesheets')
@stop

@section('header-scripts')
    {!! Html::script('bower_components/tinymce/tinymce.min.js') !!}

    <script type="text/javascript">
        tinymce.init({
            selector: '#main-body',
            menubar: false,
            toolbar: 'undo redo | bold italic underline strikethrough hr | alignleft aligncenter alignright alignjustify | bullist numlist | link | styleselect | fullscreen',
            plugins: 'link wordcount hr fullscreen lists advlist'
        });
    </script>
@stop

@section('heading', 'Edit Service Listing')

@section('content')

    <section class="nptop">
        <div class="container">

            {!! Form::model($service, ['route' => ['service.update', $service->id], 'method' => 'PUT', 'id' => 'supplierService', 'data-parsley-validate' => '', 'files' => true]) !!}

            <div class="well bg-dark-gray mb-30">
                <div class="row">
                    <div class="col-xs-6 text-white hidden-xs">
                        <p class="nmbottom">Edit your service listing here.</p>
                    </div>

                    <div class="col-sm-6 col-xs-12 text-right xs-text-center">
                        {{ Form::submit('save draft', ['class' => 'btn-sm btn-beige btn mr-20', 'id' => 'saveDraft']) }}

                        @if($service->status === 'draft')
                        {{ Form::submit('publish listing', ['class' => 'btn-sm btn-blue btn', 'id' => 'publish']) }}
                        @else
                        {{ Form::submit('update listing', ['class' => 'btn-sm btn-blue btn', 'id' => 'publish']) }}
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-8 col-sm-7">

                    <div class="well bg-light-gray">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('title', 'Title:') }}
                                </div>

                                <div class="col-sm-9">
                                    {{ Form::text('title', $service->title, ['class' => 'form-control', 'required' => '', 'maxlength' => '255']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20 hidden">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('slug', 'Service Link:', ['style' => 'padding-top:4px;']) }}
                                </div>

                                <div class="col-sm-9">
                                    {{ Form::text('slug', $service->slug, ['class' => 'nborder w-70', 'required' => '', 'readonly' => '' , 'id' => 'slug', 'maxlength' => '255', 'minlength' => '1']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-15">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('price', 'Price (p/h):', ['class' => '']) }}
                                </div>

                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon addon-pound bg-white text-bold" style="background-color: #fff !important;">&pound;</span>

                                        {{ Form::number('price', null, ['class' => 'form-control', 'required' => '', 'min' => '0.01', 'max' => '500', 'step' => '0.01']) }}
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    {{ Form::label('description', 'Service Description:') }}
                                    <div class="clearfix mb-10"></div>
                                    {{ Form::textarea('description', null, array('class' => 'form-control', 'id' => 'main-body')) }}
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="clearfix mb-10"></div>

                    <a href="{{ route('supplier.services') }}" class="btn btn-md btn-beige mt-10 hidden-xs">back</a>

                </div>

                <div class="col-md-4 col-sm-5">
                    <div class="well bg-light-gray">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5 class="text-bold">
                                        Cover Image:
                                        <span class="pull-right">
                                        <a id="clearImage" style="text-decoration: none;" onclick="clearFilestyle()"
                                           class="text-bold label-danger label-xs label {{ $service->cover_image === null || $service->cover_image === '' ? 'hidden' : '' }}">
                                            <i class="fa fa-times"></i> Remove
                                        </a>
                                    </span>
                                    </h5>
                                </div>
                            </div>

                            <div class="clearfix mb-5"></div>

                            @if($service->cover_image === '' || $service->cover_image === null)
                                <div class="row">

                                    <div id="image_preview-cont" class="col-sm-12 text-right hidden">
                                        <img id="image_preview" alt="Image Preview" class="img-responsive">

                                    </div>

                                    <div class="clearfix"></div>

                                    <div id="cover_image-cont" class="col-sm-12">

                                        {{ Form::file('cover_image', ['id' => 'cover_image', 'class' => 'filestyle','data-buttonName' => '', 'data-input' => 'true', 'data-placeholder' => 'No file chosen', 'data-buttonName' => 'btn-beige-plain',  'onchange' => 'loadFile(event)']) }}
                                    </div>

                                </div>
                            @else
                                <div class="row">

                                    <div id="image_preview-cont" class="col-sm-12 text-right">
                                        <img id="image_preview" alt="Image Preview"
                                             src="{{ asset('uploads/services/' . $service->cover_image) }}"
                                             class="img-responsive">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div id="cover_image-cont" class="col-sm-12 hidden">
                                        {{ Form::file('cover_image', ['id' => 'cover_image', 'class' => 'filestyle','data-buttonName' => '', 'data-input' => 'true', 'data-placeholder' => 'No file chosen', 'data-buttonName' => 'btn-beige-plain',  'onchange' => 'loadFile(event)']) }}
                                    </div>

                                </div>

                                {{ Form::text('remove_image', 'remove', ['class' => 'form-control hidden', 'disabled' => '', 'id' => 'remove-image']) }}
                            @endif


                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('live_date', 'Live Date:') }}
                                    <a href="#" onclick="revealInput(live_date_placeholder, live_date)">
                                        <i class="fa fa-pencil ml-10" ></i>
                                    </a>
                                </div>

                                <div class="col-sm-7">
                                    <div id="live_date_placeholder" style="padding-top: 7px">
                                        {{ date('j M, Y @ h:ia', strtotime($service->live_date)) }}


                                    </div>
                                    {{ Form::text('live_date', null, ['class' => 'form-control hidden', 'id' => 'live_date', 'placeholder' => date('j M, Y @ h:ia', strtotime($service->live_date))]) }}

                                    <script>
                                        $(function () {
                                            $("#live_date").datetimepicker({
                                                format: 'DD/MM/YYYY H:mm'
                                            });
                                        });
                                    </script>

                                    {{--{{ Form::text('live_date', $service->live_date, ['class' => 'form-control', 'id' => 'live_date', 'placeholder' => date('j M, Y h:ia', strtotime($service->live_date))]) }}--}}

                                    {{--<script>--}}
                                        {{--$(function () {--}}
                                            {{--$("#live_date").datetimepicker({--}}
                                                {{--format: 'DD/MM/YYYY H:mm'--}}
                                            {{--});--}}
                                        {{--});--}}
                                    {{--</script>--}}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('service_type', 'Service Type:') }}
                                </div>

                                <div class="col-sm-7">
                                <span class="form-control disabled npleft">
                                    <span class="hidden">{!! $treat = $treatments->where('id', $service->service_type)->first() !!}</span>
                                    {{ $treat->name }}
                                </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('status', 'Status:') }}
                                </div>

                                <div class="col-sm-7">
                                    {{ Form::text('status', null, ['class' => 'form-control disabled text-capitalize npleft', 'required' => '', 'readonly' => '']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('promotion', 'Sale:', ['class' => 'nptop']) }}
                                </div>

                                <div class="col-sm-7">
                                    <label class="npadding">
                                        {{ Form::radio('promotion', 1, false, ['id' => 'promotion-on', 'data-radio-all-off' => 'true', 'data-on-color' => 'success', 'data-off-color' => 'danger', 'data-size' => 'mini']) }}
                                    </label>

                                    <label class="npleft">
                                        {{ Form::radio('promotion', 0, true, ['id' => 'promotion-off', 'class' => 'hidden']) }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-15">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('promotion_price', 'Sale Price:', ['class' => '']) }}
                                </div>

                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <span id="promo-pound" class="npleft input-group-addon addon-pound text-bold" style="background-color: transparent !important;">&pound;</span>

                                        {{ Form::number('promotion_price', null, ['class' => 'form-control', 'disabled' => '', 'min' => '0.01', 'step' => '0.01']) }}
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="well bg-light-gray">
                        <label>Tags <span class="small"><a role="button" data-toggle="collapse" href="#addTag"
                                                           aria-expanded="false" aria-controls="addTag"
                                                           class="text-dark-gray small">[create new]</a></span></label>

                        <div class="form-group">
                            {{ Form::select('tags[]', $tag, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple', 'id' => 'tagMultiple']) }}
                        </div>
                        {!! Form::close() !!}

                        <div id="addTag" class="row collapse">
                            <div class="clearfix mb-20"></div>
                            <div class="col-sm-12">
                                {!! Form::open(['method' => 'POST', 'id' => 'tagForm', 'data-parsley-validate' => '']) !!}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                {{ Form::text('name', null, ['id' => 'tagName', 'class' => 'form-control', 'ng-model' => 'tagSlug', 'placeholder' => 'Enter tag name here']) }}
                                {{ Form::text('slug', $data['tagSlug'], ['class' => 'hidden']) }}

                                <div class="text-right nmbottom">
                                    <div id="tagErrorResponse" class="text-left mt-10">

                                    </div>
                                    <div class="clearfix"></div>
                                    <label class="pt-10 mt-5 pull-left small">
                                        <a role="button" data-toggle="collapse" href="#addTag" aria-expanded="false"
                                           aria-controls="addTag" class="text-dark-gray small">[close]</a>
                                    </label>
                                    {{ Form::submit('add', ['class' => 'btn btn-md btn-blue mt-10 pull-right']) }}

                                    <i id="tagLoader" class="fa-spinner fa hidden mt-20 mr-20"></i>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    <a href="{{ route('supplier.services') }}" class="btn btn-md btn-beige visible-xs mt-10">back</a>


                </div>

            </div>

        </div>
    </section>

@stop

@section('scripts')
    {!! Html::script('bower_components/angular/angular.min.js') !!}
    {!! Html::script('bower_components/angular-slugify/angular-slugify.js') !!}
    {!! Html::script('dist/js/supplierCreate.js') !!}

    <script type="text/javascript">
        $("#promotion-on").bootstrapSwitch();

        $('#promotion-on').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state === true) {
                $('#promotion-on').prop("checked", true);
                $('#promotion-off').prop("checked", false);
                $('#promotion_price').removeAttr('disabled');
                $('#promotion_price').attr('required', '');
                $('#promo-pound').attr('style', 'background-color:#FFF !important;').toggleClass('npleft');

            } else {
                $('#promotion-on').prop("checked", false);
                $('#promotion-off').prop("checked", true);
                $('#promotion_price').attr('disabled', '');
                $('#promotion_price').removeAttr('required');
                $('#pound-pound').removeAttr('style');
                $('#promo-pound').attr('style', 'background-color:transparent !important;').toggleClass('npleft');

            }
        });

        if ($('#promotion-on').is(':checked')) {
            $('#promotion_price').removeAttr('disabled');
            $('#promotion_price').attr('required', '');
            $('#promo-pound').attr('style', 'background-color:#FFF !important;').toggleClass('npleft');
        }

        $("#service_type").select2({
            width: '100%'
        });

        $("#tagMultiple").select2({
            width: '100%',
        });

        $("#tagMultiple").select2().val( {!! json_encode($service->tags()->allRelatedIds()) !!} ).trigger('change');
    </script>
@endsection
