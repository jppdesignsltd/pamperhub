@extends('layouts.supplier')

@section('title', 'Supplier Services')

@section('stylesheets')
@endsection

@section('header-scripts')
@endsection

@section('heading', 'My Service Listings')

@section('content')

    <section class="nptop">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">

                    @include('partials.supplier._nav')

                </div>

                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                    <div class="well bg-white npadding">
                        <div class="row">
                            <div class="col-xs-8 hidden-xs">
                                <p class="nmbottom">
                                @if ($supplier->services->count() === 0)
                                    You have not created any service listings yet why not start?
                                @else
                                    You can create a maximum of <span class="badge"></span>{{ $supplier->treatments->count() }} listing{{ $supplier->treatments->count() === 1 ? '' : 's' }} in total. 1 for every treatment type you have selected.
                                @endif
                                </p>
                            </div>

                            <div class="col-sm-4 col-xs-12 text-right">
                                @if($supplier->services->count() !== $supplier->treatments->count())
                                    <div class="hidden-xs">
                                        <a href="{{ route('supplier.create') }}" class="btn btn-blue btn-sm pull-right">add new</a>
                                    </div>
                                    <div class="visible-xs">
                                        <a href="{{ route('supplier.create') }}" class="btn btn-blue btn-sm btn-block">add new service</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <p class="nmargin">

                    </p>

                    @if ($supplier->services->count() > 0)
                        <div class="row mt-30">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="services-index" class="table table-bordered">
                                        <thead class="bg-light-gray">
                                        <th style="width:180px;">Title</th>
                                        <th style="width:100px;">Service</th>
                                        <th>Price p/h</th>
                                        <th>Promotion</th>
                                        <th>Sale Price</th>
                                        <th style="width:80px;">Status</th>
                                        <th style="width:100px;"></th>
                                        </thead>

                                        <tbody>
                                        @foreach($supplier->services as $service)

                                            <tr id="service-{{ $service->id }}">
                                                <td>
                                                    <a href="{{ route('supplier.edit', $service->id) }}"
                                                       class="hover-dark-gray-1 text-dark-gray text-underline"><span
                                                                class="title text-dark-gray-1 text-bold">{{ $service->title }}</span></a>
                                                </td>
                                                <td>
                                                    <span class="hidden">{!! $treat = $treatments->where('id', $service->service_type)->first() !!}</span>
                                                    {{ $treat->name }}
                                                </td>
                                                <td class="slug">
                                                    <strong>&pound;{{ $service->price }}</strong>
                                                </td>
                                                <td class="promotion-label">
                                                    @if( $service->promotion === 0 )
                                                        <label class="label label-danger {{-- nbradius --}}">Off</label>
                                                    @else
                                                        <label class="label label-success {{-- nbradius --}}">on</label>
                                                    @endif
                                                </td>
                                                <td class="promotion_price">
                                                    {{ $service->promotion_price !== null ? "&pound;$service->promotion_price" : 'N/A' }}
                                                </td>
                                                <td class="status-label">
                                                    @if( $service->status == 'draft' )
                                                        <label class="label label-danger {{-- nbradius --}}">{{ $service->status }}</label>
                                                    @else
                                                        <label class="label label-success {{-- nbradius --}}">{{ $service->status }}</label>
                                                    @endif
                                                </td>
                                                <td class="text-right">
                                                    <a role="button" data-toggle="collapse"
                                                       href="#deleteService-{{ $service->id }}" aria-expanded="false"
                                                       aria-controls="deleteService-{{ $supplier->id }}"
                                                       data-parent="#services-index"
                                                       class="text-danger small">[delete]</a>
                                                </td>
                                            </tr>

                                            <tr class="bg-danger npadding">
                                                <td class="npadding" colspan="7">
                                                    <div id="deleteService-{{ $service->id }}" class="collapse">
                                                        <div class="pl-10 pt-20 pb-20 pr-10 text-center">
                                                            <p>
                                                                Are you sure you would like to delete this service?
                                                            </p>
                                                            {{ Form::open(['url' => ['service', $service->id], 'method' => 'DELETE', 'data-parsley-validate' => '']) }}
                                                            {{ Form::button('<i class="fa fa-clone mr-10"></i> delete service', ['class' => 'btn btn btn-danger btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    @endif

                </div>
            </div>

        </div>


    </section>

@stop

@section('scripts')
@endsection
