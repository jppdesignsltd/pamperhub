@extends('layouts.supplier')

@section('title', 'Supplier Holiday/Sickness')

@section('stylesheets')
@endsection

@section('header-scripts')
@endsection

@section('heading', 'Holidays/Sickness')

@section('content')

    <section class="nptop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">

                    @include('partials.supplier._nav')

                </div>


                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                    {!! Form::open(['route' => ['holiday.store'], 'method' => 'POST', 'data-parsley-validate' => '', 'id' => 'supplierHoliday']) !!}
                    <p>
                        Booked a holiday? Or off Sick? Let us know so we can block that time out for you.
                    </p>
                    <div class="clearfix"></div>

                    <div class="well bg-light-gray mt-10">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('holiday_date1', 'From:', ['class' => 'nptop']) }}

                                    <div class='input-group date' id='holiday_dates1'>

                                        {{ Form::text('holiday_date1', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Date from...']) }}
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    {!! Form::label('holiday_date2', 'To:', ['class' => 'nptop']) !!}

                                    <div class='input-group date' id='holiday_dates2'>

                                        {{ Form::text('holiday_date2', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Date to...']) }}
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>

                                        <script type="text/javascript">
                                            $(function () {
                                                var today = new Date();
                                                var dateArray = [];
                                                var startDate;
                                                var endDate;
                                                var dateMove;

                                                @foreach($holidays as $holidayDate)
                                                    startDate = "{{ $holidayDate->date_from }}";
                                                    endDate = "{{ $holidayDate->date_to }}";
                                                    dateMove = new Date(startDate);

                                                    if (startDate === endDate) {

                                                        dateArray.push(startDate);

                                                    } else {

                                                        while (startDate < endDate){
                                                            startDate = dateMove.toISOString().slice(0,10);
                                                            dateArray.push(startDate);
                                                            dateMove.setDate(dateMove.getDate()+1);
                                                        }

                                                    }

                                                @endforeach

                                                console.log(dateArray);

                                                $('#holiday_dates1').datetimepicker({
                                                    disabledDates: dateArray,
                                                    minDate: today,
                                                    format: 'DD/MM/Y'
                                                });
                                                $('#holiday_dates2').datetimepicker({
                                                    useCurrent: false,
                                                    disabledDates: dateArray,
                                                    minDate: today,
                                                    format: 'DD/MM/Y'
                                                });

                                                $("#holiday_dates1").on("dp.change", function (e) {
                                                    $('#holiday_dates2').data("DateTimePicker").minDate(e.date);
                                                });
                                                $("#holiday_dates2").on("dp.change", function (e) {
                                                    $('#holiday_dates1').data("DateTimePicker").maxDate(e.date);
                                                });
                                            });
                                        </script>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    {{ Form::label('holiday_reason', 'Reason', ['class' => 'nptop']) }}
                                    <select name="holiday_reason" id="holiday_reason" class="form-control select2">
                                        <option value="holiday" selected>Holiday</option>
                                        <option value="sick">Sick</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    {{ Form::submit('save', ['class' => 'btn btn-md btn-blue btn-block', 'style' => 'margin-top:25px;']) }}
                                </div>
                            </div>
                        </div>

                    </div>
                    {!! Form::close() !!}


                    <div class="row mt-30">
                        <div class="col-sm-12">

                            @if($holidays->count() === 0)

                                <p>
                                    You have no holiday days added.
                                </p>

                            @else

                                <div class="table-responsive">
                                    <table id="holiday-index" class="table table-bordered">
                                        <thead class="bg-light-gray">
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Reason</th>
                                        <th style="width:100px;"></th>
                                        </thead>

                                        <tbody>
                                        @foreach($holidays as $holiday)

                                            <tr id="holiday-{{ $supplier->id }}">
                                                <td>
                                                    {{ date('d/m/Y', strtotime($holiday->date_from)) }}
                                                </td>
                                                <td>
                                                    {{ date('d/m/Y', strtotime($holiday->date_to)) }}
                                                </td>
                                                <td class="text-capitalize">
                                                    <span class="{{ $holiday->reason === 'sick' ? 'text-danger' : 'text-success' }}">{{ $holiday->reason }}</span>
                                                </td>
                                                <td class="text-right">
                                                    {{ Form::open(['route' => ['holiday.destroy', $holiday->id], 'method' => 'DELETE', 'data-parsley-validate' => '']) }}
                                                    {{ Form::button('<i class="fa fa-trash mr-10"></i> delete', ['class' => 'btn btn btn-danger btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif

                        </div>
                    </div>

                </div>
            </div>

        </div>


    </section>

@stop

@section('scripts')
    <script type="text/javascript">
        $(".select2-multi").select2({
            width: '100%',
        });

        $(".select2").select2({
            width: '100%',
        });
    </script>
@endsection
