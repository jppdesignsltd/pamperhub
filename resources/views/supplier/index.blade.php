@extends('layouts.supplier')

@section('title', 'My Account')

@section('stylesheets')
@endsection

@section('header-scripts')
@stop

@section('heading', 'My Account')

@section('content')

<section class="nptop">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4">
                @include('partials.supplier._nav')
            </div>

            {{ Form::model($supplier, ['url' => ['supplier' . '?page=basic', $supplier->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'id' => 'supplierBasicProfile']) }}
            <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                <div class="panel panel-default panel-body pb-10">
                    <div class="col-sm-6">
                        <p>
                            <strong class="mr-10">Full Name:</strong>
                            {{ $user->firstname . ' ' . $user->lastname }}
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <p>
                            <strong class="mr-10">Date of Birth:</strong>
                            {{ date('j M, Y', strtotime($user->supplier->dob)) }}
                        </p>
                    </div>

                    <div class="col-sm-6">
                        <p>
                            <strong class="mr-10">Email:</strong>
                            {{ $user->email }}
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <p>
                            <strong class="mr-10">Contact Number:</strong>
                            {{ $user->supplier->contact_number }}
                        </p>
                    </div>
                </div>

                <div class="well mt-10">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('company', 'Company') }}
                            </div>

                            <div class="col-sm-9">
                                {{ Form::text('company', $user->supplier->company, ['class' => 'form-control', 'disabled' => '', 'required' => '']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('address1', 'Address Line 1') }}
                            </div>

                            <div class="col-sm-9">
                                {{ Form::text('address1', $user->supplier->address1, ['class' => 'form-control', 'disabled' => '', 'required' => '']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('address2', 'Address Line 2') }}
                            </div>

                            <div class="col-sm-9">
                                {{ Form::text('address2', $user->supplier->address2, ['class' => 'form-control', 'disabled' => '']) }}

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('city', 'City') }}
                            </div>

                            <div class="col-sm-9">
                                {{ Form::text('city', $user->supplier->city, ['class' => 'form-control', 'disabled' => '', 'required' => '']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('postal_code', 'Post Code') }}
                            </div>

                            <div class="col-sm-9">
                                {{ Form::text('postal_code', $user->supplier->postal_code, ['class' => 'form-control', 'disabled' => '', 'required' => '']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('treatments', 'Your Services') }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::select('treatments[]', $treatment, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple', 'id' => 'treatmentsMultiple', 'disabled' => '', 'required' => '']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                {{ Form::label('bio', 'About me') }}
                            </div>

                            <div class="col-sm-9">
                                {{ Form::textarea('bio', $user->supplier->bio, ['class' => 'form-control', 'disabled' => '', 'maxLength' => $settings->services_excerpt, 'style' => 'height:80px;']) }}
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <a id="editBtn" onclick="activateSubmit('supplierBasicProfile', 'editBtn')"
                               class="btn btn-md btn-beige">Edit</a>
                            {{ Form::submit('save', ['class' => 'btn btn-md btn-blue hidden', 'id' => 'supplierBasicProfileBtn']) }}

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        {{ Form::close() }}

    </div>
</section>


@stop

@section('scripts')
    <script type="text/javascript">
        $(".select2-multi").select2({
            width: '100%',
            placeholder: 'Select days of operations'
        });

        $(".select2-multi").select2().val( {!! json_encode($supplier->treatments()->allRelatedIds()) !!} ).trigger('change');
    </script>
@endsection
