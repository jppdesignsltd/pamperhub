@extends('layouts.main')

@section('title', 'Register Supplier Account')

@section('stylesheets')
    {!! Html::style('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
@endsection

@section('header-scripts')
    {!! Html::script('/bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
@endsection

@section('content')

    <section id="signup-hero">
        <div class="container">
            <div class="row">

                @if(Auth::guest())
                    <div id="signup-stage-1">

                        <div class="col-md-4 col-md-offset-4">
                            <h2 class="">
                                Create An Account
                            </h2>

                            <hr>

                            {{ Form::open(['route' => ['supplier.register'], 'method' => 'POST', 'data-parsley-validate' => '']) }}

                            <div class="form-group">

                                {{ Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'First name*']) }}

                            </div>
                            <div class="form-group">

                                {{ Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Last name*']) }}

                            </div>
                            <div class="form-group">

                                {{ Form::email('email', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Email*']) }}

                            </div>
                            <div class="form-group">

                                {{ Form::password('password', ['class' => 'form-control', 'required' => '', 'placeholder' => 'Password*']) }}

                            </div>

                            <div class="form-group">

                                {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => '', 'placeholder' => 'Confirm Password*']) }}

                            </div>

                            <div class="form-group hidden">

                                {{ Form::text('user_type', 'supplier', ['class' => 'hidden']) }}

                            </div>

                            <div class="form-group text-center mt-30">

                                {{ Form::button('Join &raquo;', ['class' => 'btn btn-blue btn-lg text-uppercase', 'type' => 'submit']) }}

                            </div>

                            <div class="form-group">
                                <p class="small-print">
                                    By signing up you agree to our terms and conditions. Not a professional? Then signup
                                    for a <a href="{{ route('register') }}">customer account here.</a> If you already have an account then <a href="{{ '/login' }}">log in here</a>.
                                </p>
                            </div>

                            {!! Form::close() !!}
                        </div>

                        <div class="clearfix mb-100"></div>

                        <div class="col-sm-12">
                            <div class="progress">
                                <div class="progress-bar bg-blue nborder" role="progressbar" aria-valuenow="0"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span class="sr-only">0% Complete</span>
                                </div>

                                <div class="step-1 active">
                                    1
                                </div>

                                <div class="step-2">
                                    2
                                </div>

                                <div class="step-3">
                                    3
                                </div>

                                <div class="step-4">
                                    4
                                </div>
                            </div>
                        </div>

                    </div>

                @endif

                @if($step===null && !Auth::guest() && $supplier===null)
                    {{ Form::open(['route' => ['supplier.store', $user->id], 'method' => 'POST', 'data-parsley-validate' => '']) }}
                    <div id="signup-stage-2">
                        <div class="col-sm-12 text-center">
                            <h2>Basic Information</h2>
                            <hr>
                        </div>

                        <div class="col-sm-8 col-sm-offset-2">
                            <div class="">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                        <span class="form-control disabled nb-radius">
                                            {{ $user->firstname }}
                                        </span>
                                        </div>

                                        <div class="col-sm-6">
                                        <span class="form-control disabled nb-radius">
                                            {{ $user->lastname }}
                                        </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class='input-group date' id='dob'>

                                                {{ Form::text('dob', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Enter your DOB']) }}
                                                <span class="input-group-addon nb-radius">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>
                                        </div>

                                        <script type="text/javascript">
//                                            $(function () {
//                                                $('#dob').datetimepicker({
//                                                    viewMode: 'years',
//                                                    format: 'YYYY/MM/DD'
//                                                });
//                                            });

                                            $(function () {
                                                var today = new Date();

                                                $('#dob').datetimepicker({
                                                    defaultDate: "01/01/1990",
                                                    useCurrent: false,
                                                    maxDate: today.setFullYear(today.getFullYear()-18),
                                                    viewMode: 'years',
                                                    format: 'DD/MM/Y'
                                                });
                                            });
                                        </script>

                                        <div class="col-sm-6">
                                            {{ Form::text('contact_number', null, ['class' => 'form-control', 'required' => '', 'minLength' => '11', 'maxLength' => '11', 'data-parsley-error-message' => 'Please enter a valid phone number', 'placeholder' => 'Mobile']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group mt-30">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            {{ Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Company Name', 'required' => '']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            {{ Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Website (optional)']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            {{ Form::text('address1', null, ['class' => 'form-control', 'placeholder' => 'Address Line 1', 'required' => '']) }}
                                        </div>

                                        <div class="col-sm-6">
                                            {{ Form::text('address2', null, ['class' => 'form-control', 'placeholder' => 'Address Line 2']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            {{ Form::text('city', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'City']) }}
                                        </div>

                                        <div class="col-sm-6">
                                            {{ Form::text('postal_code', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Post Code']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center mt-30">
                                {{ Form::submit('next', ['class' => 'btn btn-lg btn-blue']) }}
                            </div>
                        </div>

                        <div class="clearfix mb-100"></div>

                        <div class="col-sm-12">
                            <div class="progress">
                                <div class="progress-bar bg-blue nborder" role="progressbar" aria-valuenow="32"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 32%;">
                                    <span class="sr-only">32% Complete</span>
                                </div>

                                <div class="step-1 active">
                                    1
                                </div>

                                <div class="step-2 active">
                                    2
                                </div>

                                <div class="step-3">
                                    3
                                </div>

                                <div class="step-4">
                                    4
                                </div>
                            </div>
                        </div>
                    </div>



                    {{ Form::close() }}
                @endif

                @if(!Auth::guest() && $supplier !== null)

                    @if(($step === 2 && $supplier->signup_complete === 0) || ($step === '2' && $supplier->signup_complete === 0))

                        {{ Form::open(['url' => ['supplier' . '?step=2', $user->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}

                        <div class="col-sm-12 text-center">
                            <h2>Your Experience</h2>
                            <hr>
                        </div>

                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            {{ Form::number('years_exp', null, ['class' => 'form-control', 'placeholder' => 'Years Experience', 'required' => '', 'min' => '1', 'max' => '50']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group bg-white pl-10 pr-10 pb-10" style="height: 150px;">
                                    <div class="row">
                                        <div class="col-sm-12 pl-5 pr-5">
                                            {{ Form::label('treatments', 'Select services you provide:', ['class' => 'pl-10']) }}
                                            {{ Form::select('treatments[]', $treatment, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple', 'id' => 'treatmentsMultiple', 'required' => '']) }}
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group text-center mt-30">
                                {{ Form::submit('next', ['class' => 'btn btn-lg btn-blue']) }}
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="clearfix mt-100"></div>

                            <div class="progress">
                                <div class="progress-bar bg-blue nborder" role="progressbar" aria-valuenow="67"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 67%;">
                                    <span class="sr-only">67% Complete</span>
                                </div>

                                <div class="step-1 active">
                                    1
                                </div>

                                <div class="step-2 active">
                                    2
                                </div>

                                <div class="step-3 active">
                                    3
                                </div>

                                <div class="step-4">
                                    4
                                </div>
                            </div>
                        </div>

                        {{ Form::close() }}

                    @endif

                    @if(($step === 3 && $supplier->signup_complete === 0) || ($step === '3' && $supplier->signup_complete === 0))

                        {{ Form::open(['url' => ['supplier' . '?step=3', $user->id], 'method' => 'PUT', 'data-parsley-validate' => '']) }}
                        <div class="col-sm-12 text-center">
                            <h2>Other Details</h2>
                            <hr>
                        </div>

                        <div class="col-sm-6 col-sm-offset-3">

                            <div class="">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6" style="padding-right: 2px !important;">
                                            <div class='input-group date' id='opening_time'>

                                                {{ Form::text('opening_time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Opening Time']) }}
                                                <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>

                                                <script type="text/javascript">
                                                    $(function () {
                                                        $('#opening_time').datetimepicker({
                                                            format: 'LT',
                                                            widgetPositioning: {
                                                                horizontal: 'left',
                                                                vertical: 'auto'
                                                            }
                                                        });
                                                    });
                                                </script>

                                            </div>
                                        </div>

                                        <div class="col-xs-6" style="padding-left: 2px !important;">
                                            <div class='input-group date' id='closing_time'>

                                                {{ Form::text('closing_time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Closing Time']) }}
                                                <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>

                                                <script type="text/javascript">
                                                    $(function () {
                                                        $('#closing_time').datetimepicker({
                                                            format: 'LT',
                                                            widgetPositioning: {
                                                                horizontal: 'right',
                                                                vertical: 'auto'
                                                            }
                                                        });
                                                    });
                                                </script>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group bg-white pl-10 pr-10 pb-10" style="height: 150px;">
                                    <div class="row">
                                        <div class="col-sm-12 pl-5 pr-5">
                                            {{ Form::label('availability', 'Availability:', ['class' => 'pl-10']) }}
                                            {{ Form::select('days[]', $day, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple', 'id' => 'timesMultiple', 'required' => '']) }}
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group bg-white pb-5">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-5">
                                            {{ Form::label('work_eligibility', 'Work Eligibility', ['class' => 'pl-10 pt-10']) }}
                                        </div>
                                        <div class="col-sm-6 col-xs-7">
                                            <div class="radio-wrap">
                                                <span class="radio-label">Yes</span> {{ Form::radio('work_eligibility', '1', true) }}
                                            </div>

                                            <div class="radio-wrap">
                                                <span class="radio-label">No</span> {{ Form::radio('work_eligibility', '0') }}
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row mt-10">
                                        <div class="col-sm-12">
                                            <p class="pl-10 pr-10 small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam, consequatur cumque dolor ducimus eum eveniet facilis fuga, laudantium modi nisi non obcaecati odio quae, quasi sapiente sint voluptate voluptates?</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group bg-white pt-5 pb-20">
                                    <label class="pl-10">Referee 1</label>

                                    <div class="row">
                                        <div class="col-xs-6 pr-5 pl-25 ">
                                            {{ Form::text('referee1_name', null, ['class' => 'form-control npleft npright bb-beige', 'placeholder' => 'Full Name', 'required' => '']) }}

                                        </div>

                                        <div class="col-xs-6 pl-5 pr-25">

                                            {{ Form::text('referee1_contact_number', null, ['class' => 'form-control npleft npright bb-beige', 'placeholder' => 'Contact Number', 'required' => '']) }}

                                        </div>
                                    </div>

                                    <div class="row mt-10">
                                        <div class="col-sm-12 pl-25 pr-25">

                                            {{ Form::text('referee1_email', null, ['class' => 'form-control npleft npright bb-beige', 'placeholder' => 'Email Address', 'required' => '']) }}

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group bg-white pt-5 pb-20">
                                    <label class="pl-10">Referee 2</label>

                                    <div class="row">
                                        <div class="col-xs-6 pr-5 pl-25 ">
                                            {{ Form::text('referee2_name', null, ['class' => 'form-control npleft npright bb-beige', 'placeholder' => 'Full Name', 'required' => '']) }}

                                        </div>

                                        <div class="col-xs-6 pl-5 pr-25">

                                            {{ Form::text('referee2_contact_number', null, ['class' => 'form-control npleft npright bb-beige', 'placeholder' => 'Contact Number', 'required' => '']) }}

                                        </div>
                                    </div>

                                    <div class="row mt-10">
                                        <div class="col-sm-12 pl-25 pr-25">

                                            {{ Form::text('referee2_email', null, ['class' => 'form-control npleft npright bb-beige', 'placeholder' => 'Email Address', 'required' => '']) }}

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            {{ Form::label('signup_complete', '') }}
                                        </div>
                                        <div class="col-sm-6">
                                            {{ Form::radio('signup_complete', '1', true) }}
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="form-group text-center mt-40">
                                {{ Form::submit('next', ['class' => 'btn btn-lg btn-blue']) }}
                            </div>

                        </div>

                        <div class="clearfix mb-50"></div>

                        <div class="col-sm-12">
                            <div class="progress">
                                <div class="progress-bar bg-blue nborder" role="progressbar" aria-valuenow="100"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span class="sr-only">100% Complete</span>
                                </div>

                                <div class="step-1 active">
                                    1
                                </div>

                                <div class="step-2 active">
                                    2
                                </div>

                                <div class="step-3 active">
                                    3
                                </div>

                                <div class="step-4 active">
                                    4
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}

                    @endif

                    @if($supplier->signup_complete === 1 && $supplier->active === 0)

                        <div class="col-sm-6 col-sm-offset-3 text-center mb-100">
                            <div id="stage-complete" class="well bg-transparent">
                                <i class="fa fa-check"></i>
                                <h1 class="text-uppercase">You have created your account</h1>
                                <p>We will review your application and get back to you asap</p>
                            </div>

                            <a href="{{ route('/') }}" class="btn btn-blue btn-lg mt-20 text-uppercase">Return Home</a>
                        </div>

                        <div class="clearfix mb-0"></div>

                        <div class="col-sm-12">
                            <div class="progress">
                                <div class="progress-bar bg-blue nborder" role="progressbar" aria-valuenow="100"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span class="sr-only">100% Complete</span>
                                </div>

                                <div class="step-1 active">
                                    1
                                </div>

                                <div class="step-2 active">
                                    2
                                </div>

                                <div class="step-3 active">
                                    3
                                </div>

                                <div class="step-4 active">
                                    <i class="fa fa-check"></i>
                                </div>
                            </div>
                        </div>

                    @endif

                    @if($supplier->signup_complete === 1 && $supplier->active === 1)

                        <div class="col-sm-6 col-sm-offset-3 text-center mt-50 mb-100">
                            <div id="stage-complete" class="well bg-transparent">
                                <i class="fa fa-check"></i>
                                <h1 class="text-uppercase">You are already registered</h1>
                                <p>Please continue and access your account.</p>
                            </div>

                            <a href="{{ route('supplier') }}" class="btn btn-blue btn-lg">View Account</a>
                        </div>

                    @endif

                @endif

            </div>
        </div>
    </section>

@endsection

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}

    <script type="text/javascript">
        $(".select2-multi").select2({
            width: '100%',
            placeholder: 'Select all options'
        });
    </script>
@endsection
