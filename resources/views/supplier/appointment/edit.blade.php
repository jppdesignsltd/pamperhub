@extends('layouts.supplier')

@section('title', 'Edit Appointment')

@section('stylesheets')

@endsection

@section('header-scripts')
@endsection

@section('heading', 'Edit Appointment')

@section('content')

    <section class="nptop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">

                    @include('partials.supplier._nav')

                </div>

                {{ Form::model($appointment, ['route' => ['booking.update', $appointment->id], 'method' => 'PUT', 'id' => 'editAppointment', 'data-parsley-validate' => '']) }}

                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                    <div class="row">
                        <div class="col-md-7 {{--pl-30 pr-30--}}">
                            <div class="service_post nb-radius" style="height: auto !important;">

                                <div class="service_post-container nb-radius" style="overflow: visible !important;">
                                    <div class="service_post-header">
                                        <img
                                                @if($appointment->service->cover_image !== '')
                                                src="{{ asset('uploads/services/' . $appointment->service->cover_image) }}"
                                                alt=""
                                                @else
                                                src="{{ asset('uploads/services/bg-' . strtolower($appointment->service->treatment->name) . '.jpg') }}"
                                                onerror="this.src='{{ asset('dist/img/default-service-bg.png') }}'"
                                                alt=""
                                                @endif

                                        >
                                        <div class="image-overlay"></div>
                                        <h3>
                                            {{ $appointment->service->supplier->company }}
                                            {{ Form::text('company', $appointment->service->supplier->company, ['class' => 'hidden', 'readonly' => '']) }}
                                            {{ Form::text('service_title', $appointment->service->title, ['class' => 'hidden', 'readonly' => '']) }}
                                            {{ Form::number('service_id', $appointment->service->id, ['class' => 'hidden', 'readonly' => '']) }}
                                            <div class="clearfix"></div>
                                            <span class="small">
                                        {{ $appointment->service->supplier->city }}
                                    </span>
                                        </h3>

                                        {{--<p class="availability">--}}
                                            {{--<span class="clearfix"></span>--}}
                                            {{--<span class="text-bold mr-5 small">Open:</span>--}}
                                            {{--@foreach($appointment->service->supplier->days as $dayName)--}}
                                                {{--<span class="label label-xs bg-white text-dark-gray">{{ $dayName->name }}</span>--}}
                                            {{--@endforeach--}}
                                        {{--</p>--}}

                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="service_post-body pb-20">
                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="service" class="nptop">
                                                        Service Type:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p class="nmbottom">
                                                        {{ $appointment->treatment->name }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="service" class="nptop">
                                                        Price (p/h):
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p class="nmbottom">&pound;{{ $appointment->service->price }}</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="duration" class="pt-5">
                                                        Duration:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">

                                                    <p class="nmbottom pt-5">
                                                        {{ $appointment->duration }}{{ $appointment->duration === '1' ? 'hr' : 'hrs' }}
                                                    </p>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group mb-10">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="date" class="">
                                                        Date:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p id="date_placeholder" class="nmbottom pt-5">
                                                        {{ date('jS, F Y', strtotime($appointment->date)) }}

                                                        {{--<a onclick="revealInput(date_placeholder, date)">--}}
                                                            {{--<i class="fa fa-pencil ml-10"></i>--}}
                                                        {{--</a>--}}
                                                    </p>

                                                    <div id="date" class='input-group date hidden'>

                                                        {{ Form::text('date', null, ['class' => 'form-control bg-white', 'disabled' => '', 'placeholder' => 'Choose Date']) }}

                                                        <span class="input-group-addon nb-radius">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>

                                                    </div>

                                                    <script type="text/javascript">
                                                        $(function () {

                                                            var today = new Date();

                                                            var year = today.getFullYear();
                                                            var month = today.getMonth();
                                                            var date = today.getDate();
                                                            var dateArray = [];
                                                            var deleteArray = [];
                                                            var startDate;
                                                            var endDate;
                                                            var dateMove;
                                                            var disableHoliday;

                                                            for (var j = 0; j < 30; j++) {
                                                                var day = new Date(year, month, date + j);

                                                                dateArray.push(day);
                                                            }

//                                                            console.log(dateArray);

                                                            var supplierDays = [];
                                                            var disabledDays = [0, 1, 2, 3, 4, 5, 6];

                                                            @foreach($appointment->service->supplier->days as $day)
                                                                supplierDays.push({{ $day->time_ref }});
                                                            @endforeach

                                                            @if($holidays)
                                                                @foreach($holidays as $holidayDate)
                                                                    startDate = "{{ $holidayDate->date_from }}";
                                                                    endDate = "{{ $holidayDate->date_to }}";
                                                                    dateMove = new Date(startDate);


                                                                    while (startDate < endDate) {
                                                                        startDate = dateMove.toISOString().slice(0, 10);
                                                                        disableHoliday = new Date(startDate).setHours(0, 0, 0, 0);
                                                                        deleteArray.push(disableHoliday);
                                                                        dateMove.setDate(dateMove.getDate() + 1);
                                                                    }
                                                                @endforeach

                                                                var l = deleteArray.length;
                                                                while (l--) {

                                                                    var dh = deleteArray.indexOf(deleteArray[l]);

                                                                    if (dh !== -1) {
                                                                        console.log('Delete input');
                                                                        dateArray.splice(dh, 1);
                                                                    }
                                                                }

                                                            @endif

//                                                            console.log(dateArray);
//                                                            console.log(deleteArray);

                                                            for (var i = supplierDays.length - 1; i >= 0; i--)
                                                                disabledDays.splice(disabledDays.indexOf(supplierDays[i]), 1);

                                                            $('#date').datetimepicker({
                                                                daysOfWeekDisabled: disabledDays,
                                                                useCurrent: false,
                                                                enabledDates: dateArray,
                                                                viewMode: 'days',
                                                                format: 'DD/MM/YYYY'
                                                            });
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="time" class="pt-5">
                                                        Time:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p id="time_placeholder" class="nmbottom pt-5 text-capitalize">


                                                        @if($appointment->time)
                                                            {{ date('h:i A', strtotime($appointment->time)) }}

                                                            <a onclick="revealInput(time_placeholder, time)">
                                                                <i class="fa fa-pencil ml-10"></i>
                                                            </a>
                                                        @endif
                                                    </p>

                                                    <div class='input-group date {{ !$appointment->time ? '' : 'hidden' }}' id='time'>

                                                        {{ Form::text('time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Opening Time']) }}
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>

                                                        <script type="text/javascript">
                                                            $(function () {
                                                                var timeArray = [];

                                                                @if($appointment->time_of_day === 'morning')
                                                                    timeArray = [7,8,9,10,11,12];
                                                                @elseif($appointment->time_of_day === 'afternoon')
                                                                    timeArray = [13,14,15,16,17,18];
                                                                @elseif($appointment->time_of_day === 'evening')
                                                                    timeArray = [19,20,21,22];
                                                                @endif

                                                                console.log(timeArray);

                                                                $('#time').datetimepicker({
                                                                    format: 'LT',
                                                                    enabledHours: timeArray,
                                                                    widgetPositioning: {
                                                                        horizontal: 'left',
                                                                        vertical: 'auto'
                                                                    }
                                                                });
                                                            });
                                                        </script>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group nmbottom">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="time_of_day" class="pt-5">
                                                        Time of Day:
                                                    </label>
                                                </div>

                                                <div class="col-xs-6">
                                                    <p id="time_of_day_placeholder" class="nmbottom pt-5 text-capitalize">
                                                        {{ $appointment->time_of_day }}

                                                        @if(!count($tod) >= 3)
                                                            <a onclick="revealInput(time_of_day_placeholder, time_of_day)">
                                                                <i class="fa fa-pencil ml-10"></i>
                                                            </a>
                                                        @endif
                                                    </p>

                                                    <select name="time_of_day" id="time_of_day"
                                                            class="form-control select2-standard hidden"
                                                            disabled>
                                                        <option value="" disabled selected>Please Choose</option>
                                                        @if(!in_array("morning", $tod))
                                                            <option value="morning">Morning</option>
                                                        @endif

                                                        @if(!in_array("afternoon", $tod))
                                                            <option value="afternoon">Afternoon</option>
                                                        @endif

                                                        @if(!in_array("evening", $tod))
                                                            <option value="evening">Evening</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <p class="mb-20">
                                You can only change the time of day of any appointment.
                            </p>

                            <a href="{{ route('supplier.appointments') }}" class="btn btn-sm btn-beige pull-right">
                                back
                            </a>


                            <a data-toggle="modal" data-target="#appointment-edit-modal"
                               class="btn btn-blue btn-sm pull-left">
                                save

                            </a>

                        </div>

                        <div class="clearfix"></div>


                    </div>


                </div>
            </div>

        </div>
    </section>

    <!-- Modal -->
    <div id="appointment-edit-modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="appointment-edit-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pt-30 pb-30">

                    <h3 class="text-danger">Are you sure?</h3>

                    <p>
                        Once this has been saved and changed all parties will be notified via email.
                    </p>


                    <div class="clearfix"></div>

                    {{ Form::submit('save', ['class' => 'btn btn-md btn-blue mt-10']) }}

                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

@stop

@section('scripts')
@endsection
