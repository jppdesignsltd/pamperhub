@extends('layouts.supplier')

@section('title', 'My Appointments')

@section('stylesheets')
@endsection

@section('header-scripts')
@endsection

@section('heading', 'My Appointments')

@section('content')

    {{ Session::forget('error') }}

    <section class="nptop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">

                    @include('partials.supplier._nav')

                </div>

                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                    @if($appointments->count() === 0)

                        <p>
                            You do not have any appointments booked.
                        </p>

                    @else

                        <h4 class="mb-20">Your next appointment:</h4>

                        <div class="appointment bg-light-gray nborder">
                            <div class="row">
                                <div class="appointment-date">
                                <span class="day">
                                    {{ date('j M' , strtotime($next_appointment->date)) }}
                                </span>
                                    <hr>
                                    <span class="month">
                                    {{ date('Y' , strtotime($next_appointment->date)) }}
                                </span>
                                </div>

                                <div class="appointment-meta">
                                    <a href="{{ route('supplier.appointment.show', $next_appointment->id) }}">
                                        <i class="fa fa-eye mr-5"></i>
                                        <span>
                                            #ORD-0{{ $next_appointment->id }}
                                            </span>
                                    </a>
                                </div>

                                <div class="col-sm-8 col-xs-8">
                                    <p>
                                        <strong>Customer Name:</strong>
                                        <span class="clearfix visible-xs"></span>
                                        {{ $next_appointment->customer->user->firstname }}
                                        {{ $next_appointment->customer->user->lastname }}

                                        (#00{{ $next_appointment->customer->id }})
                                    </p>
                                    <p>
                                        <strong>Duration:</strong>
                                        {{ $next_appointment->duration }}{{ $next_appointment->duration === '1' ? 'hr' : 'hrs' }}
                                    </p>
                                    <p>
                                        <strong>Service:</strong>
                                        {{ $next_appointment->service->treatment->name }}
                                    </p>

                                    @if($next_appointment->time)
                                        <p>
                                            <strong>Time:</strong>
                                            {{ date('h:i A', strtotime($next_appointment->time)) }}
                                        </p>
                                    @endif

                                    <p class="nmbottom">
                                        <strong>Location:</strong>
                                        <span class="clearfix"></span>
                                        {{ $next_appointment->customer->address1 }},
                                        {{ $next_appointment->customer->address2 }}{{ $next_appointment->customer->address2 !== null ? ',' : '' }}
                                        {{ $next_appointment->customer->city }},
                                        {{ $next_appointment->customer->postal_code }}

                                        -

                                        <a class="" target="_blank"
                                           href="https://www.google.co.uk/maps/place/{{ $next_appointment->customer->city }}+{{ $next_appointment->customer->postal_code }}">
                                            <i class="fa fa-map-o"></i>
                                            (view on map)
                                        </a>

                                    </p>

                                    @if(!$next_appointment->time)
                                        <div class="alert alert-danger  mt-20 nmbottom pl-10 pr-10 pt-10 pb-10">
                                            <p>
                                                There is no time added to this booking.

                                                <a href="{{ route('supplier.appointment.edit', $next_appointment->id) }}" class="pull-right btn btn-sm btn-danger text-white">Update Now</a>
                                            </p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="clearfix mb-10"></div>

                        @if($page === null || $page === 1)


                            @if($appointments->count() === 1)

                            @else

                                <h4 class="mb-20 nmtop">Upcoming Appointment:</h4>

                            @endif

                        @elseif($page >= 1)

                            <h4 class="mb-20 nmtop">Upcoming Appointment:</h4>

                        @endif

                        <div class="appointment-list">
                            @foreach($appointments as $appointment)

                                <div class="appointment bg-white {{ $next_appointment->id === $appointment->id ? 'hidden' : '' }}">
                                    <div class="row">
                                        <div class="appointment-date">
                                        <span class="day">
                                            {{ date('j M' , strtotime($appointment->date)) }}
                                        </span>
                                            <hr>
                                            <span class="month">
                                            {{ date('Y' , strtotime($appointment->date)) }}
                                        </span>
                                        </div>

                                        <div class="appointment-meta">
                                            <a href="{{ route('supplier.appointment.show', $appointment->id) }}">
                                                <i class="fa fa-eye mr-5"></i>
                                                <span>
                                            #ORD-0{{ $appointment->id }}
                                            </span>
                                            </a>
                                        </div>

                                        <div class="col-sm-8 col-xs-8">
                                            <p>
                                                <strong>Customer Name:</strong>
                                                <span class="clearfix visible-xs"></span>
                                                {{ $appointment->customer->user->firstname }}
                                                {{ $appointment->customer->user->lastname }}

                                                (#00{{ $appointment->customer->id }})
                                            </p>
                                            <p>
                                                <strong>Duration:</strong>
                                                {{ $appointment->duration }}{{ $appointment->duration === '1' ? 'hr' : 'hrs' }}
                                            </p>
                                            <p>
                                                <strong>Service:</strong>
                                                {{ $appointment->treatment->name }}
                                            </p>

                                            @if($appointment->time)
                                                <p>
                                                    <strong>Time:</strong>
                                                    {{ date('h:i A', strtotime($appointment->time)) }}
                                                </p>
                                            @endif

                                            <p class="nmbottom">
                                                <strong>Location:</strong>
                                                <span class="clearfix"></span>
                                                {{ $appointment->customer->address1 }},
                                                {{ $appointment->customer->address2 }}{{ $appointment->customer->address2 !== null ? ',' : '' }}
                                                {{ $appointment->customer->city }},
                                                {{ $appointment->customer->postal_code }}

                                                -

                                                <a class="" target="_blank"
                                                   href="https://www.google.co.uk/maps/place/{{ $appointment->customer->city }}+{{ $appointment->customer->postal_code }}">
                                                    <i class="fa fa-map"></i>
                                                    (view on map)
                                                </a>
                                            </p>

                                            @if(!$appointment->time && $appointment->canceled !== 1)
                                                <div class="alert alert-danger mt-20 nmbottom pl-10 pr-10 pt-10 pb-10">
                                                    <p>
                                                        There is no time added to this booking.

                                                        <a href="{{ route('supplier.appointment.edit', $appointment->id) }}" class="pull-right btn btn-sm btn-danger text-white">Update Now</a>
                                                    </p>
                                                </div>
                                            @elseif($appointment->canceled === 1)
                                                <div class="alert alert-warning mt-20 nmbottom pl-10 pr-10 pt-10 pb-10">
                                                    <p>
                                                        This appointment has been canceled. Please get in touch with a member of the PamperHub team. (support@pamperhub.com).
                                                    </p>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            @endforeach
                        </div>


                        <div class="clearfix"></div>

                        <div class="pull-left pt-5">
                            Page <strong>{!! $appointments->currentPage() !!}</strong>
                            of {!! $appointments->lastPage() !!}
                        </div>

                        <div class="pull-right">
                            {!! $appointments->links() !!}
                        </div>

                    @endif
                </div>
            </div>

        </div>
    </section>

@stop

@section('scripts')
@endsection
