@extends('layouts.supplier')

@section('title', 'Preview Appointment')

@section('stylesheets')
@endsection

@section('header-scripts')
@endsection

@section('heading', 'Preview Appointment')

@section('content')

    {{ Session::forget('error') }}

    <section class="nptop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">

                    @include('partials.supplier._nav')

                </div>

                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                    <div class="row">

                        @if($appointment->canceled === 1)
                            <div class="alert alert-danger">
                                This appointment is canceled. Please email support@pamperhub.com for more information.
                            </div>
                        @elseif(!$appointment->time)
                            <div class="alert alert-warning">
                                This appointment has no time associated to it. Before it is confirmed you must add a time.
                            </div>
                        @endif


                        <div class="">
                            <h4>Customer Details</h4>

                            <div class="table-responsive">
                                <table id="customers-index" class="table table-bordered">
                                    <thead class="bg-light-gray">
                                    <th>Customer No.</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Address</th>
                                    </thead>

                                    <tbody>

                                    <tr>
                                        <td>
                                            <strong>
                                                #00{{ $appointment->customer->id }}
                                            </strong>

                                        </td>
                                        <td>
                                            {{ $appointment->customer->user->firstname }}
                                        </td>
                                        <td>
                                            {{ $appointment->customer->user->lastname }}
                                        </td>
                                        <td>
                                            {{ $appointment->customer->address1 }},
                                            {{ $appointment->customer->address2 }}{{ $appointment->customer->address2 === '' ? ',' : ''}}
                                            {{ $appointment->customer->city }},
                                            {{ $appointment->customer->postal_code }}
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <div class="row mt-20">
                        <div class="">
                            <h4>Booking Details</h4>

                            <div class="table-responsive">
                                <table id="customers-index" class="table table-bordered">
                                    <thead class="bg-light-gray">
                                    <th style="width: 110px;">Order No.</th>
                                    <th>Duration</th>
                                    <th>Total Price</th>
                                    <th>Date</th>

                                    @if($appointment->time)
                                        <th>Time</th>
                                    @else
                                        <th>Time of Day</th>
                                    @endif

                                    <th>Paid</th>
                                    <th>Payment Method</th>
                                    </thead>

                                    <tbody>

                                    <tr>
                                        <td>
                                            <strong>
                                                #ORD-0{{ $appointment->id }}
                                            </strong>
                                        </td>
                                        <td>
                                            {{ $appointment->duration > 1 ? "$appointment->duration hrs" : "$appointment->duration hr" }}
                                        </td>
                                        <td>
                                            &pound;{{ $appointment->price }}
                                        </td>
                                        <td>
                                            {{ date('jS F, Y' , strtotime($appointment->date)) }}
                                        </td>
                                        @if($appointment->time)

                                            <td style="width: 120px">
                                                {{ date('h:i A', strtotime($appointment->time)) }}
                                            </td>

                                        @else

                                            <td style="width: 120px">
                                                {{ ucfirst($appointment->time_of_day) }}
                                            </td>

                                        @endif
                                        <td>
                                            @if($appointment->canceled)
                                                <span class="label label-sm label-danger">Canceled</span>

                                            @elseif($appointment->payment_received === 0)
                                                <span class="label label-sm label-danger">No</span>

                                            @else
                                                <span class="label label-sm label-success">Yes</span>
                                            @endif
                                        </td>
                                        <td>
                                            PayPal
                                        </td>
                                    </tr>

                                    </tbody>

                                    <tr class="clearfix"></tr>

                                    <thead class="bg-light-gray">
                                    <th style="width: 110px;">Supplier ID</th>
                                    <th>Service Name</th>
                                    <th style="width: 130px">Treatment</th>
                                    <th>Supplier</th>
                                    <th>Contact No.</th>
                                    <th colspan="2">Address</th>
                                    </thead>

                                    <tbody>

                                    <tr>
                                        <td>
                                            <strong>#SUP-0{{ $appointment->supplier->id }}</strong>
                                        </td>
                                        <td>{{ $appointment->service->title }}</td>
                                        <td>{{ $appointment->treatment->name }}</td>
                                        <td>{{ $appointment->supplier->company }}</td>
                                        <td>{{ $appointment->supplier->contact_number }}</td>
                                        <td colspan="2">
                                            {{ $appointment->supplier->address1 }},
                                            {{ $appointment->supplier->address2 }}{{ $appointment->supplier->address2 === '' ? '' : ','}}
                                            {{ $appointment->supplier->city }},
                                            {{ $appointment->supplier->postal_code }}
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <a href="{{ route('supplier.appointments') }}" class="btn btn-md btn-beige mt-10 pull-left">
                            back
                        </a>

                        @if($appointment->canceled !== 1)
                            <a href="{{ route('supplier.appointment.edit', $appointment->id) }}" class="btn btn-md btn-blue mt-10 pull-right">edit</a>
                        @endif

                    </div>

                </div>
            </div>

        </div>
    </section>

@stop

@section('scripts')
@endsection
