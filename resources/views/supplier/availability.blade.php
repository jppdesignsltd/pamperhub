@extends('layouts.supplier')

@section('title', 'Supplier Availability')

@section('stylesheets')
@endsection

@section('header-scripts')
@endsection

@section('heading', 'My Availability')

@section('content')

    <section class="nptop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">

                    @include('partials.supplier._nav')

                </div>


                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                    {!! Form::model($supplier, ['url' => ['supplier' . '?page=availability', $supplier->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'id' => 'supplierAvailability']) !!}

                    <p>
                        Let customers know what dates, and times you can work.
                    </p>

                    <div class="well mt-20">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    {{ Form::label('opening_time', 'Opening Time:', ['class' => 'nptop']) }}

                                    <div class='input-group date' id='opening_time'>

                                        {{ Form::text('opening_time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Opening Time', 'disabled' => '', 'required' => '']) }}
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>

                                        <script type="text/javascript">
                                            $(function () {
                                                $('#opening_time').datetimepicker({
                                                    format: 'LT'
                                                });
                                            });
                                        </script>

                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    {{ Form::label('closing_time', 'Closing Time:', ['class' => 'nptop']) }}

                                    <div class='input-group date' id='closing_time'>

                                        {{ Form::text('closing_time', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Closing Time', 'disabled' => '', 'required' => '']) }}
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>

                                        <script type="text/javascript">
                                            $(function () {
                                                $('#closing_time').datetimepicker({
                                                    format: 'LT'
                                                });
                                            });
                                        </script>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    {{ Form::label('days', 'Days of Operation:') }}
                                    {{ Form::select('days[]', $day, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple', 'id' => 'timesMultiple', 'disabled' => '', 'required' => '']) }}
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <a id="editBtn" onclick="activateSubmit('supplierAvailability', 'editBtn')"
                                   class="btn btn-md btn-beige">Edit</a>
                                {{ Form::submit('save', ['class' => 'btn btn-md btn-blue hidden', 'id' => 'supplierAvailabilityBtn']) }}

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>

        </div>


    </section>

@stop

@section('scripts')
    <script type="text/javascript">
        $(".select2-multi").select2({
            width: '100%',
        });

        $(".select2-multi").select2().val( {!! json_encode($supplier->days()->allRelatedIds()) !!} ).trigger('change');
    </script>
@endsection
