@extends('layouts.admin')

@section('title', 'Tags')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-tags mr-10"></i> Tags
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $tags->count() === 1 ? 'is' : 'are' }} <span class="badge">{{ $tags->count() }}</span> tag{{ $tags->count() === 1 ? '' : 's' }} added.</h5>

        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-6">
                    @if($tags->count() === 0)

                        <p>You do not have any tags created.</p>

                    @else

                        <div class="panel panel-body panel-default npadding">
                            <div class="table-responsive">
                                <table id="tags-index" class="table table-bordered">
                                    <thead class="bg-blue nb-radius">
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th class="nbright" style="width:120px"></th>
                                    </thead>

                                    <tbody>
                                    @foreach($tags as $tag)

                                        <tr id="service-{{ $tag->id }}">
                                            <td>
                                                <span class="">{{ $tag->name }}</span>
                                            </td>
                                            <td>
                                                <span class="">{{ $tag->slug }}</span>
                                            </td>
                                            <td class="text-right">
                                                <a href="{{ route('tag.edit', $tag->id) }}" class="text-dark-gray small">edit</a>
                                                @if(Entrust::hasRole('super-admin'))
                                                    |
                                                    <a role="button" data-toggle="collapse" href="#verifyDelete-{{ $tag->id }}" aria-expanded="false" aria-controls="verifyDelete-{{ $tag->id }}" data-parent="#tags-index" class="text-danger small">[delete]</a>
                                                @endif
                                            </td>
                                        </tr>

                                        <tr class="bg-danger npadding">
                                            <td class="npadding bt-dark-gray" colspan="3">
                                                <div id="verifyDelete-{{ $tag->id }}" class="collapse">
                                                    <div class="pl-10 pt-10 pb-10 pr-10 text-center">
                                                        <p class="medium">Are you sure you want to delete this tag? This is permanent.</p>
                                                        {!! Form::open(['route' => ['tag.destroy', $tag->id], 'method' => 'DELETE']) !!}
                                                        {{ Form::button('<i class="fa fa-trash"></i> delete', ['class' => 'btn btn btn-danger btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    @endif



                </div>

                <div class="col-lg-5 col-lg-offset-1 col-sm-6">
                    @if(Entrust::hasRole('super-admin') || Entrust::hasRole('customer-service-rep'))
                        {{ Form::open(['route'=> ['tag.store'], 'data-parsley-validate' => '']) }}

                        <div class="well bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('name', 'Tag Name', ['class' => ' text-left control-label']) }}
                                    </div>
                                    <div class="col-sm-8">
                                        {{ Form::text('name', null, ['class' => 'form-control', 'required' => '', 'ng-model' => 'tagSlug']) }}
                                    </div>
                                    {{ Form::text('slug', $data['slug'], ['class' => 'hidden']) }}
                                </div>
                            </div>
                        </div>

                        {{ Form::submit('create new', ['class' => 'btn btn-blue btn-md']) }}
                        {{ Form::close() }}
                    @else
                        <p>
                            You need to be an admin to create a new treatment type.
                        </p>
                    @endif


                </div>
            </div>

        </div>
    </section>


@endsection

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {!! Html::script('bower_components/angular/angular.min.js') !!}
    {!! Html::script('bower_components/angular-slugify/angular-slugify.js') !!}
    {!! Html::script('dist/js/supplierCreate.js') !!}
@endsection