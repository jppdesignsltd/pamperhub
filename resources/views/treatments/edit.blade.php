{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Edit Treatments')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-bookmark mr-10"></i> Treatments
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $treatments->count() === 1 ? 'is' : 'are' }} <span class="badge">{{ $treatments->count() }}</span> treatment{{ $treatments->count() === 1 ? '' : 's' }} added. Only Admins can add new ones.</h5>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            @if($treatments->count() === 0)
                <div class="row">
                    <div class="col-sm-12">
                        <p>You have not added any treatments.</p>
                    </div>
                </div>

            @else
                <div class="row">

                    <div class="col-sm-8">
                        {{ Form::model($treatment, ['route'=> ['treatment.update', $treatment->id], 'data-parsley-validate' => '', 'method' => 'PUT']) }}
                            <div class="well bg-light-gray">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            {{ Form::label('name', 'Treatment Name', ['class' => ' text-left control-label']) }}
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::text('name', null, ['class' => 'form-control', 'required' => '']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{ Form::submit('update treatment', ['class' => 'btn btn-blue btn-md']) }}

                        {{ Form::close() }}
                    </div>

                </div>
            @endif

            <div class="row mt-30">
                <div class="col-sm-12">
                    {{--<a href="{{URL::previous()}}" class="btn btn-md btn-beige">back</a>--}}
                </div>
            </div>
        </div>
    </section>


@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
@endsection
