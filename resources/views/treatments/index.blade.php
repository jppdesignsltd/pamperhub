{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Treatments')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-bookmark mr-10"></i> Treatments
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $treatments->count() === 1 ? 'is' : 'are' }} <span class="badge">{{ $treatments->count() }}</span> treatment{{ $treatments->count() === 1 ? '' : 's' }} added.</h5>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-6">
                    <div class="panel panel-body panel-default npadding">
                        <div class="table-responsive">
                            <table id="treamtments-index" class="table table-bordered">
                                <thead class="bg-blue nb-radius">
                                    <th>Name</th>
                                    <th># Suppliers Associated</th>
                                    <th class="nbright" style="width: 110px;"></th>
                                </thead>

                                <tbody>
                                @foreach($treatments as $treatment)

                                    <tr id="service-{{ $treatment->id }}">
                                        <td>
                                            <span class="">{{ $treatment->name }}</span>
                                        </td>
                                        <td>
                                            {{ $treatment->suppliers()->count() }}
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('treatment.edit', $treatment->id) }}" class="text-dark-gray small">edit</a>
                                            @if($treatment->suppliers()->count() === 0)
                                                |
                                                <a role="button" data-toggle="collapse" href="#verifyDelete-{{ $treatment->id }}" aria-expanded="false" aria-controls="verifyDelete-{{ $treatment->id }}" data-parent="#treamtments-index" class="text-danger small">[delete]</a>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="bg-danger npadding">
                                        <td class="npadding bt-dark-gray" colspan="3">
                                            <div id="verifyDelete-{{ $treatment->id }}" class="collapse">
                                                <div class="pl-10 pt-10 pb-10 pr-10 text-center">
                                                    <p class="medium">Are you sure you want to delete this treatment type? This is permanent.</p>
                                                    {!! Form::open(['route' => ['treatment.destroy', $treatment->id], 'method' => 'DELETE']) !!}
                                                        {{ Form::button('<i class="fa fa-trash"></i> delete', ['class' => 'btn btn btn-danger btn-sm', 'role' => 'button', 'type' => 'submit']) }}

                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="col-lg-5 col-lg-offset-1 col-sm-6">
                    @if(Entrust::hasRole('super-admin'))
                        {{ Form::open(['route'=> ['treatment.store'], 'data-parsley-validate' => '']) }}

                        <div class="well bg-light-gray">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('name', 'Treatment Name', ['class' => ' text-left control-label']) }}
                                    </div>
                                    <div class="col-sm-7">
                                        {{ Form::text('name', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{ Form::submit('create new', ['class' => 'btn btn-blue btn-md']) }}
                        {{ Form::close() }}
                    @else
                        <p>
                            You need to be an admin to create a new treatment type.
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </section>


@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
@endsection
