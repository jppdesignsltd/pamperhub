{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Settings')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {!! Html::style('bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') !!}
@stop

@section('header-scripts')
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-cogs mr-10"></i> Settings
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
        <div class="clearfix"></div>
    </div>

    {!! Form::model($settings, ['route' => ['setting.update', $settings->id], 'method' => 'PUT', 'class' => '', 'id' => 'settingsForm', 'data-parsley-validate' => '']) !!}

    <div id="action_panel" class="bg-beige">
        <h5>Here you can update your site settings.</h5>

        {{ Form::submit('update settings', ['class'=>'btn btn-md btn-dark-gray']) }}
        <div class="clearfix"></div>
    </div>

    <section>


        <div class="container-fluid">


            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">General Settings</h3>
                        </div>
                        <div class="panel-body npleft npright bg-light-gray">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('site_name', 'Site Name') }}
                                    </div>

                                    <div class="col-sm-7">
                                        {{ Form::text('site_name', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('tagline', 'Tagline') }}
                                    </div>

                                    <div class="col-sm-7">
                                        {{ Form::text('tagline', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('home_tagline', 'Home Tagline') }}
                                    </div>

                                    <div class="col-sm-7">
                                        {{ Form::textarea('home_tagline', null, ['class' => 'form-control', 'required' => '', 'style' => 'height:100px']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('email_address', 'Default Email') }}
                                    </div>

                                    <div class="col-sm-7">
                                        {{ Form::text('email_address', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">SEO Settings</h3>
                        </div>

                        <div class="panel-body npleft npright bg-light-gray">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('default_page_author', 'Default Page Author', ['class' => '']) }}
                                    </div>

                                    <div class="col-sm-7">
                                        {{ Form::text('default_page_author', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('default_keywords', 'Default Keywords') }}
                                    </div>

                                    <div class="col-sm-7">
                                        {{ Form::textarea('default_keywords', null, ['class' => 'form-control', 'required' => '', 'style' => 'max-height:80px;', 'maxlength' => '250']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5">
                                        {{ Form::label('default_description', 'Default Description') }}
                                    </div>

                                    <div class="col-sm-7">
                                        {{ Form::textarea('default_description', null, ['class' => 'form-control', 'required' => '', 'style' => 'max-height:80px;', 'maxlength' => '160']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <h3 class="nmargin">Reading Settings</h3>
                        </div>

                        <div class="panel-body npleft npright bg-light-gray">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('breadcrumbs', 'Breadcrumbs', ['class' => 'nptop']) }}
                                    </div>

                                    <div class="col-sm-8">
                                        <label class="npadding">
                                            {{ Form::radio('breadcrumbs', 1, false, ['id' => 'breadcrumbs-on', 'data-radio-all-off' => 'true', 'data-on-color' => 'success', 'data-off-color' => 'danger', 'data-size' => 'mini']) }}
                                        </label>

                                        <label class="npleft">
                                            {{ Form::radio('breadcrumbs', 0, true, ['id' => 'breadcrumbs-off', 'class' => 'hidden']) }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('services_per_page', 'Services Per Page') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::number('services_per_page', null, ['class' => 'form-control', 'required' => '']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::label('services_excerpt', 'Services Excerpt') }}
                                    </div>

                                    <div class="col-sm-8">
                                        {{ Form::number('services_excerpt', null, ['class' => 'form-control', 'required' => '', 'min' => '50', 'max' => '200']) }}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-4 col-sm-12 hidden-xs npadding">
                            <p class="text-right">
                                <a href="{{ route('admin') }}" class="btn btn-md btn-beige ">back</a>
                            </p>
                        </div>

                        <div class="col-xs-4 col-sm-12 visible-xs npleft">
                            <p class="text-right">
                                <a href="{{ route('admin') }}" class="btn btn-md btn-beige ">back</a>
                            </p>
                        </div>
                        <div class="col-xs-8 visible-xs npright">
                            {{ Form::submit('update settings', ['class'=>'btn btn-block btn-blue ']) }}
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                
                
            </div>

        </div>

    </section>
    {{ Form::close() }}

@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}
    {!! Html::script('bower_components/bootstrap-switch/dist/js/bootstrap-switch.js') !!}

    <script type="text/javascript">
        $("#breadcrumbs-on").bootstrapSwitch();

        $('#breadcrumbs-on').on('switchChange.bootstrapSwitch', function (event, state) {
            if(state === true) {
                $('#breadcrumbs-on').prop("checked", true);
                $('#breadcrumbs-off').prop("checked", false);
                console.log(state);
            } else {
                $('#breadcrumbs-on').prop("checked", false);
                $('#breadcrumbs-off').prop("checked", true);
                console.log(state);
            }
        });

//        $("#service_type").select2({
//            width: '100%'
//        });

{{--        $("#service_type").select2().val( {!! json_encode($service->service_type) !!} ).trigger('change');--}}

    </script>
@endsection
