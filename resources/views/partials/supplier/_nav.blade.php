

<div class="well text-center pl-10 pr-10">
    <div class="img-wrap">


        <div id="" class=" text-left">
            @if($supplier->display_picture === null || $supplier->display_picture === '')
                <img id="image_preview" src="{{ asset('dist/img/default_user.png') }}" alt="Supplier Image" class="img-responsive">
            @else
                <a onclick="clearFilestyle()" class="text-bold"><i class="fa fa-times text-red"></i></a>
                <div class="clearfix mb-5"></div>

                <img id="image_preview" src="{{ asset('uploads/suppliers/' . $supplier->display_picture) }}" onerror="this.src='{{ asset('dist/img/default_user.png') }}'" alt="Supplier Image" class="img-responsive">
            @endif
        </div>


    </div>
    <div class="clearfix mb-20"></div>
    <a id="editBtnImage" onclick="activateImgForm('supplierImg', 'editBtnImage')"
       class="btn btn-md btn-clear text-underline text-bold">change image</a>

    {!! Form::model($supplier, ['url' => ['supplier' . '?page=image', $supplier->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'id' => 'supplierImg', 'files' => true, 'class' => 'hidden']) !!}
    <div class="form-group">
        <div class="row">
            <div id="cover_image-cont" class="col-sm-12">
                {{ Form::file('display_picture', ['id' => 'display_picture', 'class' => 'filestyle','data-buttonName' => '', 'data-input' => 'true', 'data-placeholder' => 'No file chosen', 'data-buttonName' => 'btn-beige-plain',  'onchange' => 'loadFile(event)']) }}
            </div>
            <div class="clearfix mb-10"></div>

            <div class="col-sm-12">
                {{ Form::button('<i class="fa fa-save mr-10"></i> save', ['type' => 'submit', 'class' => 'btn btn-md text-bold bg-blue pt-5 pb-5 btn-block btn-clear']) }}
            </div>

        </div>

        <div class="row">

            <div class="clearfix"></div>



        </div>
    </div>
    {{ Form::close() }}
</div>

<ul class="nav nav-pills nav-stacked mb-20">
    <li class="{{ Request::is('supplier') ? "active" : "" }}"><a href="{{ route('supplier') }}">basic information</a></li>
    <li class="{{ Request::is('supplier/availability*') ? "active" : "" }}"><a href="{{ route('supplier.availability') }}">availability</a></li>
    <li class="{{ Request::is('supplier/holidays*') ? "active" : "" }}"><a href="{{ route('supplier.holiday') }}">holiday/sickness</a></li>
    <li class="{{ Request::is('supplier/services*') ? "active" : "" }}"><a href="{{ route('supplier.services') }}">service listings</a></li>
    <li class="{{ Request::is('supplier/appointments*') ? "active" : "" }} {{ Request::is('supplier/appointment*') ? "active" : "" }}"><a href="{{ route('supplier.appointments') }}">my appointments</a></li>
</ul>

<hr class="visible-xs">