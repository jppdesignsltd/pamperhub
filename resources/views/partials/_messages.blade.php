@if (Session::has('success'))
    <div class="alert_messages success_message">
        <div class="alert alert-success nmbottom" role="alert">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>Success:</strong> {{ Session::get('success') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if (Session::has('error'))
    <div class="alert_messages error_message">
        <div class="alert alert-danger nmbottom" role="alert">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>Whoops!</strong>
                        {{ Session::get('error') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if (count($errors) > 0)
    <div class="error_messages">
        <div class="alert alert-danger nmbottom" role="alert">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>
                            <strong>Error:</strong>
                            <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true" class="">&times;</span>
                            </button>
                        </h3>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
