<footer class="hidden-print">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-12">
                    <h5>About PamperHub</h5>
                    <hr>
                    <p>
                        Pamperhub brings together independent mobile hair, beauty & wellness professionals and clients.
                        Professionals can showcase their work, connect with new and existing clients, and build their
                        mobile business. Clients can discover new services and providers, book appointments online for
                        services to be delivered at home, and get inspired.
                    </p>
                    <a href="{{ route('about') }}" class="btn-clear btn-md btn">Read more &raquo;</a>
                </div>

                <div class="col-md-2 col-sm-6 col-md-offset-1 col-xs-6">
                    <ul>
                        <li>
                            <a href="{{ route('about') }}">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('help') }}">
                                F.A.Q
                            </a>
                        </li>
                        {{--<li>--}}
                        {{--<a href="">--}}
                        {{--Sitemap--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        <li>
                            @if(Auth::guest())
                                <a href="{{ route('login') }}">Login</a>
                            @else
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            @endif
                        </li>
                    </ul>
                </div>

                <div class="col-md-2 col-sm-6 col-xs-6">
                    <ul>
                        <li>
                            <a href="{{ route('terms') }}">
                                Terms &amp; Conditions
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('privacy') }}">
                                Privacy Policy
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cookie') }}">
                                Cookie Policy
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="clearfix visible-xs"></div>

                <div class="col-md-2 col-sm-12">
                    <ul>
                        <li>
                            <p>
                                <span class="text-uppercase text-blue text-underline">Get in Touch</span>
                                <span class="clearfix"></span>
                                1 Hamilton Road, Oxford OX2 7PY
                            </p>
                            <p>
                                <span class="clearfix"></span>
                                Tel: 07985177514
                                <span class="clearfix"></span>
                                <a href="mailto:info@pamperhub.com" class="mt-10">
                                    info@pamperhub.com
                                </a>
                            </p>
                        </li>
                    </ul>

                    {{--<ul>--}}
                    {{--<li>--}}
                    {{--<a href="{{ route('customer.index') }}">--}}
                    {{--My Account--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="{{ route('list-your-business') }}">--}}
                    {{--List your business--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}

                    {{--<ul class="social">--}}
                    {{--<li>--}}
                    {{--<a href="">--}}
                    {{--<i class="fa fa-facebook"></i>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                    {{--<a href="">--}}
                    {{--<i class="fa fa-twitter"></i>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                    {{--<a href="">--}}
                    {{--<i class="fa fa-instagram"></i>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h6 class="text-right xs-text-center">
                        &copy; PamperHub Ltd | Website by <a href="http://www.jppdesigns.co.uk" target="_blank">JPPdesigns</a>
                    </h6>
                </div>
            </div>
        </div>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</footer>

<!-- Modal -->
<div class="modal fade " id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">

            {{--<div class="modal-header">--}}
            {{----}}

            {{--</div>--}}
            <div class="modal-body">
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
                {{--aria-hidden="true">&times;</span></button>--}}

                <div class="clearfix"></div>

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#login" aria-controls="login" role="tab" data-toggle="tab">Login</a>
                    </li>
                    <li role="presentation">
                        <a href="#signup" aria-controls="signup" role="tab" data-toggle="tab">Signup</a>
                    </li>
                </ul>

                <div class="clearfix"></div>

                <div class="tab-content pt-20">
                    <div role="tabpanel" class="tab-pane active" id="login">

                        <h4>Login</h4>

                        <hr>

                        <p class="small mb-20 text-center">
                            To book your appointment please login.
                        </p>

                        {!! Form::open(['url' => '/login', 'id' => 'modal-login', 'data-parsley-validate' => '']) !!}

                        <div class="form-group {{ $errors->has('email') ? ' form-error' : '' }}">
                            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => '']) }}
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group" {{ $errors->has('password') ? ' form-error' : '' }}>
                            {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' => '']) }}
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="form-group mt-10 text-left">
                            {{ Form::submit('Log In', ['class' => 'btn-blue btn btn-md']) }}
                            {{--<div class="clearfix"></div>--}}

                            <a class="btn btn-link ml-10 mt-5 small" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                        </div>

                        <div class="form-group hidden">

                            {{ Form::text('method', 'modal', ['class' => 'hidden']) }}

                        </div>

                        {!! Form::close() !!}

                        <p class="small mt-20">
                            {{--Haven't got an account?--}}
                            {{--<a href="#signup" aria-controls="signup" class="text-blue text-bold" role="tab"--}}
                            {{--data-toggle="tab">Signup</a> now. --}}

                            Want to become a therapist? Then list you business
                            <a class="text-blue text-bold" href="{{ url('/supplier/register') }}">here</a>.
                        </p>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="signup">

                        {!! Form::open(['url' => '/register', 'id' => 'modal-signup', 'data-parsley-validate' => '']) !!}

                        <h4>Signup</h4>
                        <hr>

                        <p class="small mb-20 text-center">
                            Signup today and enjoy all the benefits
                            <span class="clearfix"></span>
                            Pamperhub has to offer.
                        </p>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    {{ Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'First Name']) }}
                                </div>

                                <div class="clearfix visible-xs mt-10"></div>

                                <div class="col-sm-6">
                                    {{ Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Last Name']) }}
                                </div>
                            </div>

                        </div>

                        <div class="form-group">

                            {{ Form::email('email', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Email Address']) }}
                            <div class="clearfix"></div>

                        </div>
                        <div class="form-group">

                            {{ Form::password('password', ['class' => 'form-control', 'required' => '', 'minLength' => '6', 'placeholder' => 'Password']) }}
                            <div class="clearfix"></div>

                        </div>

                        <div class="form-group">

                            {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => '', 'minLength' => '6', 'placeholder' => 'Confirm Password']) }}
                            <div class="clearfix"></div>

                        </div>

                        <div class="form-group hidden">

                            {{ Form::text('user_type', null, ['class' => 'hidden']) }}

                            {{ Form::text('method', 'modal', ['class' => 'hidden']) }}

                        </div>
                        <div class="clearfix"></div>

                        <div class="form-group text-center mt-10 ">
                            {{ Form::submit('sign up', ['class' => 'btn btn-blue btn-md']) }}
                        </div>

                        {!! Form::close() !!}

                        <div class="clearfix mb-20"></div>

                        {{--<p class="small mt-20">--}}
                        {{--Already have an account? Then <a href="#login" aria-controls="login" role="tab"--}}
                        {{--data-toggle="tab" class="text-blue text-bold">login</a>--}}
                        {{--here.--}}
                        {{--</p>--}}

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

{!! Html::script('bower_components/bootstrap/dist/js/bootstrap.js') !!}
{!! Html::script('dist/js/main.js') !!}