<section class="ie8">
    <div class="container">
        <div class="row mt-20 mb-20">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <h1 class="nmtop">Oops!</h1>
                <p>It looks like you are using an outdated version of Internet Explorer.</p>
                <p>This website is best viewed using IE9 and later. Please update your browser to continue and experience the site and content at its very best.</p>
                <p>Alternatively open the site in a recommended modern browser such as Firefox or Chrome.</p>
            </div>
        </div>
    </div>
</section>

<header class="ie8-hide" id="top">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="navbar-header text-center">
                    <button id="menu-toggle" type="button" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="{{ url('/') }}">
                        <span class="sr-only">PamperHub</span>
                        <img src="/dist/img/logo.svg" onerror="this.src='/dist/img/logo.png'" alt="PamperHub Logo" class="logo" width="200px">
                    </a>
                </div>

                <div id="menu">
                    {{--<img src="/dist/img/favicon.png" onerror="this.src='/dist/img/favicon.png'" alt="PamperHub Logo" class="menu-logo visible-xs" width="80px">--}}

                    <img src="/dist/img/logo.svg" onerror="this.src='/dist/img/logo.png'" alt="PamperHub Logo" class="menu-logo visible-xs-inline-block" width="150px">


                    <ul class="nav navbar-nav navbar-right visible-xs">
                        @if(Entrust::hasRole('supplier'))

                            <li class="{{ Request::is('/about') ? "active" : "" }}">
                                <a href="{{ route('about') }}">
                                    Company
                                </a>
                            </li>

                            <li class="{{ Request::is('frequently-asked-questions*') ? "active" : "" }}">
                                <a href="{{ route('help') }}" class="nbright">Help</a>
                            </li>

                            <li class="">
                                <a href="{{ route('customer.index') }}" class="btn btn-blue-header btn-sm">
                                    My Account
                                </a>
                            </li>

                            <li class="mt-10">
                                <a href="{{ route('customer.bookings') }}" class="btn btn-blue-header btn-sm bg-beige" style="border-color:#e2d9c2 !important;">
                                    My Bookings
                                </a>
                            </li>

                            <li class="text-white mt-10 pl-15"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> logout
                            </li>

                        @elseif(Entrust::hasRole('customer'))

                            <li class="{{ Request::is('/') ? "active" : "" }}">
                                <a href="{{ route('/') }}">
                                    Home
                                </a>
                            </li>

                            <li class="{{ Request::is('/about') ? "active" : "" }}">
                                <a href="{{ route('about') }}">
                                    Company
                                </a>
                            </li>

                            <li class="{{ Request::is('frequently-asked-questions*') ? "active" : "" }}">
                                <a href="{{ route('help') }}" class="nbright">Help</a>
                            </li>

                            <li class="npadding">
                                <a href="{{ route('logout') }}" class="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            </li>

                            <li class="">
                                <a href="{{ route('customer.index') }}" class="btn btn-blue-header btn-sm">
                                    My Account
                                </a>
                            </li>

                            <li class="">
                                <a href="{{ route('customer.bookings') }}" class="btn btn-blue-header btn-sm bg-beige" style="border-color:#e2d9c2 !important;">
                                    My Bookings
                                </a>
                            </li>



                        @else

                            <li class="{{ Request::is('/') ? "active" : "" }}">
                                <a href="{{ route('/') }}">
                                    Home
                                </a>
                            </li>

                            <li class="{{ Request::is('/about') ? "active" : "" }}">
                                <a href="{{ route('about') }}">
                                    Company
                                </a>
                            </li>

                            <li class="{{ Request::is('register*') ? "active" : "" }}"><a href="{{ route('register') }}" class="nbright">Sign Up</a></li>

                            <li class="{{ Request::is('login*') ? "active" : "" }}"><a href="#" data-toggle="modal" data-target="#login-modal" class="nbright">Login</a></li>

                            <li class="{{ Request::is('frequently-asked-questions*') ? "active" : "" }}">
                                <a href="{{ route('help') }}" class="nbright">Help</a>
                            </li>

                            <li class="npadding">
                                <a href="{{ route('list-your-business') }}" class="btn btn-blue-header btn-sm">stylist? join us!</a>
                            </li>


                        @endif
                    </ul>

                    <ul class="nav navbar-nav navbar-right hidden-xs">
                        @if(Entrust::hasRole(['super-admin', 'customer-service-rep']))

                            <li class=""><a href="{{ route('admin') }}" class="nbright">admin</a></li>
                            <li class="npadding">
                                <a href="{{ route('logout') }}" class="btn btn-sm btn-blue-header" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">logout</a>
                            </li>

                        @elseif(Entrust::hasRole('supplier'))

                            <li class="{{ Request::is('supplier*') ? "active" : "" }}"><a href="{{ url('supplier')}}" class="nbright">my account</a></li>
                            <li class="npadding">
                                <a href="{{ route('logout') }}" class="btn btn-sm btn-blue-header" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">logout</a>
                            </li>

                        @elseif(Entrust::hasRole('customer'))

                            <li class=""><a href="{{ route('/') }}" class="">search</a></li>
                            <li class=""><a href="{{ route('customer.index') }}" class="nbright">my account</a></li>
                            <li class="npadding">
                                <a href="{{ route('logout') }}" class="btn btn-sm btn-blue-header" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">logout</a>
                            </li>
                            
                            <li class="nbright">
                                <a href="{{ route('help') }}" class="nbright">Help</a>
                            </li>

                        @else

                            <li class="{{ Request::is('register*') ? "active" : "" }}"><a href="{{ route('register') }}">Sign Up</a></li>
                            <li class="{{ Request::is('login*') ? "active" : "" }}"><a href="#" data-toggle="modal"
                                                                                       data-target="#login-modal" class="nbright">Login</a></li>
                            <li class="npadding">
                                <a href="{{ route('list-your-business') }}" class="btn btn-blue-header btn-sm">stylist? join us!</a>
                            </li>
                            <li class="{{ Request::is('frequently-asked-questions*') ? "active" : "" }}">
                                <a href="{{ route('help') }}" class="nbright">Help</a>
                            </li>
                            
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>