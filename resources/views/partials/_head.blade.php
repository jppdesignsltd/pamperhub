<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<meta name="robots" content="index, follow"/>

<meta name="description" content="{{ htmlspecialchars($settings->default_description) }}">
<meta name="keywords" content="{{ htmlspecialchars($settings->default_keywords) }}">
<meta name="author" content="{{ htmlspecialchars($settings->default_page_author) }}">

{{--@if ($setting->advanced_seo == 0)--}}
{{--@elseif ($setting->advanced_seo == 1)--}}
{{--@yield('custom_meta', "<meta name='description' content=\"$setting->default_description\">--}}
{{--<meta name='keywords' content=\"$setting->default_keywords\">--}}
{{--<meta name='author' content=\"$setting->default_page_author\">")--}}
{{--@endif--}}

<title>@yield('title') | {{ htmlspecialchars($settings->site_name) }}
    | {{ htmlspecialchars($settings->tagline) }}</title>

<!--[if gt IE 6]>
<style type="text/css" media="screen,print">
    div {
        min-height: 1px;
    }
</style>
<![endif]-->

<!--[if lt IE 7]>
<style type="text/css" media="screen,print">
    div {
        height: 1px;
    }
</style>
<![endif]-->

<!--[if IE 8]>
{!! Html::style('dist/css/ie8.css') !!}
<![endif]-->

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Load CSS Styles -->
{!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
{!! Html::style('bower_components/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('dist/css/style.css') !!}
@yield('stylesheets')


{!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
@yield('header-scripts')

