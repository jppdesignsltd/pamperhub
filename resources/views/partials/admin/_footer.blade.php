<footer class="{{--navbar-fixed-bottom--}}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h6>
                    &copy; PamperHub Ltd

                    <span class="pull-right">Cheese Twist CMS <i class="fa fa-code"></i>'d with <i class="fa fa-heart text-red"></i> by Pixel Twist</span>
                </h6>
            </div>
        </div>
    </div>
</footer>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>

{!! Html::script('bower_components/bootstrap/dist/js/bootstrap.js') !!}
{!! Html::script('dist/js/admin.js') !!}