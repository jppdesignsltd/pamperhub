<header class="ie8-hide" id="admin_menu-cont">
    <div id="admin_menu-topbar">
        {{--<h2>Cheese Twist Admin</h2>--}}
        <img src="/dist/img/logo.svg" onerror="this.src='/dist/img/logo.png'" alt="Company Logo" class="logo" height="100%">
        <span class="ml-10">| Admin</span>
    </div>

    <div id="admin_menu-inner">
        <div id="admin_menu-profile">
            <h5 class="text-white">
                <img src="{{ asset('dist/img/default_user.png') }}" alt="Supplier Image" class="mr-10 bg-white img-circle" width="40">
                <span>
                    {{ $user->firstname . ' ' . $user->lastname }}
                    @if(Entrust::hasRole(['super-admin']))
                        <span>Super Admin</span>
                    @else
                        <span>Customer Service Rep.</span>
                    @endif
                </span>
                <div class="clearfix"></div>
            </h5>
        </div>

        <nav id="admin_menu">
            <ul>
                <li class="{{ Request::is('admin') ? "active" : "" }}">
                    <a href="{{ route('admin') }}"><i class="mr-10 fa fa-th"></i> Dashboard</a>
                </li>

                <li class="{{ Request::is('admin/suppliers*') ? "active" : "" }}">
                    <a href="{{ route('admin.suppliers') }}"><i class="mr-10 fa fa-cubes"></i> Suppliers <span class="badge pull-right {{ Request::is('admin/suppliers*') ? "bg-blue text-dark-gray" : "" }}">{{ $suppliers->count() }}</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/customers*') ? "active" : "" }}">
                    <a href="{{ route('admin.customers') }}">
                        <i class="mr-10 fa fa-address-card-o"></i> Customers
                        <span class="badge pull-right {{ Request::is('admin/customers*') ? "bg-blue text-dark-gray" : "" }}">{{ $customers->count() }}</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/services*') ? "active" : "" }} {{ Request::is('service*') ? "active" : "" }}">
                    <a href="{{ route('admin.services') }}">
                        <i class="mr-10 fa fa-clone"></i> Listings
                        <span class="badge pull-right {{ Request::is('admin/services*') ? "bg-blue text-dark-gray" : "" }} {{ Request::is('service*') ? "bg-blue text-dark-gray" : "" }}">{{ $services->count() }}</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/appointments*') ? "active" : "" }}">
                    <a href="{{ route('admin.appointments') }}">
                        <i class="mr-10 fa fa-calendar-o"></i> Appointments
                        <span class="badge pull-right {{ Request::is('admin/appointments*') ? "active" : "" }}">{{  $appointments->count() }}</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/treatments*') ? "active" : "" }}">
                    <a href="{{ route('admin.treatments') }}">
                        <i class="mr-10 fa fa-bookmark"></i> Treatments
                        <span class="badge pull-right {{ Request::is('admin/treatments*') ? "bg-blue text-dark-gray" : "" }}">{{ $treatments->count() }}</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/tags*') ? "active" : "" }}">
                    <a href="{{ route('admin.tags') }}">
                        <i class="mr-10 fa fa-tags"></i> Tags
                        <span class="badge pull-right {{ Request::is('admin/tags*') ? "bg-blue text-dark-gray" : "" }}">{{ $tags->count() }}</span>
                    </a>
                </li>


                @if(Entrust::hasRole(['super-admin']))
                <li class="{{ Request::is('admin/users*') ? "active" : "" }}">
                    <a href="{{ route('admin.users') }}">
                        <i class="mr-10 fa fa-users"></i> Users
                        <span class="badge pull-right {{ Request::is('admin/users*') ? "bg-blue text-dark-gray" : "" }}">{{ $users->count() }}</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/settings*') ? "active" : "" }}">
                    <a href="{{ route('admin.settings') }}">
                        <i class="mr-10 fa fa-cogs"></i>
                        Settings
                    </a>
                </li>
                @endif

                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="mr-10 fa fa-sign-out"></i> Logout
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>