<div class="well text-center pl-10 pr-10">
    <div class="img-wrap">


        <div id="" class=" text-left">
            @if($customer->display_picture === null || $customer->display_picture === '')
                <img id="image_preview" src="{{ asset('dist/img/default_user.png') }}" alt="Supplier Image" class="img-responsive">
            @else
                <a onclick="clearFilestyle()" class="text-bold"><i class="fa fa-times text-red"></i></a>
                <div class="clearfix mb-5"></div>
                <img id="image_preview" src="{{ asset('uploads/customers/' . $customer->display_picture) }}" onerror="this.src='{{ asset('dist/img/default_user.png') }}'" alt="Supplier Image" class="img-responsive">
            @endif
        </div>


    </div>
    <div class="clearfix mb-20"></div>
    <a id="editBtnImage" onclick="activateImgForm('customerImg', 'editBtnImage')"
       class="btn btn-md btn-clear text-underline text-bold">change image</a>

    {!! Form::model($customer, ['url' => ['customer' . '?page=image', $customer->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'id' => 'customerImg', 'files' => true, 'class' => 'hidden']) !!}
    <div class="form-group">
        <div class="row">
            <div id="cover_image-cont" class="col-sm-12">
                {{ Form::file('display_picture', ['id' => 'display_picture', 'class' => 'filestyle','data-buttonName' => '', 'data-input' => 'true', 'data-placeholder' => 'No file chosen', 'data-buttonName' => 'btn-beige-plain',  'onchange' => 'loadFile(event)']) }}
            </div>
            <div class="clearfix mb-10"></div>

            <div class="col-sm-12">
                {{ Form::button('<i class="fa fa-save mr-10"></i> save', ['type' => 'submit', 'class' => 'btn btn-md text-bold bg-blue pt-5 pb-5 btn-block btn-clear']) }}
            </div>

        </div>

        <div class="row">

            <div class="clearfix"></div>



        </div>
    </div>
    {{ Form::close() }}
</div>

<hr class="visible-xs">

<ul class="nav nav-pills nav-stacked mb-20">
    <li class="{{ Request::is('customer') ? "active" : "" }}"><a href="{{ route('customer.index') }}">basic information</a></li>

    <li class="{{ Request::is('customer/bookings') ? "active" : "" }}"><a href="{{ route('customer.bookings') }}">my bookings</a></li>

</ul>

<hr class="visible-xs">