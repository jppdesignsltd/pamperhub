@extends('layouts.main')

@section('title', 'Cookie Policy')

@section('stylesheets')
@stop

@section('header-scripts')
@endsection

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 {{--pl-30 pr-30--}}">
                    <h2 class="nmbottom nmtop text-center">
                        Cookie Policy
                    </h2>

                    <div class="clearfix"></div>
                    <hr style="width: 200px;" class="bc-blue mt-10">
                </div>
            </div>

            <div class="row mt-30">
                
            </div>
        </div>
    </section>

@stop

@section('scripts')
@stop
