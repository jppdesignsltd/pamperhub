@extends('layouts.main')

@section('title', 'Help')

@section('stylesheets')
    {!! Html::style('bower_components/owl.carousel/dist/assets/owl.carousel.css') !!}
    {!! Html::style('bower_components/owl.carousel/dist/assets/owl.theme.default.css') !!}
@endsection

@section('content')

    <section id="help">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 {{--pl-30 pr-30--}}">
                    <h2 class="nmbottom nmtop text-center">
                        Frequently Asked Questions
                    </h2>

                    <div class="clearfix"></div>
                    <hr style="width: 200px;" class="bc-blue mt-10">
                </div>
            </div>

            <div class="row mt-30">
                {{--Question--}}
                <div class="help-block">
                    <div class="col-md-6 col-sm-12">
                        <a class="help-question" role="button" data-toggle="collapse" href="#question0" aria-expanded="true" aria-controls="question0">
                            How does Pamperhub work?
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-12">
                        <div class="collapse in" id="question0">
                            <div class="help-answer">
                            <p>
                                Pamperhub connects independent hair, beauty and wellness professionals with clients, allowing you to book appointments for services or treatments  to be delivered at your home.
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End of Question--}}

                {{--Question--}}
                <div class="help-block">
                    <div class="col-md-6 col-sm-12">
                        <a class="help-question" role="button" data-toggle="collapse" href="#question1" aria-expanded="true" aria-controls="question1">
                            How do I book an appointment?
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-12">
                        <div class="collapse in" id="question1">
                            <div class="help-answer">
                            <p>
                                The process for booking via our website is as follows:
                            </p>
                            <ul>
                                <li>Enter in your postcode so we know where you are</li>
                                <li>We will then take you to the page where you can see all of the independent professionals who operate in your area. You can either select a professional directly or you can choose the specific service you are looking for, to further narrow down the list.</li>
                                <li>For each independent professional you can see the next available appointments. Select one and book it.</li>
                                <li>You will need to pay for the service upfront via PayPal.</li>
                                <li>We will then contact you to confirm the bookin</li>
                            </ul>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End of Question--}}

                {{--Question--}}
                <div class="help-block">
                    <div class="col-md-6 col-sm-12">
                        <a class="help-question" role="button" data-toggle="collapse" href="#question2" aria-expanded="false" aria-controls="question2">
                            How do I cancel an appointment?
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-12">
                        <div class="collapse" id="question2">
                            <div class="help-answer">
                            <p>
                                Contact us by email (info@pamperhub.com) and we will get in touch with you
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End of Question--}}

                {{--Question--}}
                <div class="help-block">
                    <div class="col-md-6 col-sm-12">
                        <a class="help-question" role="button" data-toggle="collapse" href="#question3" aria-expanded="false" aria-controls="question3">
                            How is my booking confirmed?
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-12">
                        <div class="collapse" id="question3">
                            <div class="help-answer">
                            <p>
                                We will send you a text and an email to confirm your appointment
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End of Question--}}

                {{--Question--}}
                <div class="help-block">
                    <div class="col-md-6 col-sm-12">
                        <a class="help-question" role="button" data-toggle="collapse" href="#question4" aria-expanded="false" aria-controls="question4">
                            How do I pay for my appointment?
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-12">
                        <div class="collapse" id="question4">
                            <div class="help-answer">
                            <p>
                                You pay via PayPal – which also allows you to use all major credit and debit cards as well as paying directly from your bank account.
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End of Question--}}

                {{--Question--}}
                <div class="help-block">
                    <div class="col-md-6 col-sm-12">
                        <a class="help-question" role="button" data-toggle="collapse" href="#question5" aria-expanded="false" aria-controls="question5">
                            How do I change an appointment?
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-12">
                        <div class="collapse" id="question5">
                            <div class="help-answer">
                            <p>
                                Contact us by email ( info@pamperhub.com) and we will get in touch with you
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End of Question--}}

            </div>
        </div>
    </section>


@stop

@section('scripts')

    {!! Html::script('bower_components/owl.carousel/dist/owl.carousel.js') !!}



@stop
