@extends('layouts.main')

@section('title', 'List your business')

@section('stylesheets')

@endsection

@section('content')

    <section id="page-hero" class="bg-list-your-business">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-center">

                    @if(Entrust::hasRole('supplier'))

                        @if($user->supplier === null || $user->supplier->signup_complete === 0)

                            <h2 class="text-uppercase ">Complete registration</h2>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate dicta, ea, eius et
                                excepturi facere fuga harum iste magni modi mollitia natus nesciunt odio porro possimus
                                temporibus ut voluptate voluptates.
                            </p>
                            <a href="{{route('supplier.register')}}" class="btn btn-blue btn-lg text-uppercase">complete
                                signup</a>

                        @else

                            <h2 class="text-uppercase ">View your account</h2>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate dicta, ea, eius et
                                excepturi facere fuga harum iste magni modi mollitia natus nesciunt odio porro possimus
                                temporibus ut voluptate voluptates.
                            </p>
                            <a href="{{route('supplier')}}" class="btn btn-blue btn-lg text-uppercase">my account</a>

                        @endif

                    @else
                        <h2 class="text-uppercase ">List your business</h2>
                        <hr>
                        <p>
                            Connect with new and existing clients and grow your mobile salon business.
                        </p>
                        <a href="{{route('supplier.register')}}" class="btn btn-blue btn-lg text-uppercase">join now</a>
                    @endif

                </div>
            </div>
        </div>

        <div class="learn-more">
            Learn More
            <div class="clearfix"></div>
            <i class="fa fa-angle-double-down"></i>
        </div>
    </section>

    <section id="how-it-works" class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="nmbottom">How it works</h2>
                    <hr class="bc-dark-gray mt-5 mb-5 bw-2 hr-150">
                </div>
            </div>

            <div class="row mt-40">
                <div class="col-md-4 fb">
                    <h3>Join us</h3>
                    <p>You tell us about your business, experience and general availability.</p>
                </div>

                <div class="col-md-4 fb">
                    <h3>Activate your account</h3>
                    <p>We get in touch to get to know you and so you can get to know us, and once we're all happy we set you up on Pamperhub</p>
                </div>

                <div class="col-md-4 fb">
                    <h3>Get connected</h3>
                    <p>Clients book  and pay for appointments online. We collect the payment on your behalf and pass it on once you've delivered the service.</p>
                </div>
            </div>
        </div>
    </section>

    {{--<section id="why-choose-us">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<h3>Why choose us</h3>--}}
                    {{--<hr class="bc-blue mt-5 mb-5 bw-2 hr-100 nmleft">--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="row mt-40">--}}
                {{--<div class="col-lg-3 col-md-4">--}}
                    {{--<ul class="list-group bg-transparent">--}}
                        {{--<li class="list-group-item npleft nborder">--}}
                            {{--<i class="fa fa-check"></i> Lorem ipsum dolor sit amet--}}
                        {{--</li>--}}

                        {{--<li class="list-group-item npleft nborder">--}}
                            {{--<i class="fa fa-check"></i> Lorem ipsum dolor sit amet--}}
                        {{--</li>--}}

                        {{--<li class="list-group-item npleft nborder">--}}
                            {{--<i class="fa fa-check"></i> Lorem ipsum dolor sit amet--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}

                {{--<div class="col-lg-3 col-md-4">--}}
                    {{--<ul class="list-group bg-transparent">--}}
                        {{--<li class="list-group-item npleft nborder">--}}
                            {{--<i class="fa fa-check"></i> Lorem ipsum dolor sit amet--}}
                        {{--</li>--}}

                        {{--<li class="list-group-item npleft nborder">--}}
                            {{--<i class="fa fa-check"></i> Lorem ipsum dolor sit amet--}}
                        {{--</li>--}}

                        {{--<li class="list-group-item npleft nborder">--}}
                            {{--<i class="fa fa-check"></i> Lorem ipsum dolor sit amet--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}


@stop

@section('scripts')

@stop
