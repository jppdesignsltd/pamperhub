@extends('layouts.main')

@section('title', 'Terms and Conditions')

@section('stylesheets')
@stop

@section('header-scripts')
@endsection

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 {{--pl-30 pr-30--}}">
                    <h2 class="nmbottom nmtop text-center">
                        Terms &amp; Conditions
                    </h2>

                    <div class="clearfix"></div>
                    <hr style="width: 200px;" class="bc-blue mt-10">
                </div>
            </div>

            <div class="row mt-30">
                <div class="col-md-10 col-md-offset-1">
                    <p class="text-italic">
                        Last updated: 14th March 2017
                    </p>
                    <p>
                        Your use of Our Site is subject to these terms and conditions together with any additional terms and policies referred to in them ("Our Terms").
                    </p>
                    <p>
                        These terms and conditions of service together with any terms and policies referred to in them (" Our Terms ") constitute a legally binding agreement between you and Pamperhub Ltd (" Pamperhub ", " we ", " us " or " our "). Pamperhub is a private limited company registered in England and Wales with company No. 10111931 whose registered office is located at the following address: 1 Hamilton Road, Oxford, OX2 7PY.
                    </p>

                    <p>
                        Pamperhub provides a booking service (the "Services") for a range of hair, beauty and massage services ("Services") that are performed in each and every case by independent self-employed Professionals ("Independent Professionals"). In providing this booking service, Pamperhub acts as the agent of these Independent Professionals. Pamperhub has no responsibility for any Services which you book through us: we are simply involved with the booking process, as well as providing ancillary services as explained below.

                    </p>

                    <p>
                        The Services we offer allow you to search through Our Site and purchase Services from a number of Independent Professionals. As part of the Services, we also provide some ancillary services such as arranging such bookings and providing you with customer service assistance.
                        However, as stated above, the contract for the purchase of the Services is between you and the Independent Professionals. This means that it is the Independent Professional (not us) who is legally responsible for providing the Services to you. However, Pamperhub remain responsible in respect of its obligations to you in accordance with these Terms and Conditions which shall be legally binding. If you have any questions, complaints or requests for refunds, we can be contacted directly via our Customer Services Team via info@pamperhub.com.   Pamperhub will remain the point of contact for the Customer for Customer Service questions.
                        Pamperhub shall, if requested, provide intermediary services between you and Independent Professionals in connection with Customer Service or dispute resolution matters.

                    </p>

                    <p>
                        In order to book the Services you must be over 18 years of age, however services can be booked on behalf of a minor.

                        When booked by us, the Service you receive will be subject to the Independent Professionals' terms and conditions as set out in the Client Consultation Form ("Independent Professionals' T&Cs") and you will be asked to confirm your acceptance of them prior to receiving the Service. The Independent Professional's T&Cs will form the basis of your agreement with the Independent Professional for your Service (each a "Service Agreement"). Pamperhub is not a party to any Service Agreement: this will solely be between you and the Independent Professional who provides you with your Service.
                    </p>

                    <p>
                        Our Terms govern your use of our website ("Our Site "). By using Our Site or you agree to abide by Our Terms.
                    </p>

                    <p>
                        Please read Our Terms carefully and thoroughly. If you do not accept Our Terms, you must not make a booking through Pamperhub for any Service.
                        In order to provide Our Booking Services, we may collect personal data from you. Before making a booking with us please read our privacy policy available here ( <a href="{{ route('privacy') }}">‘Our Privacy Policy’</a>)The Consumer Contracts (Information, Cancellation and Additional Charges) Regulations 2013 requires Pamperhub to give you certain key information for there to be a legally binding contract between you and us. This information is set out below [and is also linked in the email which we will send to you to confirm any booking you make through us with an Independent Professional]. Details on how to make a booking are set out below. If we wish to change any key information once a legally binding contract is made, we can only do this if you agree to it.
                        We only conclude contracts in the English language.

                    </p>

                    <h4 class="text-bold mt-20">
                        ELIGIBLE USE
                    </h4>
                    <p>
                        You confirm that you are of legal age to access and use Our Site and of legal capacity to agree to Our Terms. You are not eligible to use Our Booking Services if you are under the age of 18.
                    </p>

                    <h4 class="text-bold mt-20">
                        USAGE
                    </h4>
                    <p>

                        When registered on Our Site or through Our App you will be able to access to and use of our search engine to look for Services within the United Kingdom and to book Services with an Independent Professional of your choice.

                    </p>

                    <h4 class="text-bold mt-20">
                        BOOKING PROCESS &amp; PAYMENT
                    </h4>
                    <p>
                        You can make a booking through Our Site by choosing an Independent Professional.
                    </p>
                    <p>
                        You may only make a booking up to 4 weeks in advance.
                    </p>
                    <p>
                        Your payment details will be requested at the time of booking and payment will be collected when you make a booking.
                    </p>
                    <p>
                        Full payment of the Service Fee is due at the time of booking that Service with the Independent Professional through us. Full details of the prices are referred to below. 
                    </p>
                    <p>
                        The Service Fee belongs to the Independent Professional who provides the Service. Pamperhub collects the Service Fee from you as their agent. For each booking made through us, we charge the relevant Independent Professional an agreed fee (plus any VAT payable thereon) for introducing you to that Independent Professional (" Our Agent's Fee "). We have an agreement with each Independent Professional under which we are entitled to deduct Our Agent's Fee from the Service Fee paid by you for the Service. 
                    </p>
                    <p>
                        We will do all that we reasonably can to ensure that all of the information you give us when paying for the Service is secure by using an encrypted secure payment mechanism. However, in the absence of negligence on the part of Pamperhub, we will not be legally responsible to you for any loss that you may suffer if a third party gains unauthorised access to any information that you may give us at any time.
                    </p>
                    <p>
                        By making a booking you agree to provide complete, correct and true information including without limitation billing and payment information.
                    </p>
                    <p>
                        Once your booking is accepted and confirmed by the Independent Professional, you will receive confirmation of your appointment by email.
                    </p>
                    <p>
                        By making a booking, you are responsible for:
                    </p>
                    <ul>
                        <li>
                            Full payment of the applicable Service Fee;
                        </li>
                        <li>
                            Ensuring the Independent Professional has access to your Designated Premises which must, in all cases, represent a suitable space in which the Service can be performed, with all appropriate facilities (including, adequate lighting and heating); and
                        </li>
                        <li>
                            Ensuring the health and safety of the Independent Professional whilst at your Designated Premises.
                        </li>
                    </ul>
                    <p>
                        All Service Fees are payable through Our Site. As noted above, we collect payment of Service Fees on behalf of the Independent Professional to whom we charge Our Agent's Fee.
                    </p>
                    <p>
                        Please note that all fees and charges are exclusive of VAT and VAT will be charged by the Independent Professional at the prevailing rate where applicable.
                    </p>

                    <h4 class="text-bold mt-20">
                        PRICES
                    </h4>
                    <p>

                        The price of Services (each a " Service Price " and collectively " Service Prices ") varies according to the type and duration of the Service, and the Independent Professional you book and also the location of the premises you have designated for the Service to be provided to you by the Independent Professional (" Designated Premises "). Service Prices are set from time to time and the Service Price you will be required to pay for a specific Service (each a " Service Fee " and collectively " Service Fees ") will be determined by reference to the Service Prices in force at the date when that Service is booked.
                        Full details are set out in our site.

                    </p>

                    <h4 class="text-bold mt-20">
                        CUSTOMER RESPONSIBILITIES


                    </h4>
                    <p>
                        In addition to the responsibilities noted above, it is your responsibility to provide complete and accurate information at the time of booking. Failure to provide complete and accurate information may result in a rejection of your booking request, cancellation of your booking or an inability of the booked Independent Professional to provide the Service as requested. Such failure may also result in loss or incorrect delivery of your booking confirmation.
                    </p>

                    <h4 class="text-bold mt-20">
                        CANCELLATION &amp; REFUNDS
                    </h4>
                    <p>

                        You acknowledge that you do not have any statutory right to cancel a booking made for a Service. However, you have a contractual entitlement to cancel any booking you have made with an Independent Professional through us in the following circumstances and on the described terms.
                    </p>
                    <p>
                        Subject to the cancellation being a Late or Very Late Cancellation (as described below), if you change your mind about your booking prior to the agreed appointment start time in that booking (" Service Appointment Time ") then the Independent Professional will be willing to treat your booking as cancelled (without requiring payment of all or any part of the applicable Service Fee and without levying any cancellation fee) if you cancel your booking via Our Site or via Customer Care at info@pamperhub.com
                    </p>
                    <ul>
                        <li>
                            at least twenty-four (24) hours prior to the Service Appointment Time; or
                        </li>
                        <li>
                            if the Service Appointment Time falls within twenty-four (24) hours of the time of booking, within ten (10) minutes of you having confirmed that you want to make a booking (" Grace Period ").
                        </li>
                    </ul>
                    <p>
                        If your cancellation of a booking is:
                    </p>
                    <ul>
                        <li>
                            within twenty-four (24) hours of the Service Appointment Time; or
                        </li>
                        <li>
                            within twenty-four (24) hours of the Service Appointment Time; or
                        </li>
                        <li>
                            after the Grace Period has elapsed
                        </li>
                    </ul>
                    <p>
                        then unless the cancellation represents a Very Late Cancellation (as described below), the Independent Professional will be entitled to retain (or charge, as the case may be) thirty per cent (30%) of the applicable Service Fee because your cancellation represents a Late Cancellation.
                    </p>
                    <p>
                        However, if your cancellation of a booking is within two (2) hours of the Service Appointment Time and if applicable, the Grace Period has elapsed, the Independent Professional will be entitled to retain (or charge, as the case may be) one hundred per cent (100%) of the applicable Service Fee because your cancellation represents a Very Late Cancellation .
                    </p>
                    <p>
                        You will also be charged the full Service Fee by the Independent Professional if you:
                    </p>
                    <ol>
                        <li>Cancel a booking other than as permitted above;</li>
                        <li>
                            Attempt to cancel a booking on or after Service Appointment Time; or
                        </li>
                        <li>
                            Fail to attend a booking at the Service Appointment Time and/or at the Designated Premises.
                        </li>
                    </ol>
                    <p>
                        A cancellation fee is charged in order to compensate the Independent Professional because it is not reasonable to expect that Independent Professional to be able to provide Services at another booking where you cancel with short or no notice.
                    </p>
                    <p>
                        Cancellation fees may in our absolute discretion be waived where you have been unable to cancel a booking without incurring the cancellation fee for genuine reasons which were outside of your control. Where cancellation fees are waived by us, we act as agent of the Independent Professional who is the principal in supplying the Services.
                    </p>

                    <h4 class="text-bold mt-20">
                        DELAYS
                    </h4>
                    <p>
                        If you are delayed and unable to start the Service at the Designated Premises at the Service Appointment Time by more than 10 minutes then, unless the Independent Professional agrees to the contrary (which is within the sole discretion of the Independent Professional):
                    </p>
                    <ul>
                        <li>
                            the Independent Professional is entitled to end the Service at the time agreed when the booking was made without making any adjustment to the Service Fee to reflect the reduced time of the Service; or
                        </li>
                        <li>
                            if you and the Independent Professional agree to proceed with the Service for the full time allotted notwithstanding the delayed start time, then Independent Professional shall be entitled to receive and you shall be obliged to pay before the relevant Service is given, an additional charge calculated at the rate set out with the Service Prices for extra time.
                        </li>
                    </ul>

                    <h4 class="text-bold mt-20">
                        PAMPERHUB ACTS ONLY AS AN INTRODUCER
                    </h4>
                    <p>
                        Our Booking Services enable those seeking Services to book appointments with Independent Professionals seeking to provide such Services. Whilst Pamperhub interviews and assesses Independent Professionals who wish to provide Service, we do not guarantee or warrant, and make no representations regarding the reliability, quality or suitability of Independent Professionals. This is entirely a matter for you. Accordingly, you acknowledge and agree that Pamperhub does not have any obligation to conduct any background checks on any Independent Professional and have no obligation in respect of any service they may provide.
                    </p>
                    <p>
                        When interacting with any Independent Professional you should exercise caution and common sense to protect your personal safety, details and property, just as you would when interacting with other persons who are unknown to you.
                    </p>

                    <h4 class="text-bold mt-20">
                        PROHIBITED USE
                    </h4>
                    <p>
                        Our Booking Services are for your personal and non-commercial use and must only be used for the purposes of inquiring about or making bookings for Services as expressly described above. You must not use the Site, App or the Telephone Booking Services to do any of the following which are strictly prohibited:
                    </p>
                    <ul>
                        <li>
                            Engage in any inappropriate behaviour including but not limited to illicit or sexually suggestive remarks, sexual advances, drug use, excessive drinking and/or other inappropriate behaviour;
                        </li>
                        <li>
                            Restrict or inhibit any other user from using and enjoying Our Booking Services;
                        </li>
                        <li>
                            Infringe the privacy rights, property rights, or other civil rights of any person;
                        </li>
                        <li>
                            Harass, abuse, threaten or otherwise infringe or violate the rights of Independent Professionals, Pamperhub (including its employees and personnel) or others;
                        </li>
                        <li>
                            Harvest, data-mine or otherwise collect information about others, including email addresses, without their consent;
                        </li>
                        <li>
                            Use technology or other means to access our computer network, unauthorised content or non-public spaces;
                        </li>
                        <li>
                            Introduce or attempt to introduce any viruses or any other harmful code, files or programs that interrupt or otherwise or limit Our Booking Services' functionality;
                        </li>
                        <li>
                            Damage, disable or otherwise impair our servers or networks or attempt to do the same; and
                        </li>
                        <li>
                            Engage in or encourage others to engage in criminal or unlawful conduct or breach these Terms including misuse of Our Booking Services for unlawful or unauthorised purposes.
                        </li>
                    </ul>
                       <p>
                        You agree not to breach these Terms in any way which may result in, among other things, termination or suspension of your access to Our Booking Services.


                    </p>

                    <h4 class="text-bold mt-20">
                        INTELLECTUAL PROPERTY
                    </h4>
                    <p>
                        PAMPERHUB, the Pamperhub logo, brand and all other intellectual property rights, trademarks, service marks, graphics and logos used in connection with the Site, belong to us or our licensors (as are applicable) and are protected by intellectual property law. Nothing in these Terms grants you any rights in the Site, or the content within the same. All rights are reserved.

                    </p>

                    <h4 class="text-bold mt-20">
                        DISCLAIMER
                    </h4>
                    <p>
                        As noted above, we introduce individuals seeking Services to Independent Professionals seeking to provide such Services. We are not liable or responsible for the fulfillment of any booking or the performance of the Independent Professional. You acknowledge and agree that we are not responsible for addressing any claims you have as regards any Independent Professional or any Services, however we do try to assist by providing the intermediary services around resolving disputes and complaints as mentioned above.
                    </p>
                    <p>
                        Content on Our Site which is made available as part of Our Booking Services is provided for your general information purposes only. Nothing contained on Our Site or communicated any other way as part of Our Booking Services constitutes, or is meant to constitute, advice, opinion or guidance of any kind. We are not a health care provider nor do we provide medical advice or medical Service. References in Our Site, Our App and elsewhere to "Service" and "Therapy" or any similar terms do not refer to medical Service or medical therapy. The information presented on Our Site and otherwise as part of Our Booking Services is not intended to diagnose health problems or to take the place of professional medical care. Should you have a medical issue you should always consult with a qualified medical professional.
                    </p>
                    <p>
                        We make no warranty or guarantee that Our Site or any other aspect of Our Booking Services is suitable for your intended use, error-free, timely, reliable, entirely secure, virus-free or available. We make no guarantee of particular results or outcomes by use of Our Site or any other aspect of Our Booking Services.
                    </p>
                    <p>
                        Nothing in Our Terms will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit and nothing in Our Terms will exclude or limit our liability in respect of any: death or personal injury caused by the negligence of Pamperhub, fraud or fraudulent misrepresentation by Pamperhub, or any matter which it would be illegal or unlawful for Pamperhub to exclude or limit, or to attempt or purport to exclude or limit, its liability.
                    </p>
                    <p>
                        We are not liable or responsible for any errors in or failure to provide Our Booking Services due to your error or failure to provide accurate and complete information.
                    </p>
                    <p>
                        Whilst we make every effort to ensure that Our Booking Services are available, we do not represent, warrant or guarantee in any way the continued availability at all times or the uninterrupted use by you of Our Booking Services. We reserve the right to suspend or cease the operation of all or part of Our Booking Services from time to time at our sole discretion. 
                    </p>
                    <p>
                        USE OF THIS SITE AND BOOKING SERVICES IS ON AN "AS-IS" AND "AS AVAILABLE" BASIS. TO THE MAXIMUM EXTENT PERMITTED AT LAW IN NO EVENT SHALL WE BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, LOSS OF DATA, LOST REVENUES, LOSS OF GOODWILL, LOSS OF ANTICIPATED SAVING OR PROFITS, OR ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE OR SERVICES, OR WITH THE DELAY OR INABILITY TO USE THE SITE OR SERVICES, OR WITH THE PROVISION OF OR FAILURE TO PROVIDE THE SITE OR SERVICES.
                    </p>

                    <h4 class="text-bold mt-20">
                        INDEMNITY
                    </h4>
                    <p>
                        You agree to defend and indemnify us from and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature including but not limited to reasonable legal and accounting fees, that arise from or relate to your use or misuse of, or access to Our Booking Services and otherwise from your violation of Our Terms.

                    </p>

                    <h4 class="text-bold mt-20">
                        MODIFICATION & TERMINATION
                    </h4>
                    <p>
                        We may modify Our Terms or terminate use of Our Booking Services at any time by giving notice of termination to you. The Service Prices may also change from time to time. We may also change, suspend, terminate or discontinue any aspect of Our Booking Services including availability of certain features at any time for any reason.


                    </p>

                    <h4 class="text-bold mt-20">
                        SEVERABILITY
                    </h4>
                    <p>

                        If any provision of Our Terms is deemed or becomes invalid, the validity of the other provisions shall not be affected.

                    </p>

                    <h4 class="text-bold mt-20">
                        GOVERNING LAW & JURISDICTION
                    </h4>
                    <p>

                        You agree that Our Terms for all purposes, shall be governed by and construed in accordance with English law. You also agree, subject to the following clause, to submit to the nonexclusive jurisdiction of the English courts as regards any claim or matter arising under these Terms.

                    </p>

                    {{--<h4 class="text-bold mt-20">--}}

                    {{--</h4>--}}
                    {{--<p>--}}

                    {{--</p>--}}

                    {{--<h4 class="text-bold mt-20">--}}

                    {{--</h4>--}}
                    {{--<p>--}}

                    {{--</p>--}}
                </div>
            </div>
        </div>
    </section>

@stop

@section('scripts')
@stop
