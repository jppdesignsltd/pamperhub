@extends('layouts.main')

@section('title', 'About')

@section('stylesheets')
@stop

@section('header-scripts')
@endsection

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 {{--pl-30 pr-30--}}">
                    <h2 class="nmbottom nmtop text-center">
                        About Pamperhub
                    </h2>

                    <div class="clearfix"></div>
                    <hr style="width: 200px;" class="bc-blue mt-10">
                </div>
            </div>
        </div>
    </section>

    <section class="about-module-contiainer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 about-module-image">
                    <img src="{{ asset('dist/img/about-image-1.jpg') }}" alt="About image PamperHub">
                </div>

                <div class="col-sm-6 about-module">
                    <div class="about-module-content">
                        <h2>
                            How it works for clients...
                        </h2>

                        <p>
                            Let us know where you are and we will let you know which professionals operate in your area, and when they are available to come to you.  You can then make a booking online and pay for it and then sit back and relax.  Your chosen professional will come to your home and deliver the service to you
                        </p>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="about-module-contiainer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 about-module-image visible-sm visible-xs">
                    <img src="{{ asset('dist/img/about-image-2.jpg') }}" alt="About image PamperHub">
                </div>

                <div class="col-sm-6 about-module">
                    <div class="about-module-content">
                        <h2>
                            How it works for professionals...
                        </h2>
                        <p>
                            List your business with Pamperhub and we will help clients discover you and your work and book appointments with you. Let us take care of marketing and administratino so you can focus on doing what you love - making your clients look and feel great.
                        </p>

                    </div>

                </div>

                <div class="col-sm-6 about-module-image visible-lg visible-md">
                    <img src="{{ asset('dist/img/about-image-2.jpg') }}" alt="About image PamperHub">
                </div>
            </div>
        </div>
    </section>

@stop

@section('scripts')
@stop
