@extends('layouts.main')

@section('title', 'Booking')

@section('stylesheets')
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {{--    {!! Html::style('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}--}}
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {{--{!! Html::style('bower_components/unslider/dist/css/unslider.css') !!}--}}
    {!! Html::style('bower_components/owl.carousel/dist/assets/owl.carousel.css') !!}
    {!! Html::style('bower_components/owl.carousel/dist/assets/owl.theme.default.css') !!}
@endsection

@section('header-scripts')
    {{--{!! Html::script('/bower_components/moment/min/moment.min.js') !!}--}}
    {{--    {!! Html::script('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}--}}
@endsection

@section('content')

    @if(Entrust::hasRole(['supplier', 'super-admin']))

        <section class="padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text-center">

                        <div class="well">

                            <h1 class="nmtop">Whoops!</h1>
                            <hr>
                            <h3>You appear to have a {{ Entrust::hasRole('supplier') ? 'supplier' : 'admin' }}
                                account.</h3>
                            <h4 class="nmtop mb-10">Please <a
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">logout</a>
                                and sign up for a customer account.</h4>

                        </div>

                    </div>
                </div>
            </div>
        </section>

    @else

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        @if ($search)
                            <h3>Find services in: <span class="text-uppercase text-bold">{{ $search }}</span></h3>
                        @else
                            <h3>Search Here</h3>
                        @endif
                    </div>
                </div>

                <div class="row mt-20 js-masonry">

                    <div class="col-md-4 col-sm-6 col-xs-12 masonry-item service_search">
                        <div class="service_search-container">


                            {!! Form::open(['route' => 'search.index', 'method' => 'GET', 'role' => 'search']) !!}
                            <div class="form-group mb-20">
                                {{ Form::label('search', 'Post Code', ['class' => 'nmtop nptop']) }}
                                {{ Form::text('search', $search, ['class' => 'form-control', 'title' => 'Enter Post Code', 'placeholder' => 'Enter Post Code', 'required' => '', 'minLength' => '3']) }}
                            </div>

                            <div class="form-group mb-20">
                                {{ Form::label('service', 'Service', ['class' => 'nmtop nptop']) }}
                                {{ Form::select('service', $treatment, $serviceSearch, ['class' => 'form-control select2-multiple']) }}
                            </div>

                            <div class="form-group mb-20">
                                {{ Form::label('distance', 'Distance', ['class' => 'nmtop nptop']) }}
                                <select name="distance" id="distance" class="form-control select2-multiple" required="">
                                    <option value="5" {{ $distanceSearch === '5' ? 'selected' : '' }}>5 Miles</option>
                                    <option value="15" {{ $distanceSearch === '15' ? 'selected' : '' }}>15 Miles
                                    </option>
                                    <option value="25" {{ $distanceSearch === '25' ? 'selected' : '' }}>25 Miles
                                    </option>
                                    <option value="50" {{ $distanceSearch === '50' ? 'selected' : '' }}>50 Miles
                                    </option>
                                    <option value="unlimited" {{ $distanceSearch === 'unlimited' ? 'selected' : '' }}>
                                        50+ Miles
                                    </option>
                                </select>
                            </div>

                            {{ Form::submit('Search', ['class' => 'btn btn-beige btn-lg']) }}
                            {!! Form::close() !!}
                        </div>

                    </div>

                    @if($servicesArray->count() === 0)

                        <div class="col-sm-8 masonry-item">
                            <p>
                                Sorry, we are not in your area yet. We’re growing fast though and hope to be there soon!
                            </p>
                        </div>

                    @elseif($servicesArray->count() !== 0)

                        @foreach($servicesArray as $service)

                            @if($service)

                                <div class="service_post col-md-4 col-sm-6 col-xs-12 masonry-item">

                                    <div class="service_post-container">
                                        <div class="service_post-header">
                                            <img
                                                    @if($service->cover_image !== '')
                                                    src="{{ asset('uploads/services/' . $service->cover_image) }}"
                                                    alt=""
                                                    @else
                                                    src="{{ asset('uploads/services/bg-' . strtolower($service->treatment->name) . '.jpg') }}"
                                                    onerror="this.src='{{ asset('dist/img/default-service-bg.png') }}'"
                                                    alt=""
                                                    @endif

                                            >
                                            <div class="image-overlay"></div>
                                            <h3>
                                                {{ $service->supplier->company }}
                                                <div class="clearfix"></div>
                                                <span class="small
">
                                                {{ $service->supplier->city }}
                                            </span>
                                            </h3>

                                            <div class="clearfix"></div>
                                            <p class="availability">
                                                <span class="clearfix"></span>
                                                <span class="text-bold mr-5 small">Available:</span>
                                                <span class="clearfix"></span>
                                            </p>
                                            <div class="owl-carousel">

                                                @foreach(($service->supplier->days->sortBy('time_ref')) as $timeName)
                                                    @if($timeName->name === 'sunday')
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Sun - Morn.</span>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Sun - Aft.</span>--}}
                                                        {{--</div>--}}
                                                        <div class="item">
                                                            <span class="label label-xs bg-white text-dark-gray">Sun - Eve.</span>
                                                        </div>
                                                    @endif

                                                    @if($timeName->name === 'monday')
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Mon - Morn.</span>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Mon - Aft.</span>--}}
                                                        {{--</div>--}}
                                                        <div class="item">
                                                            <span class="label label-xs bg-white text-dark-gray">Mon - Eve.</span>
                                                        </div>
                                                    @endif

                                                    @if($timeName->name === 'tuesday')
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Tue - Morn.</span>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Tue - Aft.</span>--}}
                                                        {{--</div>--}}
                                                        <div class="item">
                                                            <span class="label label-xs bg-white text-dark-gray">Tue - Eve.</span>
                                                        </div>
                                                    @endif

                                                    @if($timeName->name === 'wednesday')
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Wed - Morn.</span>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Wed - Aft.</span>--}}
                                                        {{--</div>--}}
                                                        <div class="item">
                                                            <span class="label label-xs bg-white text-dark-gray">Wed - Eve.</span>
                                                        </div>
                                                    @endif

                                                    @if($timeName->name === 'thursday')
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Thur - Morn.</span>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Thur - Aft.</span>--}}
                                                        {{--</div>--}}
                                                        <div class="item">
                                                            <span class="label label-xs bg-white text-dark-gray">Thur - Eve.</span>
                                                        </div>
                                                    @endif

                                                    @if($timeName->name === 'friday')
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Fri - Morn.</span>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Fri - Aft.</span>--}}
                                                        {{--</div>--}}
                                                        <div class="item">
                                                            <span class="label label-xs bg-white text-dark-gray">Fri - Eve.</span>
                                                        </div>
                                                    @endif

                                                    @if($timeName->name === 'saturday')
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Sat - Morn.</span>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="item">--}}
                                                        {{--<span class="label label-xs bg-white text-dark-gray">Sat - Aft.</span>--}}
                                                        {{--</div>--}}
                                                        <div class="item">
                                                            <span class="label label-xs bg-white text-dark-gray">Sat - Eve.</span>
                                                        </div>
                                                    @endif
                                                @endforeach

                                            </div>
                                        </div>

                                        <div class="service_post-body">

                                            <div class="clearfix"></div>

                                            <p class="excerpt">
                                                @if(strlen($service->description) > 0)
                                                    {{ substr(strip_tags($service->description), 0, $settings->services_excerpt) }}{{ strlen(strip_tags($service->description)) > $settings->services_excerpt ? "..." : "" }}
                                                @else

                                                    {{ $service->supplier->bio }}

                                                @endif
                                            </p>

                                            <div class="clearfix"></div>

                                        </div>


                                        <div class="service_post-footer">
                                            <div class="row">

                                                <div class="col-xs-12">
                                                    <p class="mb-20 text-right">
                                                        <i class="fa fa-map-marker mr-5"></i>
                                                        {{ round($service->distance, 1) }}
                                                        <span class="small">miles away</span>
                                                    </p>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div class="col-xs-6 npright text-left pt-5">
                                                    @if($service->promotion === 1)
                                                        <span class="label label-md label-success">
                                                            <strong>On Sale:</strong>
                                                            &pound;{{ $service->promotion_price }}
                                                            {{--<span class="small">p/h</span>--}}
                                                        </span>
                                                    @else
                                                        <span class="label label-md text-dark-gray text-left npleft">
                                                        <strong>Price:</strong>
                                                        &pound;{{ $service->price }}
                                                            {{--<span class="small">p/h</span>--}}
                                                    </span>
                                                    @endif

                                                </div>

                                                <div class="col-xs-6 npleft">
                                                    @if(Auth::guest())

                                                        <a href="#" class="btn btn-sm btn-blue btn-block"
                                                           data-toggle="modal"
                                                           data-target="#login-modal">
                                                            sign up to book
                                                        </a>

                                                    @else

                                                        <a href="{{ url('booking?s=' . $service->id) }}"
                                                           class="btn btn-sm btn-blue btn-block">
                                                            book now
                                                        </a>

                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                {{--@else--}}

                                {{--<div class="no-listing col-sm-8 masonry-item hidden">--}}
                                {{--<p>--}}
                                {{--There are no listings matching your criteria.--}}
                                {{--</p>--}}
                                {{--</div>--}}



                            @endif
                        @endforeach
                    @endif

                    {{--<div class="col-md-4 col-sm-6 col-xs-12 masonry-item paginate-service_search">--}}
                    {{--<div class="paginate-service_search-container">--}}

                    {{--</div>--}}

                    {{--</div>--}}
                </div>
            </div>
        </section>

    @endif

@stop

@section('scripts')
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {{--{!! Html::script('bower_components/unslider/dist/js/unslider-min.js') !!}--}}
    {!! Html::script('bower_components/owl.carousel/dist/owl.carousel.js') !!}
    {!! Html::script('bower_components/masonry/dist/masonry.pkgd.js') !!}
    {!! Html::script('bower_components/imagesloaded/imagesloaded.pkgd.js') !!}

    <script type="text/javascript">
        $(".select2-multiple").select2({
            width: '100%',
        });

        $(document).ready(function () {
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                navText: [
                    '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
                    '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
                ],
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }
            })
        })

        //        $( ".no-listing" ).first().toggleClass('hidden');

        if ($(".js-masonry")[0]) {
            $(document).ready(function () {
                var container = document.querySelector(".js-masonry");
                //create empty var msnry
                var msnry;
                // initialize Masonry after all images have loaded
                imagesLoaded(container, function () {
                    msnry = new Masonry(container, {
                        itemSelector: ".masonry-item"
                    });
                });
            });
        }
    </script>
@stop