@extends('layouts.main')

@section('title', 'Privacy Policy')

@section('stylesheets')
@stop

@section('header-scripts')
@endsection

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 {{--pl-30 pr-30--}}">
                    <h2 class="nmbottom nmtop text-center">
                        Privacy Policy
                    </h2>

                    <div class="clearfix"></div>
                    <hr style="width: 200px;" class="bc-blue mt-10">
                </div>
            </div>

            <div class="row mt-30">
                <div class="col-md-10 col-md-offset-1">
                    <p>
                        Pamperhub Ltd ("Pamperhub", "we", "us" or "our") is committed to protecting and respecting your privacy. Please read this Privacy Policy (the Policy) carefully as it contains important information about how we will use and treat your personal data. By visiting www.pamperhub.com (“Our Site”) you are accepting and consenting to the practices described in this policy.
                    </p>

                    <h3>
                        FOR CUSTOMERS AND VISITORS TO OUR SITE
                    </h3>

                    <p>
                        (If you are or wish to apply to be an Independent Professional, please also read the separate section below).
                    </p>
                    <p>
                        We act as agents of the Independent Professionals in providing our Booking Services and in storing any Client Consultation Forms. 
                    </p>
                    <p>
                        We may collect and process the following data about you:
                    </p>
                    <ul>
                        <li>
                            Information you give us. You may give us information about you by filling in forms on Our Site, or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you register to use Our Site, subscribe to our service, search Our Site for an Independent Professional, use Our Booking Services, when you report a problem with Our Site or contact us for any other reason. This may include your name, date of birth, gender, address of where the Service will be provided, e-mail address, phone number and payment information. It will also include any further information that you provide in relation to your booking, either to us or to your Independent Professional (and this may include information about your health).
                        </li>
                        <li>
                            Information we collect about you. With regard to each of your visits to Our Site we may automatically collect the following information:

                            <ul>
                                <li>
                                    Technical information, including the Internet protocol (IP) address used to connect your computer to the Internet; your login information; browser type, version, country and language; time zone setting; browser plug-in types and versions; operating system and platform;
                                </li>
                                <li>
                                    Information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from Our Site (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer service number.

                                </li>
                            </ul>
                        </li>

                        <li>
                            Device information: Each time you use our site on a mobile device we may also collect information about your device. This may include information on the type of mobile device that you are using and its unique device identifier (for example, the IMEI number, the device's mobile phone number, the MAC address of the device's wireless network interface or push ID), the mobile operating system that you are using and mobile network information.
                        </li>

                        <li>
                            Information we receive from other sources. Client Consultation Forms – Prior to your Service you may be asked to complete a Client Consultation Form in which you will be asked for details of any medical conditions. We will store these on behalf of the Independent Professionals.

                            <ul>

                                <li>
                                    Professionals' Notes – The Independent Professionals may make notes during the Service. These notes will record information that you give the Independent Professional and the Independent Professional's observations, e.g. whether there are any pets at the premises and whether you experienced any discomfort during the Service. If any of this information include details of your physical or mental health you will be asked for your explicit consent. We will store these notes and may make them available to other Independent Professionals that you choose to book through our Booking Service.
                                </li>

                                <li>
                                    We work closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers and search information providers) and may receive information about you from them.

                                </li>
                            </ul>
                        </li>
                    </ul>

                    <h4 class="text-bold mt-20">
                        USES MADE OF THE INFORMATION
                    </h4>
                    <p>
                        We use information held about you in the following ways:
                    </p>
                    <ul>
                        <li>To carry out our obligations arising from our Booking Services that you request from us; 
                        </li>
                        <li>
                            To store and share the Professionals' Notes. These may be shared with other Independent Professionals that you choose to book.
                        </li>
                        <li>
                            To store the Client Consultation Forms on behalf of the Independent Professionals 
                        </li>
                        <li>
                            To notify you about changes to our Booking Service that may affect you;
                        </li>
                        <li>
                            To ensure that content from Our Site and App(s) are presented in the most effective manner for you and for your computer or device; 
                        </li>
                        <li>
                            To improve our services;
                        </li>
                        <li>
                            To measure or understand the effectiveness of our advertising to you and others, and to deliver relevant advertising to you;
                        </li>
                        <li>
                            To contact you and make suggestions and recommendations to you about Services that may interest you or them by email, ‘phone, SMS, mail, push notifications or via our websites or the App.
                        </li>
                        <li>
                            To remind you of your appointments;
                        </li>
                        <li>
                            For administration and internal operations, including troubleshooting, data analysis, data security, testing, research, statistical and survey purposes;
                        </li>
                    </ul>

                    <h4 class="text-bold mt-20">
                        DISCLOSURE OF YOUR INFORMATION
                    </h4>
                    <p>
                        We will share your information with the Independent Professionals to enable them to provide the Services effectively and to effectively manage customer relations.
                    </p>
                    <p>
                        We also provide your information to selected third parties including:
                    </p>
                    <ul>
                        <li>
                            Suppliers for the performance of any contract we enter into with them or you.
                        </li>
                        <li>
                            Analytics and search engine providers that assist us in the improvement and optimisation of Our Site.
                        </li>
                    </ul>
                    <p>
                            We may disclose your personal information to third parties:
                    </p>
                    <ul>
                        <li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.
                        </li>
                        <li>
                            If Pamperhub or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.
                        </li>
                        <li>
                            If we are under a duty to disclose or share your personal data in order to comply with any legal obligation
                        </li>
                        <li>
                            To enforce or apply our terms of use <a href="{{ route('terms') }}">(Terms and Conditions)</a> and other agreements; or
                        </li>
                        <li>
                            To protect the rights, property, or safety of Pamperhub, the Independent Professionals or others.
                        </li>
                        <li>
                            To law enforcement agencies and regulators in connection with their investigations 
                        </li>
                        <li>
                            This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.
                        </li>
                    </ul>

                    <h4 class="text-bold mt-20">
                        WHERE WE STORE YOUR PERSONAL DATA
                    </h4>
                    <p>
                        The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, for example, the provision of support services of the processing of your payment details. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Policy.
                    </p>

                    <h4 class="text-bold mt-20">
                        SECURITY
                    </h4>
                    <p>

                        We use appropriate technical and organisational safeguards to protect your data. However you should be aware that the transmission of information via the internet is not completely secure. Any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.

                    </p>

                    <h4 class="text-bold mt-20">
                        YOUR RIGHTS
                    </h4>
                    <p>
                        Marketing - You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing use to collect your data by clicking the unsubscribe button in our emails or you can also exercise the right at any time by contacting us at legal@pamperhub.com
                    </p>
                    <p>
                        Access to Information – You have a right to access information held about you. Any access request may be subject to a fee of £10 to meet our costs in providing you with the information that we hold about you. In order to exercise this right please contact us at legal@pamperhub.com
                    </p>

                    <h4 class="text-bold mt-20">
                        CONTACT
                    </h4>
                    <p>
                        Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to legal@pamperhub.com, or Pamperhub Ltd., 1 Hamilton Road, Oxford, OX2 7PY. 
                    </p>

                    <h4 class="text-bold mt-20">
                        CHANGES TO OUR PRIVACY POLICY
                    </h4>
                    <p>

                        Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.

                    </p>

                    <h4 class="text-bold mt-20">
                        LINKS TO OTHER WEBSITES
                    </h4>
                    <p>

                        If you follow any links to any websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.

                    </p>

                    <h4 class="text-bold mt-20">
                        FOR INDEPENDENT PROFESSIONALS WISHING TO JOIN THE ONLINE PLATFORM

                    </h4>
                    <p>
                        Over and above what we say to Customers and visitors about privacy (as set out above) we will use, process and disclose your personal data in accordance with the DPA.
                    </p>
                    <p>
                        Your personal data will include:
                    </p>
                    <ul>
                        <li>
                            your facial image/photo
                        </li>
                        <li>
                            name,
                        </li>
                        <li>
                            address and contact details
                        </li>
                        <li>
                            location, using GPS technology
                        </li>
                        <li>
                            bank details
                        </li>
                        <li>
                            details of your training, qualifications and experience
                        </li>
                        <li>
                            results of all checks undertaken, including identity checks, background checks, document image checks and criminal record cheques
                        </li>
                        <li>
                            insurance details
                        </li>
                        <li>
                            eligibility to work in the UK
                        </li>
                        <li>
                            car ownership
                        </li>
                        <li>
                            language skills
                        </li>
                        <li>
                            feedback on your services or details of any complaints of clients
                        </li>
                        <li>
                            any other personal data that we may collect from you or otherwise receive from third parties
                        </li>
                        <li>
                            details of criminal convictions
                            Should you become one of our Independent Professionals you will be required to notify us promptly of any changes to your personal data.
                        </li>
                    </ul>
                    <p>
                        Our Uses and Disclosures of the Personal Data may include:
                    </p>
                    <ul>
                        <li>
                            Responding to any queries you may have
                        </li>
                        <li>
                            Contacting you in respect of your registration to join the online platform
                        </li>
                        <li>
                            Arranging for your orientation and verification
                        </li>
                        <li>
                            Undertaking identity, background, document image and criminal records checks
                        </li>
                        <li>
                            Introducing you (with the use of your image) via our website(s), or offline to members of the public who are seeking hair, beauty and massage services from  self-employed Independent Professionals
                        </li>
                        <li>
                            Managing our relationship with you, including the making of payments
                        </li>
                        <li>
                            Complying with and enforcing any agreement between you and us
                        </li>
                        <li>
                            Analysis and research
                        </li>
                        <li>
                            Assisting you to make the most of our services
                        </li>
                        <li>
                            Dealing with any complaints or investigations
                        </li>
                        <li>
                            Complying with statutory and other legal requirements
                        </li>
                        <li>
                            Processing in our legitimate interests otherwise as reasonably notified to you
                        </li>
                        <li>
                            Marketing
                        </li>
                        <li>
                            Assisting you to benefit from the service
                        </li>
                        <li>
                            Disclosure so far as is reasonably necessary externally for the above purposes
                        </li>
                        <li>
                            Disclosures for the purposes of undertaking identity, background, document image and criminal records checks.
                        </li>
                        <li>
                            We may monitor your use of our technology, systems and Customer personal data for the purposes of record keeping, to establish facts, to establish compliance with regulatory and self-regulatory procedures, to prevent, detect or investigate: crime; wrongdoing; the unauthorised use of our proprietary technology and/or Customer personal data; non-compliance with any agreement between us and you or any of our practices and procedures;
                        </li>
                    </ul>
                    <p>
                        We may transfer your personal data outside the European Economic Area in order to carry on our business.
                    </p>
                    <p>
                        Independent Professionals will comply with the DPA, our privacy policy and other policies and procedures notified to them by us from time to time. In respect of any Customer personal data, including data collected whilst making notes of the Services or accessed whilst viewing such notes through our technology Independent Professionals will:
                    </p>
                    <ul>
                        <li>
                            use such personal data only in so far as is necessary for the purpose of carrying out Services on Customers and for no other purpose (including, without limitation, their own marketing purposes);
                        </li>
                        <li>
                            not disclose such personal data unless specifically authorised by us to do so;
                        </li>
                        <li>
                            comply promptly with our instructions in respect of such personal data;
                        </li>
                        <li>
                            not disclose such personal data to any third party other than on our written instructions or as required by law;
                        </li>
                        <li>
                            not allow such personal data to be transferred out of the European Economic Area;
                        </li>
                        <li>
                            promptly assist us with requests in respect of such personal data;
                        </li>
                        <li>
                            when making notes during the Services, ensure that any such personal data included therein is accurately recorded and promptly amended if inaccurate;
                        </li>
                        <li>
                            immediately forward to us any communications from Customers, regulatory bodies and other third parties concerning such personal data and not respond to or act on such communications without our prior agreement; and
                        </li>
                        <li>
                            on request provide to us such personal data in the format reasonably specified.
                        </li>
                        <li>
                            not retain any (or any copies of Customer personal data) on completion of the Service other than for the purpose of submission of notes to us. After such submission, the Independent Professional shall securely destroy such notes.
                        </li>
                        <li>
                            will keep, transport and dispose of any Customer personal data securely to prevent unauthorised disclosure, access, loss, destruction or damage to such personal data.
                        </li>
                    </ul>
                </div>
                {{--<h4 class="text-bold mt-20">--}}

                {{--</h4>--}}
                {{--<p>--}}

                {{--</p>--}}
            </div>
        </div>
    </section>

@stop

@section('scripts')
@stop
