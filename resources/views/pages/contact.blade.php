@extends('layouts.main')

@section('title', 'Contact Us')

@section('stylesheets')
  {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
@stop

@section('header-scripts')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')

  {{--@if ($setting->breadcrumbs == 1)--}}
    {{--@include('partials._breadcrumbs')--}}
  {{--@endif--}}

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div id="page-heading" class="mb-50">
            <h1 class="nmtop text-uppercase">Contact Us</h1>
            <h3 class="nmargin font-arapey text-italic text-thin text-red after-dg">Subtitle Here</h3>
          </div>

          {!! Form::open(['url' => 'contact', 'method' => 'POST', 'data-parsley-validate' => '', 'id' => 'contact-form']) !!}
            <div class="form-group">
              {{ Form::label('email', 'Email:') }}
              {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => '']) }}
            </div>

            <div class="form-group">
              {{ Form::label('subject', 'Subject:') }}
              {{ Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject', 'required' => '', 'minlength' => '3']) }}
            </div>

            <div class="form-group">
              {{ Form::label('message', 'Message:') }}
              {{ Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Message', 'required' => '', 'minlength' => '10']) }}
            </div>

            <div class="form-group">
              <div class="g-recaptcha" data-sitekey="6LfiLSkTAAAAAASMxP1R4oX_2tDlicRJTXNSLKhs"></div>
            </div>

            <div class="form-group">
              {{ Form::submit('Send Message', ['class' => 'btn btn-success']) }}
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </section>

  <!-- Call To Action Module -->
  <section id="page-cta" class="pt-20 pb-20 mh-350">
    <div class="center-content container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 text-white text-center">
          <svg width="30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          	 viewBox="-10 -10 200 300" enable-background="new 0 0 187.634 289.066" xml:space="preserve">
            <path class="parallax-logo-symbol" fill="none" stroke="white" stroke-width="8" stroke-miterlimit="10" d="M147.077,120.983"/>
            <polygon class="parallax-logo-symbol" fill="none" stroke="white" stroke-width="8" stroke-miterlimit="10" points="6.901,71.381 41.601,157.681 1.401,210.181
          	102.401,287.681 183.601,187.981 170.401,146.781 186.501,111.081 185.538,109.551 117.401,1.381 "/>
            <line class="parallax-logo-symbol" fill="none" stroke="white" stroke-width="8" stroke-miterlimit="10" x1="79.001" y1="181.981" x2="186.501" y2="111.081"/>
            <polyline class="parallax-logo-symbol" fill="none" stroke="white" stroke-width="8" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
          	7.901,73.681 79.001,181.981 1.401,210.181 "/>
            <line class="parallax-logo-symbol" fill="none" stroke="white" stroke-width="8" stroke-miterlimit="10" x1="102.401" y1="287.681" x2="170.401" y2="146.781"/>
          </svg>
          <h2 class="after-center-white text-uppercase text-bold">View our projects</h2>
          <h3 class="mb-30 text-normal">View our past work to see what we can do for you.</h3>
          <a href="{{ url('projects') }}" class="btn btn-lg btn-white-alt text-red mb-10">Past projects &raquo;</a>
        </div>
      </div>
    </div>
  </section>

@stop

@section('scripts')
  {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
@stop
