@extends('layouts.main')

@section('title', 'Home')

@section('stylesheets')
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {!! Html::style('dist/css/home.css') !!}
@endsection

@section('content')

    {{ Session::forget('error') }}

    <section id="home-hero">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <p class="text-center text-white" style="text-shadow: 1px 1px 1px #666">
                        {{ $settings->home_tagline }}
                    </p>
                </div>
            </div>

            <div class="row mt-10">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    {!! Form::open(['route' => 'search.index', 'method' => 'GET', 'role' => 'search', 'class' => 'form-inline']) !!}
                    <div class="row">
                        <div class="col-xs-6">
                            {{ Form::text('search', null, ['class' => 'form-control', 'id' => 'search-input', 'required' => '', 'title' => 'Enter Post Code', 'placeholder' => 'Enter Post Code', 'minLength' => '3']) }}
                        </div>

                        <div class="col-md-4 col-sm-4 hidden">
                        <span class="hidden">
                        {{ Form::select('service', $treatment, null, ['class' => ' select2-standard nb-radius', 'required' => '', 'readonly' => '']) }}
                        </span>

                        <span class="text-left form-control disabled" style="width: 100%">
                        Hair Colouring
                        </span>
                        </div>


                        <div class="col-xs-6">
                            {{ Form::button('<i class="fa fa-search"></i> search', ['class' => 'btn btn-md btn-block btn-blue', 'type' => 'submit']) }}
                        </div>

                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    <section class="bg-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    {{--Tetimonial slider here.--}}
                </div>
            </div>
        </div>
    </section>

    {{--<section>--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-sm-12">--}}
    {{--@if (Auth::guest())--}}
    {{--<a href="{{ route('login') }}">Login</a>--}}
    {{--<br>--}}
    {{--<a href="{{ route('register') }}">Register</a>--}}
    {{--@else--}}
    {{--<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>  --}}


@stop

@section('scripts')
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}

    <script type="text/javascript">
        $(".select2-standard").select2({
            width: '100%',
        });
    </script>
@stop
