@extends('layouts.customer')

@section('title', 'My Bookings')

@section('stylesheets')
@endsection


@section('header-scripts')
@endsection

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="nmbottom">My Bookings</h2>
                    <hr class="supplier-hr">
                </div>
            </div>


            <div class="row mt-20">
                <div class="col-lg-3 col-md-4 col-sm-4">
                    @include('partials.customer._nav')
                </div>

                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">

                    @if($appointments->count() === 0)

                        <p>
                            You do not have any appointments booked.
                        </p>

                    @else

                    @if($next_appointment)

                            <h4 class="mb-20">Your next appointment:</h4>

                            <div class="appointment bg-light-gray nborder">
                                <div class="row">
                                    <div class="appointment-date">
                                        <span class="day">
                                            {{ date('j M' , strtotime($next_appointment->date)) }}
                                        </span>
                                        <hr>
                                        <span class="month">
                                            {{ date('Y' , strtotime($next_appointment->date)) }}
                                        </span>
                                    </div>

                                    <div class="appointment-status {{ $next_appointment->supplier_confirmed === 1 ? 'bg-dark-green' : 'bg-danger' }} " >
                                        <span class="text-white">
                                            @if($next_appointment->supplier_confirmed === 1)
                                                Confirmed
                                            @else
                                                Not Confirmed
                                            @endif
                                        </span>

                                    </div>

                                    <div class="col-sm-8 col-xs-8">
                                        <p>
                                            <strong>Therapist:</strong>
                                            <span class="clearfix visible-xs"></span>
                                            {{ $next_appointment->supplier->user->firstname }}
                                            {{ $next_appointment->supplier->user->lastname }}

                                            (#00{{ $next_appointment->customer->id }})
                                        </p>
                                        <p>
                                            <strong>Duration:</strong>
                                            {{ $next_appointment->duration }}{{ $next_appointment->duration === '1' ? 'hr' : 'hrs' }}
                                        </p>
                                        <p>
                                            <strong class="">
                                                Time:
                                            </strong>
                                            <span class="text-capitalize">{{ $next_appointment->time_of_day }}</span>

                                            @ {{ $next_appointment->time }}
                                        </p>
                                        <p>
                                            <strong>Service:</strong>
                                            {{ $next_appointment->service->treatment->name }}
                                        </p>
                                        <p class="">
                                            <strong>
                                                Company:
                                            </strong>
                                            {{ $next_appointment->supplier->company }}
                                        </p>

                                        <p class="nmbottom">
                                            <strong>
                                                Company Contact:
                                            </strong>
                                            {{ $next_appointment->supplier->contact_number }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            {{--<div class="clearfix mb-10"></div>--}}
                            <hr class=" mb-30">
                    @endif



                        @if($page === null || $page === 1)

                            @if($appointments->count() === 1)

                            @else



                                <h4 class="nmtop">Upcoming Appointment:</h4>

                            @endif

                        @elseif($page > 1)


                            <h4 class="nmtop">Upcoming Appointment:</h4>

                        @endif

                    <p class="mb-20">
                        Please note that once suppliers confirm the appointment a specific time will not be added.
                    </p>

                        <div class="appointment-list">
                            @foreach($appointments as $appointment)

                                @if($next_appointment)
                                    <div class="appointment bg-white {{ $next_appointment->id === $appointment->id ? 'hidden' : '' }}">
                                @else
                                    <div class="appointment bg-white {{ $appointment->canceled === 1 ? 'disabled' : '' }}">
                                @endif


                                    <div class="row">
                                        <div class="appointment-date">
                                        <span class="day">
                                            {{ date('j M' , strtotime($appointment->date)) }}
                                        </span>
                                            <hr>
                                            <span class="month">
                                            {{ date('Y' , strtotime($appointment->date)) }}
                                        </span>
                                        </div>

                                        @if($appointment->supplier_confirmed === 1)
                                            <div class="appointment-status bg-dark-green" >
                                                <span class="text-white">
                                                    Confirmed
                                                </span>
                                            </div>
                                        @elseif($appointment->canceled === 1)

                                            <div class="appointment-status bg-dark-red" >
                                                <span class="text-white">
                                                    CANCELED
                                                </span>
                                            </div>
                                        @else
                                            <div class="appointment-status bg-beige" >
                                                <span class="">
                                                    Not Confirmed
                                                </span>
                                            </div>
                                        @endif


                                        <div class="col-sm-8 col-xs-8">
                                            <p>
                                                <strong>Therapist:</strong>
                                                <span class="clearfix visible-xs"></span>
                                                {{ $appointment->supplier->user->firstname }}
                                                {{ $appointment->supplier->user->lastname }}

                                                (#00{{ $appointment->customer->id }})
                                            </p>
                                            <p>
                                                <strong>Duration:</strong>
                                                {{ $appointment->duration }}{{ $appointment->duration === '1' ? 'hr' : 'hrs' }}
                                            </p>
                                            <p>
                                                <strong class="">
                                                    Time:
                                                </strong>
                                                <span class="text-capitalize">{{ $appointment->time_of_day }}</span>
                                                @if($appointment->supplier_confirmed === 1)
                                                    @ {{ $next_appointment->time }}
                                                @endif
                                            </p>
                                            <p>
                                                <strong>Service:</strong>
                                                {{ $appointment->treatment->name }}
                                            </p>
                                            <p class="nmbottom">
                                                <strong>
                                                    Company:
                                                </strong>
                                                {{ $appointment->supplier->company }}
                                            </p>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </div>


                        <div class="clearfix"></div>

                        <div class="pull-left pt-5">
                            Page <strong>{!! $appointments->currentPage() !!}</strong>
                            of {!! $appointments->lastPage() !!}
                        </div>

                        <div class="pull-right">
                            {!! $appointments->links() !!}
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </section>


@stop

@section('scripts')
@stop
