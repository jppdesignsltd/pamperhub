@extends('layouts.customer')

@section('title', 'My Account')

@section('stylesheets')
@endsection


@section('header-scripts')
@endsection

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="nmbottom">My Account</h2>
                    <hr class="supplier-hr">
                </div>
            </div>

            <div class="row mt-20">
                <div class="col-lg-3 col-md-4 col-sm-4">
                    @include('partials.customer._nav')
                </div>

                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-sm-8">
                    <div class="panel panel-body panel-default pb-10">
                        <div class="row">
                            <div class="col-sm-6">
                                <p><strong class="mr-10">First Name:</strong> {{ $customer->user->firstname }}</p>
                            </div>

                            <div class="col-sm-6">
                                <p><strong class="mr-10">Last Name:</strong> {{ $customer->user->lastname }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <p class=""><strong class="mr-10">Email:</strong> {{ $customer->user->email }}</p>
                            </div>
                            <div class="col-sm-6">
                                <p class=""><strong class="mr-10">User
                                        Since:</strong> {{ date('Y', strtotime($customer->created_at)) }}</p>
                            </div>
                        </div>


                    </div>

                    <div class="well">

                        {{ Form::model($customer, ['route' => ['customer.update', $customer->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'files' => true, 'id' => 'customerProfile', 'disabled' => '']) }}

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('dob', 'Date of Birth') }}
                                </div>
                                <div class="col-sm-9">
                                    <span class='input-group date' id='dob'>
                                        <span class="hidden">
                                            {{ $customer_dob = date('d/m/Y', strtotime($customer->dob)) }}
                                        </span>

                                        {!! Form::text('dob', $customer_dob, ['id'=> 'dob', 'class' => 'form-control', 'required' => '', 'disabled' => '', 'placeholder' => ""]) !!}

                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $(function () {
                                    var today = new Date();

                                    $('#dob').datetimepicker({
                                        useCurrent: false,
                                        maxDate: today.setFullYear(today.getFullYear() - 18),
                                        viewMode: 'decades',
                                        format: 'DD/MM/Y'
                                    });
                                });
                            </script>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('contact_number', 'Contact Number') }}
                                </div>
                                <div class="col-sm-9">
                                    {{ Form::text('contact_number', null, ['class' => 'form-control', 'required' => '', 'minLength' => '11', 'maxLength' => '11', 'data-parsley-error-message' => 'Please enter a valid phone number', 'disabled' => '']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('address1', 'Address Line 1') }}
                                </div>
                                <div class="col-sm-9">
                                    {{ Form::text('address1', null, ['class' => 'form-control', 'required' => '', 'disabled' => '']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('address2', 'Address Line 2') }}
                                </div>
                                <div class="col-sm-9">
                                    {{ Form::text('address2', null, ['class' => 'form-control', 'disabled' => '']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('city', 'City') }}
                                </div>
                                <div class="col-sm-9">
                                    {{ Form::text('city', null, ['class' => 'form-control', 'required' => '', 'disabled' => '']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('postal_code', 'Post Code') }}
                                </div>
                                <div class="col-sm-9">
                                    {{ Form::text('postal_code', null, ['class' => 'form-control', 'required' => '', 'disabled' => '']) }}
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="form-group text-right">
                        <a id="editBtn" onclick="activateSubmit('customerProfile', 'editBtn')"
                           class="btn btn-md btn-beige">Edit</a>
                        {{ Form::submit('update', ['class' => 'btn btn-md btn-blue hidden', 'id' => 'customerProfileBtn']) }}

                        <div class="clearfix"></div>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </section>


@stop

@section('scripts')
@stop
