{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Services')

@section('stylesheets')

@endsection

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-clone mr-10"></i> Service Listings
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>

    </div>

    <div id="action_panel" class="bg-beige">
        <h5>There {{ $services->count() === 1 ? 'is' : 'are' }} <span class="badge">{{ $services->count() }}</span>
            listed service{{ $services->count() === 1 ? '' : 's' }}.</h5>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">
            @if($services->count() === 0)
                <div class="row">
                    <div class="col-sm-12">
                        <p>No supplier service listings have been created.</p>
                    </div>
                </div>

            @else
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-body panel-default npadding">
                            <div class="table-responsive">
                                <table id="services-index" class="table table-bordered">
                                    <thead class="bg-blue nb-radius">
                                    <th style="width:250px;">Title</th>
                                    <th style="width:250px;">Company</th>
                                    <th style="width:100px;">Service</th>
                                    <th>Price p/h</th>
                                    <th>Promotion</th>
                                    <th>Sale Price</th>
                                    <th style="width:80px;">Status</th>
                                    {{-- <th style="width:300px;"></th> --}}
                                    </thead>

                                    <tbody>
                                    @foreach($servicesPaginate as $service)

                                        <tr id="service-{{ $service->id }}">
                                            <td>
                                                <a href="{{ route('service.edit', $service->id) }}"><span
                                                            class="">{{ $service->title }}</span></a>
                                            </td>
                                            <td>
                                                {{ $service->supplier->company }}
                                            </td>
                                            <td>
                                                <span class="hidden">{!! $treat = $treatments->where('id', $service->service_type)->first() !!}</span>
                                                {{ $treat->name }}
                                            </td>
                                            <td class="slug">
                                                <strong>&pound;{{ $service->price }}</strong>
                                            </td>
                                            <td class="promotion-label">
                                                @if( $service->promotion === 0 )
                                                    <label class="label label-danger {{-- nbradius --}}">Off</label>
                                                @else
                                                    <label class="label label-success {{-- nbradius --}}">on</label>
                                                @endif
                                            </td>
                                            <td class="promotion_price">
                                                {{ $service->promotion_price !== null ? "&pound;$service->promotion_price" : 'N/A' }}
                                            </td>
                                            <td class="status-label">
                                                @if( $service->status == 'draft' )
                                                    <label class="label label-danger {{-- nbradius --}}">{{ $service->status }}</label>
                                                @else
                                                    <label class="label label-success {{-- nbradius --}}">{{ $service->status }}</label>
                                                @endif
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="pl-15 pull-left pt-5">
                        Page <strong>{!! $servicesPaginate->currentPage() !!}</strong>
                        of {!! $servicesPaginate->lastPage() !!}
                    </div>

                    <div class="pull-right pr-15">
                        {!! $servicesPaginate->links() !!}
                    </div>
                </div>
            @endif

            <div class="row mt-30">
                <div class="col-sm-12">
                    <a href="{{ route('admin') }}" class="btn btn-md btn-beige">back</a>
                </div>
            </div>
        </div>
    </section>


@stop

@section('scripts')

@stop
