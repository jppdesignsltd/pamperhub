{{--Dashboard Page--}}

@extends('layouts.admin')

@section('title', 'Edit Service')

@section('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    {!! Html::style('bower_components/parsleyjs/src/parsley.css') !!}
    {!! Html::style('bower_components/select2/dist/css/select2.min.css') !!}
    {!! Html::style('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') !!}
@stop

@section('header-scripts')
    {!! Html::script('bower_components/moment/min/moment.min.js') !!}
    {!! Html::script('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::script('bower_components/tinymce/tinymce.min.js') !!}

    <script type="text/javascript">
        tinymce.init({
            selector: '#main-body',
            menubar: false,
            toolbar: 'undo redo | bold italic underline strikethrough hr | alignleft aligncenter alignright alignjustify | bullist numlist | link | styleselect | fullscreen',
            plugins: 'link wordcount hr fullscreen lists advlist'
        });
    </script>

    {!! Html::script('bower_components/bootstrap-filestyle/src/bootstrap-filestyle.min.js') !!}
@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-clone mr-10"></i> Edit Service
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>
    {!! Form::model($service, ['route' => ['service.update', $service->id, 'page=admin'], 'method' => 'PUT', 'id' => 'supplierService', 'data-parsley-validate' => '', 'files' => true]) !!}

    <div id="action_panel">
        <h3><strong>Title:</strong> {{ $service->title }}</h3>
        {{ Form::submit('update listing', ['class'=>'btn btn-md btn-white']) }}
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-7">
                    <div class="panel panel-default panel-body bg-light-gray npleft npright">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="nptop">Company</label>
                                </div>

                                <div class="col-sm-9">
                                    {{ $service->supplier->company }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('title', 'Title:') }}
                                </div>

                                <div class="col-sm-9">
                                    {{ Form::text('title', $service->title, ['class' => 'form-control', 'required' => '', 'maxlength' => '255']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('slug', 'Service Link:') }}
                                </div>

                                <div class="col-sm-9">
                                    {{ Form::text('slug', $service->slug, ['class' => 'form-control', 'required' => '', 'id' => 'slug', 'maxlength' => '255', 'minlength' => '1']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-15">
                            <div class="row">
                                <div class="col-sm-3">
                                    {{ Form::label('price', 'Price (p/h):', ['class' => '']) }}
                                </div>

                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon addon-pound bg-white text-bold" style="background-color: #fff !important;">&pound;</span>
                                        {{ Form::number('price', null, ['class' => 'form-control', 'required' => '', 'min' => '0.01', 'max' => '500', 'step' => '0.01']) }}
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    {{ Form::label('description', 'Service Description:') }}
                                    <div class="clearfix mb-10"></div>
                                    {{ Form::textarea('description', null, array('class' => 'form-control', 'id' => 'main-body')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="well bg-light-gray npleft npright pt-15 pb-15">
                        <div class="row">
                            <div class="col-sm-12">
                                <h5 class="text-bold">
                                    Cover Image:

                                    <span class="pull-right">
                                        <a id="clearImage" style="text-decoration: none;" onclick="clearFilestyle()"
                                           class="text-bold label-danger label-xs label {{ $service->cover_image === null || $service->cover_image === '' ? 'hidden' : '' }}">
                                            <i class="fa fa-times"></i> Remove
                                        </a>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="clearfix mb-5"></div>

                        @if($service->cover_image === '' || $service->cover_image === null)
                            <div class="row">

                                <div id="image_preview-cont" class="col-sm-12 text-right hidden">

                                    <img id="image_preview" alt="Image Preview" class="img-responsive">

                                </div>

                                <div class="clearfix"></div>

                                <div id="cover_image-cont" class="col-sm-12">

                                    {{ Form::file('cover_image', ['id' => 'cover_image', 'class' => 'filestyle','data-buttonName' => '', 'data-input' => 'true', 'data-placeholder' => 'No file chosen', 'data-buttonName' => 'btn-beige-plain',  'onchange' => 'loadFile(event)']) }}
                                </div>

                            </div>
                        @else
                            <div class="row">

                                <div id="image_preview-cont" class="col-sm-12 text-right">
                                    <img id="image_preview" alt="Image Preview"
                                         src="{{ asset('uploads/services/' . $service->cover_image) }}"
                                         class="img-responsive">
                                </div>

                                <div class="clearfix"></div>

                                <div id="cover_image-cont" class="col-sm-12 hidden">
                                    {{ Form::file('cover_image', ['id' => 'cover_image', 'class' => 'filestyle','data-buttonName' => '', 'data-input' => 'true', 'data-placeholder' => 'No file chosen', 'data-buttonName' => 'btn-beige-plain',  'onchange' => 'loadFile(event)']) }}
                                </div>

                            </div>

                            {{ Form::text('remove_image', 'remove', ['class' => 'form-control hidden', 'disabled' => '', 'id' => 'remove-image']) }}
                        @endif

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('service_type', 'Service Type:', ['class'=>'nptop']) }}
                                </div>

                                <div class="col-sm-7">
                                    <span class="hidden">{!! $treat = $treatments->where('id', $service->service_type)->first() !!}</span>
                                    {{ $treat->name }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('live_date', 'Live Date:') }}
                                </div>

                                <div class="col-sm-7">
                                    <div id="live_date_placeholder" style="padding-top: 7px">
                                        {{ date('j M, Y @ h:ia', strtotime($service->live_date)) }}

                                        <a onclick="revealInput(live_date_placeholder, live_date)">
                                            <i class="fa fa-pencil ml-10" ></i>
                                        </a>
                                    </div>
                                    {{ Form::text('live_date', null, ['class' => 'form-control hidden', 'id' => 'live_date', 'placeholder' => date('j M, Y @ h:ia', strtotime($service->live_date))]) }}

                                    <script>
                                        $(function () {
                                            $("#live_date").datetimepicker({
                                                format: 'DD/MM/YYYY H:mm'
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('status', 'Status:') }}
                                </div>

                                <div class="col-sm-7">
                                    <select name="status" id="status" class="form-control">
                                        <option value="published" {{ $service->status === 'published' ? 'selected' : '' }}>
                                            Published
                                        </option>
                                        <option value="draft"{{ $service->status === 'draft' ? 'selected' : '' }}>
                                            Draft
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-20">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('promotion', 'Sale:', ['class' => 'nptop']) }}
                                </div>

                                <div class="col-sm-7">
                                    <label class="npadding">
                                        {{ Form::radio('promotion', 1, false, ['id' => 'promotion-on', 'data-radio-all-off' => 'true', 'data-on-color' => 'success', 'data-off-color' => 'danger', 'data-size' => 'mini']) }}
                                    </label>

                                    <label class="npleft">
                                        {{ Form::radio('promotion', 0, true, ['id' => 'promotion-off', 'class' => 'hidden']) }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-15">
                            <div class="row">
                                <div class="col-sm-5">
                                    {{ Form::label('promotion_price', 'Sale Price:', ['class' => '']) }}
                                </div>

                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <span id="promo-pound" class="npleft input-group-addon addon-pound text-bold" style="background-color: transparent !important;">&pound;</span>

                                        {{ Form::number('promotion_price', null, ['class' => 'form-control', 'disabled' => '', 'min' => '0.01', 'step' => '0.01']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('admin.services') }}" class="btn btn-md btn-beige">back</a>
                </div>
            </div>
        </div>
    </section>


    {!! Form::close() !!}
@stop

@section('scripts')
    {!! Html::script('bower_components/parsleyjs/dist/parsley.min.js') !!}
    {!! Html::script('bower_components/angular/angular.min.js') !!}
    {!! Html::script('bower_components/angular-slugify/angular-slugify.js') !!}
    {!! Html::script('dist/js/supplierCreate.js') !!} {{-- Slugify Angular Script --}}
    {!! Html::script('bower_components/select2/dist/js/select2.min.js') !!}
    {!! Html::script('bower_components/bootstrap-switch/dist/js/bootstrap-switch.js') !!}

    <script type="text/javascript">
        
        $("#promotion-on").bootstrapSwitch();

        $('#promotion-on').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state === true) {
                $('#promotion-on').prop("checked", true);
                $('#promotion-off').prop("checked", false);
                $('#promotion_price').removeAttr('disabled');
                $('#promotion_price').attr('required', '');
                $('#promo-pound').attr('style', 'background-color:#FFF !important;');
                $('#promo-pound').toggleClass('npleft');

            } else {
                $('#promotion-on').prop("checked", false);
                $('#promotion-off').prop("checked", true);
                $('#promotion_price').attr('disabled', '');
                $('#promotion_price').removeAttr('required');
                $('#pound-pound').removeAttr('style');
                $('#promo-pound').attr('style', 'background-color:transparent !important;');
                $('#promo-pound').toggleClass('npleft');

            }
        });

        if ($('#promotion-on').is(':checked')) {
            $('#promotion_price').removeAttr('disabled');
            $('#promotion_price').attr('required', '');
            $('#promo-pound').attr('style', 'background-color:#FFF !important;').toggleClass('npleft');
        }

        $("#service_type").select2({
            width: '100%'
        });

        $("#service_type").select2().val( {!! json_encode($service->service_type) !!} ).trigger('change');



    </script>
@endsection
