@extends('layouts.admin')

@section('title', 'View Service')

@section('stylesheets')
@stop

@section('header-scripts')

@stop

@section('content')

    <div id="admin_header">
        @include('partials.admin._toggle')

        <h2>
            <i class="fa fa-clone mr-10"></i> Show Service
        </h2>

        <span class="pull-right">
            <a href="{{ route('/') }}" class="btn btn-md btn-blank"><i class="fa fa-home"></i> Visit Site</a>
        </span>
    </div>

    <div id="action_panel" class="bg-beige">
        <h3>
            {{ $service->title }}

            @if($service->status === 'published')
                <span class="label label-success pull-right">{{ $service->status }}</span>
            @else
                <span class="label label-danger pull-right">{{ $service->status }}</span>
            @endif
        </h3>
        <div class="clearfix"></div>
    </div>

    <section>
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-7">
                    <div class="">

                        <p>
                            <strong>Company:</strong>
                            {{ $service->supplier->company }}
                        </p>

                        <p>
                            <strong>Category:</strong> {{ $treatments->where('id', $service->service_type)->first()->name }}
                        </p>

                        <p>
                            <strong>Description:</strong>
                            {!! $service->description !!}
                        </p>

                        <p>
                            <strong>Price (p/h)</strong>
                            &pound;{{ $service->price }}
                        </p>

                        <p>
                            <strong>Promotion:</strong>
                            @if($service->promotion === 1)
                                On
                            @else
                                Off
                            @endif
                        </p>
                        @if($service->promotion === 1)
                            <p>
                                <strong>Promotion Price (p/h):</strong>
                                &pound;{{ $service->promotion_price }}
                            </p>
                        @endif

                        <p class="">
                            <strong>Supplier Address:</strong>
                        </p>
                        <p>
                            {{ $service->supplier->address1 }},
                            {{ $service->supplier->address2 }},
                            {{ $service->supplier->city }},
                            {{ $service->supplier->postal_code }}
                        </p>

                        <div class="clearfix mb-30"></div>

                        <a href="{{ route('admin.services') }}" class="btn btn-md btn-beige ">back</a>
                        <a href="{{ route('service.edit', $service->id) }}" class="btn btn-md btn-blue ml-20">edit</a>

                    </div>
                </div>

                <div class="col-sm-5">
                    @if($service->cover_image === null || $service->cover_image === '')
                        <p>There is no service cover image</p>
                    @else

                        <div class="well pl-15 pr-15">
                            <img id="image_preview" alt="Image Preview"
                                 src="{{ asset('uploads/services/' . $service->cover_image) }}"
                                 class="img-responsive">
                        </div>

                    @endif
                </div>
            </div>


        </div>
    </section>


    {!! Form::close() !!}
@stop

@section('scripts')

@endsection
