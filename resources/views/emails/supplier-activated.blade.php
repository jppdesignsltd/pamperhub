Hello {{ $firstname }} {{ $lastname }},
<br><br>
Your account has been activated. You can start publishing your services now.
<br>
<a href="http://www.pamperhub.com/supplier">www.pamperhub.com/supplier</a>
<br><br>
Your Username is: {{ $email }}
<br><br>
Start advertising your services now by adding service listings today. Let people find you!
<br><br>
<a href="http://www.pamperhub.com/supplier">Click here</a> to get started.
<br><br>
Regards, the PamperHub Team.