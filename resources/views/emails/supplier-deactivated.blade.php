Hello {{ $firstname }} {{ $lastname }},
<br><br>
Your account has been deactivated.
<br><br>
If you believe this has been done in error please get in touch with support@pamperhub.com referencing your username.
<br><br>
Regards, the PamperHub Team.