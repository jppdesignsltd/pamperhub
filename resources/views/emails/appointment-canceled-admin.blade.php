Hello PamperHub,
<br><br>
The appointment: {{ $orderNo }}, has been canceled.
<br><br>
If this was done by mistake please rectify this asap, as all parties have been notified.
<br><br>
Regards, the PamperHub Team.