Hello {{ $firstname }} {{ $lastname }},
<br><br>
Your order:{{ $orderNo }}, was unsuccessful please contact a member of the team to investigate the issue further.
<br><br>
If you canceled the payment then feel free to re-book the appointment.
<br><br>
Regards, the PamperHub Team.