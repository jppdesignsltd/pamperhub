Hello PamperHub,
<br><br>
A new supplier has completed their registration. Please review their application via your admin panel and activate their account.
<br><br>
Supplier Username is: {{ $email }}
<br><br>
Regards, the PamperHub Team.