Hello {{ $supplier_firstname }} {{ $supplier_lastname }},
<br><br>
Your appointment: {{ $orderNo }}, has been restored and is no longer canceled. It will appear in your bookings area in your account. PamperHub are sorry for any inconvenience caused.
<br><br>
Regards, the PamperHub Team.
