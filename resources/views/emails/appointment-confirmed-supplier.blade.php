Hello {{ $supplier_firstname }} {{ $supplier_lastname }},
<br><br>
The appointment: {{ $orderNo }}, has been confirmed. It will be on <strong>{{ date('jS F, Y' , strtotime($date)) }}</strong> at <strong>{{ date('h:i A', strtotime($time)) }}</strong>. If you need to update this then please do so via your account or contact support@pamperhub.com.
<br><br>
Regards, the PamperHub Team.