Hello {{ $firstname }} {{ $lastname }},
<br><br>
Congratulations your payment for order: {{ $orderNo }}, was successful. Please see the details below.
<br><br>
<table cellspacing="0" cellpadding="5" border="1" style="width: 100%; text-align: left !important;">
    <thead>
    <tr>
        <th>Service</th>
        <th>Order No.</th>
        <th>Date</th>
        <th>Total Price</th>
        <th>Payment Method</th>
    </tr>

    </thead>
    <tbody>
        <tr>
            <td>
                {{ $treatmentName }}
            </td>
            <td>
                {{ $orderNo }}
            </td>
            <td>
                {{ date('jS F, Y' , strtotime($date)) }}
            </td>
            <td>
                &pound;{{ $price }}
            </td>
            <td>
                Paypal
            </td>
        </tr>
    </tbody>
</table>
<br><br>
Thank you for booking your {{ $treatmentName }} appointment with PamperHub. Please note that this appointment is not confirmed yet, once it has been confirmed a specific time will be added to your appointment, you will be notified via email when this is done.
<br><br>
Regards, the PamperHub Team.