Hello {{ $supplier_firstname }} {{ $supplier_lastname }},
<br><br>
Your appointment: {{ $orderNo }}, has been canceled. Please get in touch with support@pamperhub.com to get more information.
<br><br>
If this was done by mistake then please get in touch ASAP.
<br><br>
Regards, the PamperHub Team.