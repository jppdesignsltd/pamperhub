Hello {{ $customer_firstname }} {{ $customer_lastname }},
<br><br>
Your appointment: {{ $orderNo }}, has been canceled. Please get in touch with support@pamperhub.com to get more information. If a refund is due then this can be processed by a member of the team.
<br><br>
Regards, the PamperHub Team.