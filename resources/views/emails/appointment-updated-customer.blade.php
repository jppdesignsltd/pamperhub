Hello {{ $customer_firstname }} {{ $customer_lastname }},
<br><br>
Your appointment: {{ $orderNo }}, has been updated. It will now be on <strong>{{ date('jS F, Y' , strtotime($date)) }}</strong> at <strong>{{ date('h:i A', strtotime($time)) }}</strong>. If you need to make any changes please get in touch with support. (support@pamperhub.com).
<br><br>
Regards, the PamperHub Team.