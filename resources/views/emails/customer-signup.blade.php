Hello {{ $firstname }} {{ $lastname }},
<br><br>
Thank you for registering your account.
<br><br>
Your Username is: {{ $email }}
<br><br>
You can start benefiting now from everything PamperHub has to offer.
<br><br>
<a href="http://www.pamperhub.com">Click here</a> to book a mobile therapist now.
<br><br>
Regards, the PamperHub Team.