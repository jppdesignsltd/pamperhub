Hello {{ $firstname }} {{ $lastname }},
<br><br>
Thank you for registering your supplier account. Please complete your registration using the link below:
<br>
<a href="http://www.pamperhub.com/supplier">www.pamperhub.com/supplier</a>
<br><br>
Your Username is: {{ $email }}
<br><br>
Regards, the PamperHub Team.