Hello {{ $firstname }} {{ $lastname }},
<br><br>
Thank you for completing your supplier registration.
<br><br>
Your Username is: {{ $email }}
<br><br>
A member of the team will review your application and get back to you shortly.
<br><br>
Regards, the PamperHub Team.