Hello {{ $firstname }} {{ $lastname }},
<br><br>
Your account has been re-activated.
<br>
Sorry for any inconvenience.
<br><br>
Regards, the PamperHub Team.