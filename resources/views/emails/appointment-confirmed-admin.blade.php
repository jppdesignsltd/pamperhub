Hello PamperHub,
<br><br>
The appointment: {{ $orderNo }}, has been confirmed. It will be on <strong>{{ date('jS F, Y' , strtotime($date)) }}</strong> at <strong>{{ date('h:i A', strtotime($time)) }}</strong>.
<br><br>
Regards, the PamperHub Team.