Hello PamperHub,
<br><br>
The appointment: {{ $orderNo }}, has been restored and is no longer canceled.
<br><br>
Regards, the PamperHub Team.