<?php

use Illuminate\Database\Seeder;
use App\Day;

class TimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      	$monday = new Day();
        $monday->name  = 'monday';
        $monday->time_ref = 1;
        $monday->save();

        $tuesday = new Day();
        $tuesday->name  = 'tuesday';
        $tuesday->time_ref = 2;
        $tuesday->save();

        $wednesday = new Day();
        $wednesday->name  = 'wednesday';
        $wednesday->time_ref = 3;
        $wednesday->save();

        $thursday = new Day();
        $thursday->name  = 'thursday';
        $thursday->time_ref = 4;
        $thursday->save();

        $friday = new Day();
        $friday->name  = 'friday';
        $friday->time_ref = 5;
        $friday->save();

        $saturday = new Day();
        $saturday->name  = 'saturday';
        $saturday->time_ref = 6;
        $saturday->save();

        $sunday = new Day();
        $sunday->name  = 'sunday';
        $sunday->time_ref = 0;
        $sunday->save();

//        $afternoons = new Time();
//        $afternoons->name  = 'afternoons';
//        $afternoons->save();

//        $mornings = new Time();
//        $mornings->name  = 'mornings';
//        $mornings->save();
//
//        $evenings = new Time();
//        $evenings->name  = 'evenings';
//        $evenings->save();
//
//        $weekends = new Time();
//        $weekends->name  = 'weekends';
//        $weekends->save();
//
//        $weekdays = new Time();
//        $weekdays->name  = 'weekdays';
//        $weekdays->save();

    }
}
