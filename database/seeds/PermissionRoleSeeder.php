<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Role Customer
      DB::table('permission_role')->insert([
        'permission_id' => DB::table('permissions')->where('name', '=', 'customer')->first()->id,
        'role_id' => DB::table('roles')->where('name', '=', 'customer')->first()->id
      ]);

      // Role Supplier
      DB::table('permission_role')->insert([
        'permission_id' => DB::table('permissions')->where('name', '=', 'supplier')->first()->id,
        'role_id' => DB::table('roles')->where('name', '=', 'supplier')->first()->id
      ]);

      // Role Customer Service Rep
      DB::table('permission_role')->insert([
        'permission_id' => DB::table('permissions')->where('name', '=', 'customer-service-rep')->first()->id,
        'role_id' => DB::table('roles')->where('name', '=', 'customer-service-rep')->first()->id
      ]);

      // Role Super Admin
      DB::table('permission_role')->insert([
        'permission_id' => DB::table('permissions')->where('name', '=', 'super-admin')->first()->id,
        'role_id' => DB::table('roles')->where('name', '=', 'super-admin')->first()->id
      ]);
    }
}
