<?php

use App\Customer;
use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new User();
        $admin->firstname = 'Tech';
        $admin->lastname = 'JPPdesigns';
        $admin->email = 'jpp@jppdesigns.co.uk';
        $admin->password = bcrypt('test123');
        $admin->save();

        $irene = new User();
        $irene->firstname = 'Admin';
        $irene->lastname = 'Irene';
        $irene->email = 'iomaswa@pamperhub.com';
        $irene->password = bcrypt('test123');
        $irene->save();

        $customer = new User();
        $customer->firstname = 'Customer';
        $customer->lastname = 'Customer';
        $customer->email = 'customer@test.com';
        $customer->password = bcrypt('test123');
        $customer->save();

        $customerEntry = new Customer();
        $customerEntry->user_id = $customer->id;
        $customerEntry->save();


        $supplier = new User();
        $supplier->firstname = 'Supplier';
        $supplier->lastname = 'Supplier';
        $supplier->email = 'supplier@test.com';
        $supplier->password = bcrypt('test123');
        $supplier->save();

    }
}
