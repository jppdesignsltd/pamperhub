<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Supplier;
use \App\Library\MapFunctions as MapFunctions;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // random uk postcodes
        $postcodes = array('NW11 9NJ', 'NW2 7NG', 'W6 8BN', 'SE15 6AY', 'N8 8HG', 'E1 5LA', 'N4 3AZ', 'N1 9AG', 'EC50 4NL', 'W7 1JX', 'NW3 7BB', 'SE17 1BL', 'SW17 8QE', 'N22 6UD', 'N12 7HQ', 'W5 1ZR', 'NW6 7LH', 'SE5 7BL', 'NW7 1NE', 'N20 9HH', 'NW2 6LR', 'SE20 8ZJ', 'SE19 3HS', 'SE15 5BE', 'SW7 2BB', 'SE16 7RJ', 'N15 5NT', 'N15 4JR', 'SE22 0YD', 'EC50 1TW', 'N21 1BG', 'E3 2TB', 'SW3 4BT', 'N3 3SZ', 'SE16 7ZR', 'N16 8TF', 'SE16 4TT', 'SW16 6NY', 'SW16 5YP', 'SE16 6ZG', 'E5 9DS', 'N4 3WS', 'N18 2EH', 'E17 9DD', 'W13 9ZS', 'SE16 4SA', 'N9 0TP', 'N16 8BP');

        $faker = Faker::create('en_UK');

        for($i = 1; $i < 10; $i++) {

            // user signs up first
            $surname = $faker->lastName;
            $id = DB::table('users')->insertGetId(
                [
                    'firstname' => $faker->firstName,
                    'lastname' => $surname,
                    'email' => $faker->email,
                    'password' => bcrypt('password'),
                    'active' => 1,
                    'remember_token' => '',
                    'created_at' => $faker->dateTime($max = 'now', $timezone = date_default_timezone_get()),
                    'updated_at' => $faker->dateTime($max = 'now', $timezone = date_default_timezone_get()),
                ]
            );

            //$fakePostCode = chr($faker->numberBetween($min = 65, $max = 90)) . $faker->randomFloat() > 0.5 ? "" : chr($faker->numberBetween($min = 65, $max = 90)) . $faker->numberBetween($min = 10, $max = 99) . ' ' . $faker->numberBetween($min = 1, $max = 9) . chr($faker->numberBetween($min = 65, $max = 90)) . chr($faker->numberBetween($min = 65, $max = 90));

            $fakePostCode = $faker->randomElement($postcodes);

            // get and store longitude and latitude
            $clientLongAndLat = MapFunctions::getLongitudeAndLatitude($fakePostCode);

            $streetAddress = $faker->streetAddress;
            $city = $faker->city;
            $postCode = $fakePostCode;

            // create details about the suppliers and their business
            $supplierId = DB::table('suppliers')->insertGetId(
                [
                    'dob' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-20 years')->format('Y-m-d'),
                    'contact_number' => $faker->phoneNumber,
                    'company' => $faker->company,
                    'website' => $faker->url,
                    'address1' => $streetAddress,
                    'address2' => null,
                    'city' => $city,
                    'postal_code' => $postCode,
                    'longitude' => $clientLongAndLat[0],
                    'latitude' => $clientLongAndLat[1],
                    'years_exp' => $faker->numberBetween($min = 1, $max = 20),
                    'opening_time' => $faker->numberBetween($min = 9, $max = 12) . ':00 AM',
                    'closing_time' => $faker->numberBetween($min = 3, $max = 7) . ':00 PM',
                    'work_eligibility' => $faker->numberBetween($min = 0, $max = 1),
                    'referee1_name' => $faker->name($gender = null),
                    'referee1_contact_number' => $faker->phoneNumber,
                    'referee1_email' => $faker->email,
                    'referee2_name' => $faker->name($gender = null),
                    'referee2_contact_number' => $faker->phoneNumber,
                    'referee2_email' => $faker->email,
                    'display_picture' => null,
                    'step' => 4,
                    'signup_complete' => 1,
                    'active' => 1,
                    'user_id' =>  $id,
                    'created_at' => $faker->dateTime($max = 'now', $timezone = date_default_timezone_get()),
                    'updated_at' => null ,
                ]
            );

            // list the times their open
            $totalServices = $faker->numberBetween($min = 1, $max = 4);
            for($j=1; $j<=$totalServices; $j++) {
                DB::table('day_supplier')->insert(
                    [
                        'supplier_id' => $supplierId,
                        'day_id' => $faker->numberBetween($min = 1, $max = 7),
                    ]
                );
            }

            // list the treatments they provide
             for($k=1; $k<=$totalServices; $k++) {
                 //Supplier_Treatment::create (
                 DB::table('supplier_treatment')->insert(
                     [
                         'supplier_id' => $supplierId,
                         'treatment_id' => $faker->numberBetween($min = 1, $max = 4),
                     ]
                 );
             }

            // list the services they provide
            $treatmentsArray = array('Hair', 'Beauty', 'Massage', 'Barber');

            for($j=1; $j<=$totalServices; $j++) {

                $serviceTitle = $treatmentsArray[$j-1].' '.$faker->catchPhrase;

                DB::table('services')->insert(
                   [
                        'title'=> $serviceTitle ,
                        'slug'=> str_replace(" ", "-", $serviceTitle) . $surname.'-'.$supplierId ,
                        'description'=> $faker->paragraph($nbSentences = 2, $variableNbSentences = true) ,
                        'service_type'=> $j ,
                        'duration' => $faker->numberBetween($min = 1, $max = 3),
                        'cover_image'=> 'about-history.jpg',
                        'price'=> $faker->randomFloat($nbMaxDecimals = 2, $min = 20, $max = 60) ,
                        'promotion'=> 0 ,
                        'promotion_price'=> null ,
                        'status'=> 'published' ,
                        'live_date'=> '2017-03-03 01:20:00' ,
                        'supplier_id' => $faker->numberBetween($min = 1, $max = 9),
                        'created_at'=> $faker->dateTime($max = 'now', $timezone = date_default_timezone_get()) ,
                        'updated_at'=> $faker->dateTime($max = 'now', $timezone = date_default_timezone_get()) ,
                   ]
                );
            }


        }
    }
}
