<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $customer = new Role();
      $customer->name         = 'customer';
      $customer->display_name = 'Standard User'; // optional
      $customer->description  = 'Standard user with guest privilidges'; // optional
      $customer->save();

      $supplier = new Role();
      $supplier->name         = 'supplier';
      $supplier->display_name = 'Supplier'; // optional
      $supplier->description  = 'User can create service postings'; // optional
      $supplier->save();

      $servicerep = new Role();
      $servicerep->name         = 'customer-service-rep';
      $servicerep->display_name = 'Customer Service Rep'; // optional
      $servicerep->description  = 'Helps with the day to day running of the site'; // optional
      $servicerep->save();

      $admin = new Role();
      $admin->name         = 'super-admin';
      $admin->display_name = 'Super Admin'; // optional
      $admin->description  = 'This user is all powerful!!!'; // optional
      $admin->save();
    }
}
