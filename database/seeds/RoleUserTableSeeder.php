<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RoleUserTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::table('role_user')->insert([
			'user_id' => DB::table('users')->where('firstname', '=', 'Tech')->first()->id,
			'role_id' => DB::table('roles')->where('name', '=', 'super-admin')->first()->id
		]);

		DB::table('role_user')->insert([
			'user_id' => DB::table('users')->where('firstname', '=', 'Admin')->first()->id,
			'role_id' => DB::table('roles')->where('name', '=', 'super-admin')->first()->id
		]);

		DB::table('role_user')->insert([
			'user_id' => DB::table('users')->where('firstname', '=', 'Customer')->first()->id,
			'role_id' => DB::table('roles')->where('name', '=', 'customer')->first()->id
		]);

		DB::table('role_user')->insert([
			'user_id' => DB::table('users')->where('firstname', '=', 'Supplier')->first()->id,
			'role_id' => DB::table('roles')->where('name', '=', 'supplier')->first()->id
		]);

	}
}
