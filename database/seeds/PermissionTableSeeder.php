<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        [
          'name' => 'customer',
          'display_name' => 'Customer',
          'description' => 'Can only view consumer side of site.'
        ],
        [
          'name' => 'supplier',
          'display_name' => 'Supplier',
          'description' => 'Supplier who lists services on the site.'
        ],
        [
          'name' => 'customer-service-rep',
          'display_name' => 'Customer Service Rep',
          'description' => 'Customer Service Rep'
        ],
        [
          'name' => 'super-admin',
          'display_name' => 'Super Admin',
          'description' => 'Total control of everything.'
        ]
      ];

      foreach ($permission as $key => $value) {
        Permission::create($value);
      }
    }
}
