<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		$setting = new Setting();
		$setting->site_name = "PamperHub";
		$setting->tagline = "Start something new";
		$setting->home_tagline = "Instantly bookable professional salon and spa experiences delivered to you";
		$setting->email_address = "info@pamperhub.com";
		$setting->breadcrumbs = "1";
		$setting->services_per_page = "10";
		$setting->services_excerpt = "200";
		$setting->posts_per_page = "10";
		$setting->posts_excerpt = "200";
		$setting->default_page_author = "Pamper Hub Ltd";
		$setting->default_keywords = "PamperHub, Pamper Hub, JPPdesigns Web Design and Development";
		$setting->default_description = "PamperHub Meta Description";

		$setting->save();

    }
}
