<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(UserTableSeeder::class);
		$this->call(RoleTableSeeder::class);
		$this->call(PermissionTableSeeder::class);
		$this->call(PermissionRoleSeeder::class);
		$this->call(TimeTableSeeder::class);
		$this->call(SettingsTableSeeder::class);
		$this->call(TreatmentTableSeeder::class);
		$this->call(RoleUserTableSeeder::class);
//        $this->call(SuppliersTableSeeder::class);
    }
}
