<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTreatmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
				Schema::create('supplier_treatment', function (Blueprint $table) {
						$table->increments('id');

						$table->integer('supplier_id')->unsigned();
						$table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');

						$table->integer('treatment_id')->unsigned();
						$table->foreign('treatment_id')->references('id')->on('treatments')->onDelete('cascade');
				});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_treatment');
    }
}
