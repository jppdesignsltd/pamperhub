<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// Create Settings table
		Schema::create('settings', function (Blueprint $table) {
			$table->increments('id');
			$table->text('site_name');
			$table->text('tagline');
			$table->boolean('breadcrumbs');
			$table->string('email_address');
			$table->integer('services_per_page');
			$table->integer('services_excerpt');
			$table->integer('posts_per_page');
			$table->integer('posts_excerpt');
			$table->string('default_page_author');
			$table->string('default_keywords');
			$table->string('default_description');
			$table->timestamps();
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
