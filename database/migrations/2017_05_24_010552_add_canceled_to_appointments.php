<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanceledToAppointments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
			$table->boolean('canceled')->default('0')->after('treatment_id');
			$table->boolean('refunded')->default('0')->after('canceled');
			$table->string('refund_ref')->nullable()->after('refunded');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
        	$table->dropColumn('canceled');
        	$table->dropColumn('refunded');
        	$table->dropColumn('refund_ref');
        });
    }
}
