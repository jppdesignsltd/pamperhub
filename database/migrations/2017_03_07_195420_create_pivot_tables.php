<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create table for associating tags to services (Many-to-Many)
		Schema::create('service_tag', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('service_id')->unsigned();
			$table->integer('tag_id')->unsigned();

			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
			$table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('service_tag');

	}
}
