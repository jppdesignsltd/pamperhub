<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create Supplier Table
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('dob');
            $table->string('contact_number');
            $table->string('company')->nullable();
            $table->string('website')->nullable();
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('city');
            $table->string('postal_code');
            $table->integer('years_exp')->nullable();
            $table->string('opening_time')->nullable();
            $table->string('closing_time')->nullable();
            $table->boolean('work_eligibility')->nullable();
            $table->string('referee1_name')->nullable();
            $table->string('referee1_contact_number')->nullable();
            $table->string('referee1_email')->nullable();
            $table->string('referee2_name')->nullable();
            $table->string('referee2_contact_number')->nullable();
            $table->string('referee2_email')->nullable();
            $table->float('latitude');
            $table->float('longitude');
            $table->string('display_picture')->nullable();
            $table->integer('step')->nullable();
            $table->boolean('signup_complete')->default('0');
            $table->boolean('active')->default('0');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
				Schema::dropIfExists('suppliers');
    }
}
