<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		// Create Appointments Table
		Schema::create('appointments', function (Blueprint $table) {
			$table->increments('id');
			$table->string('duration');
			$table->string('price');
			$table->date('date');
			$table->string('time_of_day')->nullable();
			$table->time('time')->nullable();
			$table->text('additional_information')->nullable();
			$table->boolean('payment_received');
			$table->string('paypal_ref');
			$table->boolean('supplier_confirmed')->default('0');
			$table->integer('service_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->integer('treatment_id')->unsigned();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('appointments');
    }
}
