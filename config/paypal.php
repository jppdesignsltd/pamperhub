<?php

return array(
	/** Set your paypal credential **/
	// Sandbox
	'client_id' => 'AcWgRmIZPgTxpw_o6qs5tsC3ApLXuYbo-Lm1tcA-RrFTtsCzcf_2Z7yUNwRNTkS4YIsOFoc7RNJTPKUF',
	'secret' => 'EM6JvEUrbbgoesBNi2t2sKC7vEloWOltcPSPasdxn_qFlDA-TlvSjelz6BP7bBB6eAyxFijemv3jBvuE',
	// Live
//	'client_id' => 'AaVc9cYdnGixziqUZulFNx1t05moMm-T1AVR8vgtvU3rx6XGVk9Qmdw7C3QCyYwJ3RKJ6GZUq-Kw1hGW',
//	'secret' => 'EJ6q3Nwl9leEbQV0LXO8x1-zGKBpfsLJqVCrKVkbuuupQYCoFdMM1f-1PI0hkT3FpWmYcIf0pvlo6VJM',

	/**
	 * SDK configuration
	 */
	'settings' => array(

		/**
		 * Available option 'sandbox' or 'live'
		 */
		'mode' => 'sandbox',

		/**
		 * Specify the max request time in seconds
		 */
		'http.ConnectionTimeOut' => 1000,

		/**
		 * Whether want to log to a file
		 */
		'log.LogEnabled' => true,

		/**
		 * Specify the file that want to write on
		 */
		'log.FileName' => storage_path() . '/logs/paypal.log',

		/**
		 * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
		 *
		 * Logging is most verbose in the 'FINE' level and decreases as you
		 * proceed towards ERROR
		 */
		'log.LogLevel' => 'FINE'

	),

);