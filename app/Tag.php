<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	protected $hidden = ['created_at', 'updated_at'];

	//	Tags belong to many services
    public function services() {
		return $this->belongsToMany('App\Service');
	}

//		Tags belong to many posts
//		public function posts() {
//				return $this->belongsToMany('App\Post');
//		}
}
