<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Suppliers That Belong to Treatments
     **/
    public function suppliers() {
        return $this->belongsToMany('App\Supplier');
    }
}
