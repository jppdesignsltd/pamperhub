<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $hidden = ['created_at', 'updated_at'];

    public function supplier() {
        return $this->belongsTo('App\Supplier');
    }

    public function appointments() {
        return $this->hasMany('App\Appointment');
    }

	/**
	 * Treatment That Belongs to Service
	 **/
	public function treatment() {
		return $this->belongsTo('App\Treatment', 'service_type');
	}

	/**
	 * Tags That Belong to post
	 **/
	public function tags() {
		return $this->belongsToMany('App\Tag');
	}
}
