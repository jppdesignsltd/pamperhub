<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
	protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
		return $this->belongsTo('App\User');
    }

    public function treatments() {
        return $this->belongsToMany('App\Treatment');
    }

    public function days() {
        return $this->belongsToMany('App\Day');
    }

    public function services() {
        return $this->hasMany('App\Service');
    }

    public function appointments() {
        return $this->hasMany('App\Appointment');
    }

	/**
	 * Holidays That Belong to Supplier
	 **/
	public function holidays() {
		return $this->hasMany('App\Holiday');
	}
}
