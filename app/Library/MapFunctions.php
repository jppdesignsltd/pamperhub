<?php

namespace App\Library {

    class MapFunctions
    {

        /**
         * get the latitude and longitude of an address
         * @param  string $address an address
         * @return array          latitude and longitude
         */
        public static function getLongitudeAndLatitude($address)
        {
//			dd($address);

            $address = str_replace(" ", "+", $address);

            $json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=AIzaSyA0KiyUCksu7LHaf9gA4aXB16UtyZJC0hU&sensor=true");
            $json = json_decode($json);

            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};

            return array((int)$long, (int)$lat);

//			//			Offline only
//			return [-0.08, 51.46];
        }

        /**
         * calculate the distance between to addresses using their longitude and latitude
         * @param  array $address1 first address
         * @param  array $address2 second address
         * @return float           distance in meters
         */
        public static function getDistanceBetweenPlaces($address1, $address2)
        {
            $p1["lng"] = (float)$address1[0];
            $p1["lat"] = (float)$address1[1];

            $p2["lng"] = (float)$address2[0];
            $p2["lat"] = (float)$address2[1];

            $R = 6378137; // Earth’s mean radius in meter
            $dLat = deg2rad((int)$p2["lat"] - (int)$p1["lat"]);
            $dLong = deg2rad((int)$p2["lng"] - (int)$p1["lng"]);
            $a = sin((int)$dLat / 2) * sin((int)$dLat / 2) + cos(deg2rad((int)$p1["lat"])) * cos(deg2rad($p2["lat"])) * sin((int)$dLong / 2) * sin((int)$dLong / 2);
            $c = 2 * atan2(sqrt((int)$a), sqrt(1 - (int)$a));
            $d = $R * $c;
            return $d * 0.000621371; // returns the distance in meter
        }

        /**
         * get the radians of a number
         * @param float $x radians
         */
        private static function Radians($x)
        {
            //$PIx = 3.141592653589793;
            //return $x * $PIx / 180;
            return deg2rad($x);
        }
    }

}