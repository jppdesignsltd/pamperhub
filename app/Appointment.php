<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
	protected $hidden = [
		'created_at', 'updated_at',
	];

    public function supplier() {
        return $this->belongsTo('App\Supplier');
    }

    public function customer() {
        return $this->belongsTo('App\Customer');
    }

	public function treatment() {
		return $this->belongsTo('App\Treatment');
	}

    public function service() {
        return $this->belongsTo('App\Service');
    }
}
