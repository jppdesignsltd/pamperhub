<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{

	//	Holidays belong to many suppliers
	public function suppliers() {
		return $this->belongsTo('App\Supplier');
	}

}
