<?php

namespace App\Http\Controllers;

//use Config;
use App\Appointment;
use App\Customer;
use App\Holiday;
use App\Service;
use App\Setting;
use App\Supplier;
use App\Treatment;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Session;
use Validator;
use URL;


/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PayPalController extends Controller
{

	private $_api_context;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role:customer|super-admin');

//		parent::__construct();

		/** setup PayPal api context **/
		$paypal_conf = \Config::get('paypal');

		$this->_api_context = new ApiContext(new OAuthTokenCredential(
			$paypal_conf['client_id'],
			$paypal_conf['secret']
		));

		$this->_api_context->setConfig($paypal_conf['settings']);
	}

	/**
	 * Show the application paywith paypalpage.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getPayWithPaypal(Request $request)
	{
//        $orderClean = substr($order, strpos($order, "-") + 1);
//        $orderID = str_replace('0', '', $orderClean);

		$user = Auth::user();
		$customer = Customer::where('user_id', $user->id)->first();
		$treatments = Treatment::all();
		$treatment = [];
		$status = $request->get('s');
		$order = $request->get('o');
		$orderID = substr($order, strpos($order, "-") + 2);
		$appointment = Appointment::where('id', $orderID)->where('customer_id', $customer->id)->first();

//		dd($orderID);

		foreach ($treatments as $treatmentName) {
			$treatment[$treatmentName->id] = $treatmentName->name;
		}

		if ($appointment) {

			return view('booking.status')->with(compact('appointment', 'customer', 'status'));

		} else {

			$status = 'no-order';

			return view('booking.status')->with(compact('status'));

		}

	}

	/**
	 * Store a details of payment with paypal.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function postPayWithPaypal(Request $request)
	{
//		dd($request);

		$this->validate($request, [
			'service_id' => 'required',
			'duration' => 'required',
			'date' => 'sometimes',
			'time_of_day' => 'required',
			'additional_information' => 'sometimes',
			'total_amount' => 'required'
		]);

		Session::put('service_id', $request->service_id);
		Session::put('duration', $request->duration);
		Session::put('time_of_day', $request->time_of_day);
		Session::put('price', $request->total_amount);

		$user = Auth::user();
		$customer = Customer::where('user_id', $user->id)->first();
		$service = Service::where('id', $request->service_id)->first();

//		Check the date added and see if it is available.
		if (isset($request->date)) {
			Session::put('date', $request->date);

			$dpValue = $request->date;
			$date = DateTime::createFromFormat('d/m/Y', $dpValue);
			$dateForTable = $date->format('Y-m-d');
			$supplier = Supplier::where('id', $service->supplier_id)->first();
			$holidays = Holiday::where('supplier_id', $supplier->id)->get();

			$appointments = Appointment::where('date', $dateForTable)
				->where('payment_received', 1)
				->where('canceled', 0)
				->where('supplier_id', $service->supplier->id)
				->get();


			if ($appointments->count() > 0) {

				$tod = [];

				foreach ($appointments as $appointment) {
					$tod[$appointment->id] = $appointment->time_of_day;
				}

				if (count($tod) >= 3) {
					$lockdate = null;

					$tod = [];

					Session::flash('error', 'Please choose a different date as there is no availability for this date. ' . $dpValue);

					return view('booking.create', ['s', $service->id])->with(compact('tod', 'service', 'customer', 'lockdate', 'dateForTable', 'date', 'dpValue', 'holidays'));
				}

				$lockdate = true;

				Session::flash('error', 'That time of day is not available');

				return view('booking.create', ['s', $service->id])->with(compact('tod', 'service', 'customer', 'lockdate', 'dpValue'));
			}
		}

		if (isset($request->additional_information)) {
			Session::put('additional_information', $request->additional_information);
		}

		$payer = new Payer();
		$payer->setPaymentMethod('paypal');

		$item_1 = new Item();
		$item_1->setName($request->service_title . '_' . $request->service_id)/** item name **/
		->setCurrency('GBP')
			->setQuantity(1)
			->setPrice($request->get('total_amount'));
		/** unit price **/

		$item_list = new ItemList();
		$item_list->setItems(array($item_1));

		$amount = new Amount();
		$amount->setCurrency('GBP')
			->setTotal($request->get('total_amount'));

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($item_list)
			->setDescription('Your transaction description');

		$redirect_urls = new RedirectUrls();
		$redirect_urls->setReturnUrl(URL::route('payment.status'))/** Specify return URL **/
		->setCancelUrl(URL::route('payment.status'));
//                      ->setCancelUrl(URL::previous());

		$payment = new Payment();
		$payment->setIntent('Sale')
			->setPayer($payer)
			->setRedirectUrls($redirect_urls)
			->setTransactions(array($transaction));
		/** dd($payment->create($this->_api_context));exit; **/

		try {

			$payment->create($this->_api_context);

		} catch (\PayPal\Exception\PPConnectionException $ex) {

			if (\Config::get('app.debug')) {

				\Session::put('error', 'Connection timeout');

				return Redirect::route('booking.status');
				/** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
				/** $err_data = json_decode($ex->getData(), true); **/
				/** exit; **/

			} else {

				\Session::put('error', 'Some error occur, sorry for inconvenient');

				return Redirect::route('booking.status');
				/** die('Some error occur, sorry for inconvenient'); **/

			}

		}

		foreach ($payment->getLinks() as $link) {

			if ($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}

		}

		/** add payment ID to session **/
		Session::put('paypal_payment_id', $payment->getId());

		if (isset($redirect_url)) {

			/** redirect to paypal **/
			return Redirect::away($redirect_url);

		}

		\Session::put('error', 'Unknown error occurred');

		return Redirect::route('booking.status');

	}

	public function getPaymentStatus()
	{
		/** Get the payment ID before session clear **/
		$payment_id = Session::get('paypal_payment_id');

		/** clear the session payment ID **/
		Session::forget('paypal_payment_id');

//		dd($payment_id);

		if ($payment_id === null) {
			return Redirect::route('/');
		}

//		//
//		Canceled Payment Route
//		//
		if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
//			\Session::put('error', 'Payment Canceled');

			$user = Auth::user();
			$service = Service::where('id', Session::get('service_id'))->first();
			$duration = Session::get('duration');
//            $price = $service->price;
//            $total_price = $price*$duration;
			$total_price = Session::get('price');
			$customer = Customer::where('user_id', $user->id)->first();
			$supplier = Supplier::where('id', $service->supplier_id)->first();
			$dpValue = Session::get('date');
			$time = Session::get('time_of_day');
			$additional = Session::get('additional_information');

			// Parse a date using a user-defined format
			$date = DateTime::createFromFormat('d/m/Y', $dpValue);

			// Using the parsed date we can create a
			// new one with a formatting of our choosing
			$dateForTable = $date->format('Y-m-d');

			$service->live_date = $dateForTable;

//			dd($supplier, $customer, $service, $total_price, $date, $time);

			$booking = new Appointment();
			$booking->duration = $duration;
			$booking->price = $total_price;
			$booking->date = $dateForTable;
			$booking->time_of_day = $time;
			$booking->service_id = $service->id;
			$booking->customer_id = $customer->id;
			$booking->supplier_id = $supplier->id;
			$booking->treatment_id = $service->service_type;
			$booking->payment_received = 0;

			if ($payment_id === null) {
				$booking->paypal_ref = 'failed';
			} else {
				$booking->paypal_ref = $payment_id;
			}

			if ($additional !== null || $additional !== '') {
				$booking->additional_information = $additional;
			}

			$booking->save();

			$settings = Setting::first();

			$data = [
				'from' => 'support@pamperhub.com',
				'email' => $user->email,
				'firstname' => $user->firstname,
				'lastname' => $user->lastname,
				'orderNo' => 'ORD-0' . $booking->id,
				'subject' => 'Payment Unsuccessful: ORD-0' . $booking->id
			];

			Mail::send('emails.appointment-payment-not-received', $data, function($message) use ($data, $settings){
				$message->to($data['email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			return Redirect::route('booking.status', ['o' => 'ORD-0' . $booking->id]);
//			return Redirect::route('booking.status', ['s' => 'canceled', 'o' => '#ORD-0' . $booking->id]);
		}

		$payment = Payment::get($payment_id, $this->_api_context);

		/** PaymentExecution object includes information necessary **/
		/** to execute a PayPal account payment. **/
		/** The payer_id is added to the request query parameters **/
		/** when the user is redirected from paypal back to your site **/
		$execution = new PaymentExecution();
		$execution->setPayerId(Input::get('PayerID'));

		/**Execute the payment **/
		$result = $payment->execute($execution, $this->_api_context);

		/** dd($result);exit; /** DEBUG RESULT, remove it later **/
		if ($result->getState() == 'approved') {

			/** Here Write your database logic like that insert record or value in database if you want **/
			$user = Auth::user();
			$settings = Setting::first();
			$service = Service::where('id', Session::get('service_id'))->first();
			$duration = Session::get('duration');
//			$price = $service->price;
//			$total_price = $price*$duration;
			$total_price = Session::get('price');
			$customer = Customer::where('user_id', $user->id)->first();
			$supplier = Supplier::where('id', $service->supplier_id)->first();
			$dpValue = Session::get('date');
			$time = Session::get('time_of_day');
			$additional = Session::get('additional_information');

			// Parse a date using a user-defined format
			$date = DateTime::createFromFormat('d/m/Y', $dpValue);

			// Using the parsed date we can create a
			// new one with a formatting of our choosing
			$dateForTable = $date->format('Y-m-d');

			$service->live_date = $dateForTable;

//			dd($supplier, $customer, $service, $total_price, $date, $time);

			$booking = new Appointment();
			$booking->duration = $duration;
			$booking->price = $total_price;
			$booking->date = $dateForTable;
			$booking->time_of_day = $time;
			$booking->service_id = $service->id;
			$booking->customer_id = $customer->id;
			$booking->supplier_id = $supplier->id;
			$booking->treatment_id = $service->service_type;
			$booking->payment_received = 1;

			if ($payment_id) {
				$booking->paypal_ref = $payment_id;
			}

			if ($additional !== null || $additional !== '') {
				$booking->additional_information = $additional;
			}

			$booking->save();

			$data = [
				'from' => 'no-reply@pamperhub.com',
				'email' => $user->email,
				'firstname' => $user->firstname,
				'lastname' => $user->lastname,
				'orderNo' => 'ORD-0' . $booking->id,
				'price' => $booking->price,
				'treatmentName' => $booking->treatment->name,
				'date' => $booking->date,
				'time_of_day' => $booking->time_of_day,
				'subject' => 'Payment Successful: ORD-0' . $booking->id
			];

			Mail::send('emails.appointment-payment-received', $data, function($message) use ($data, $settings){
				$message->to($data['email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

//			\Session::put('success', 'Payment success');

//			return Redirect::route('booking.status', ['s' => 'success', 'o' => '#ORD-0' . $booking->id]);
			return Redirect::route('booking.status', ['o' => 'ORD-0' . $booking->id]);
		}

		\Session::put('error', 'Payment failed');

		return Redirect::route('booking.status', ['s' => 'error']);
	}
}
