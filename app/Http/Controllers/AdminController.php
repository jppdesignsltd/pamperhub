<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Treatment;
use App\Supplier;
use App\Customer;
use App\User;
use App\Service;
use App\Day;

class AdminController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role:super-admin|customer-service-rep');
	}

	public function getIndex() {
		$activeSuppliers = Supplier::where('active', '1');
		$inActiveSuppliers = Supplier::where('active', '0');
		$suppliersPaginate = Supplier::orderBy('id', 'desc')->paginate(5);
		$customersPaginate = Customer::orderBy('id', 'desc')->paginate(5);
		$servicesPaginate = Service::orderBy('id', 'desc')->paginate(5);
		$treatments = Treatment::all();

//		dd($inActiveSuppliers);

		return view('admin.index')->with(compact('activeSuppliers', 'inActiveSuppliers','suppliersPaginate', 'customersPaginate', 'servicesPaginate', 'treatments'));

	}

	public function getSuppliers() {
			$suppliersPaginate = Supplier::orderBy('id', 'desc')->paginate(10);

			return view('admin.suppliers')->with(compact('suppliersPaginate'));

	}

	public function getSupplierEdit($id) {
		$supplier = Supplier::find($id);
		$treatments = Treatment::all();
		$days = Day::all();

		$treatment = [];
		$day = [];

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        foreach ($days as $dayName) {
            $day[$dayName->id] = $dayName->name;
        }

		return view('admin.supplier.edit')->with(compact('supplier', 'treatment', 'day'));

	}

    public function getSupplierShow($id) {
        $supplier = Supplier::find($id);
        $treatments = Treatment::all();
        $days = Day::all();

        $treatment = [];
        $day = [];

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        foreach ($days as $dayName) {
            $day[$dayName->id] = $dayName->name;
        }

        return view('admin.supplier.show')->with(compact('supplier', 'treatment', 'day'));

    }

	public function getCustomers() {
			$customersPaginate = Customer::orderBy('id', 'desc')->paginate(10);

			return view('admin.customers')->with(compact('customersPaginate'));

	}

    public function getCustomerEdit($id) {
        $customer = Customer::find($id);

        return view('admin.customer.edit')->with(compact('customer'));

    }

	public function getCustomerShow($id) {
		$customer = Customer::find($id);

		return view('admin.customer.show')->with(compact('customer'));

	}

}
