<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\Setting;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Role;
use Hash;
use Entrust;
use Auth;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'method' => 'sometimes'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {

		$settings = Setting::first();

        if ($data['user_type'] === 'super-admin') {

            $role = Role::where('name', 'super-admin')->get()->first();

        } elseif ($data['user_type'] === 'customer-service-rep') {

            $role = Role::where('name', 'customer-service-rep')->get()->first();

        } elseif ($data['user_type'] === 'supplier') {

            $role = Role::where('name', 'supplier')->get()->first();

        } else {

            $role = Role::where('name', 'customer')->get()->first();

        }

        $user = new User();

        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);

        $user->save();

        $user->roles()->attach($role);


        if ($role->name === 'supplier') {

			$data = [
				'from' => 'no-reply@pamperhub.com',
				'email' => $user->email,
				'firstname' => $user->firstname,
				'lastname' => $user->lastname,
				'subject' => 'Welcome ' . $user->firstname . ' now complete your registration!',
				'admin_email' => 'admin@pamperhub.com',
				'admin_subject' => 'New Supplier. Registration not complete ' . $user->email,
			];

			Mail::send('emails.supplier-signup', $data, function($message) use ($data, $settings){
				$message->to($data['email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			Mail::send('emails.supplier-signup-not-complete', $data, function($message) use ($data, $settings){
				$message->to($data['admin_email']);
				$message->subject($data['admin_subject']);
				$message->from($data['from'], $settings->site_name);
			});

            $this->redirectTo = 'supplier/register';

        } elseif (isset($data['method'])) {

			if ($data['method'] === 'modal') {

				$customer = new Customer();

				$customer->user_id = $user->id;
				$customer->active = 1;

				$customer->save();

				$data = [
					'from' => 'no-reply@pamperhub.com',
					'email' => $user->email,
					'firstname' => $user->firstname,
					'lastname' => $user->lastname,
					'subject' => 'Welcome to Pamperhub'
				];

				Mail::send('emails.customer-signup', $data, function($message) use ($data, $settings){
					$message->to($data['email']);
					$message->subject($data['subject']);
					$message->from($data['from'], $settings->site_name);
				});

				$this->redirectTo = url()->previous();

			}

		} elseif ($role->name === 'customer') {

			$customer = new Customer();

			$customer->user_id = $user->id;
			$customer->active = 1;

			$customer->save();

			$data = [
				'from' => 'no-reply@pamperhub.com',
				'email' => $user->email,
				'firstname' => $user->firstname,
				'lastname' => $user->lastname,
				'subject' => 'Welcome to Pamperhub'
			];

			Mail::send('emails.customer-signup', $data, function($message) use ($data, $settings){
				$message->to($data['email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			$this->redirectTo = 'customer';

        } else {

            $this->redirectTo = '/';

        }

        return $user;
    }
}
