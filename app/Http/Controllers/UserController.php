<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Session;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role:super-admin|customer-service-rep');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$usersPaginate = User::orderBy('id', 'desc')->paginate(10);

        return view('admin.users')->with(compact('usersPaginate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('admin.user.create');
    }

    public function store(Request $request) {
//    	dd($request);

		$this->validate($request, [
			'firstname' => 'required|max:255',
			'lastname' => 'required|max:255',
			'email' => "required|email|max:255|unique:users,email",
			'password' => 'sometimes|min:6|confirmed',
		]);

    	$user = new User();
		$role = Role::where('name', $request->user_type)->get()->first();

		$user->firstname = $request->firstname;
		$user->lastname = $request->lastname;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);

		$user->save();

		$user->roles()->attach($role);

		Session::flash('success', 'User has been created!');

		return redirect()->route('admin.users');


	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$this->middleware('role:super-admin');

		$username = User::find($id);

		return view('admin.user.edit')->with(compact('username'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$user = User::find($id);

		$this->validate($request, [
			'firstname' => 'required|max:255',
			'lastname' => 'required|max:255',
			'email' => "required|email|max:255|unique:users,email,$id",
			'password' => 'sometimes|min:6',
		]);

		$user->firstname = $request->firstname;
		$user->lastname = $request->lastname;
		$user->email = $request->email;

		if (isset($request->password)) {
			$user->password = bcrypt($request->password);
		}

		$user->save();

		$user->roles()->attach($request->role);

		Session::flash('success', 'User has been updated!');

		return redirect()->route('admin.users');

    }
}
