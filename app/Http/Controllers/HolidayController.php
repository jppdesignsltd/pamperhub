<?php

namespace App\Http\Controllers;

use App\Holiday;
use App\Supplier;
use Illuminate\Http\Request;
use Auth;
use Session;
use DateTime;

class HolidayController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
		$supplier = Supplier::where('user_id', $user->id)->first();
		$holiday = new Holiday();

		$dpValue1 = $request->holiday_date1;
		$date1 = DateTime::createFromFormat('d/m/Y', $dpValue1);
		$dateForTable1 = $date1->format('Y-m-d');

		$dpValue2 = $request->holiday_date2;
		$date2 = DateTime::createFromFormat('d/m/Y', $dpValue2);
		$dateForTable2 = $date2->format('Y-m-d');

		$holiday->date_from = $dateForTable1;
		$holiday->date_to = $dateForTable2;

		$holiday->reason = $request->holiday_reason;
		$holiday->supplier_id = $supplier->id;

		$holiday->save();

		// Redirect to Page
		Session::flash('success', 'Thanks thats been added!');


		return redirect()->route('supplier.holiday');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$holiday = Holiday::find($id);

		$holiday->delete();

		Session::flash('success', 'Holiday removed');

		return redirect()->route('supplier.holiday');
	}

}
