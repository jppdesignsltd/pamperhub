<?php

namespace App\Http\Controllers;

use App\Service;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Treatment;
use App\Day;
use App\Supplier;
use DateTime;
use Session;
use \App\Library\MapFunctions as MapFunctions;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{

    public function getIndex()
    {
        $treatments = Treatment::all();
        $treatment = [];

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        return view('pages.home')->with(compact('treatment'));
    }

    public function getAbout()
    {
        return view('pages.about');
    }

    public function getTerms()
    {
        return view('pages.terms');
    }

    public function getPrivacy()
    {
        return view('pages.privacy');
    }

    public function getCookie()
    {
        return view('pages.cookie');
    }

    public function getSearch(Request $request)
    {
//		$datetime = date('Y-m-d H:i:s');
        $today = Carbon::today();
        $datetime = Carbon::parse($today)->format('Y-m-d 23:59:59');
        $user = Auth::user();
        $search = \Request::get('search');
//		$location = \Request::get('location');
        $serviceSearch = \Request::get('service');
//		$durationSearch = \Request::get('duration');
        $distanceSearch = \Request::get('distance');
//		$time = \Request::get('time');
//		$date = \Request::get('date');
        $settings = Setting::first();
        $service_number = $settings->services_per_page;
        $treatments = Treatment::all();
        $treatment = [];
//		$duration = ['30', '60', '120'];
        $active = function ($query) {
            $query->where('active', 1);
        };

        if ($distanceSearch === null || $distanceSearch === '') {
            $distanceSearch = 5;
        }

//		dd($datetime);

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        if ($request->get('search') && $request->get('service')) {

            $servicesArray = Service::where('service_type', $serviceSearch)
                ->where('status', 'published')
                ->where('live_date', '<', $datetime)
                ->orderBy('id', 'desc')
                ->whereHas('supplier', $active)
                ->paginate($service_number);

//			dd($servicesArray);

        } else {

            Session::flash('error', 'Please enter your postcode');

            return view('pages.home')->with(compact('treatment'));
        }

        // get the longitude and Latitude of search address
        $clientLongAndLat = MapFunctions::getLongitudeAndLatitude($search);

        // loop through the services list
        foreach ($servicesArray as $service) {

            // check that the service has both longitude and latitude values
            if (!empty($service->supplier->latitude) && !empty($service->supplier->longitude)) {
                // get the distance between the client and the supplier of the service
                $distance = MapFunctions::getDistanceBetweenPlaces($clientLongAndLat, array($service->supplier->longitude, $service->supplier->latitude));

                // add distance to service
                $service->distance = $distance;
            }
        }

        if ($distanceSearch === 'unlimited') {

            $servicesArray = $servicesArray
                ->sortBy('distance');

            return view('pages.search')->with(compact('search', 'servicesArray', 'treatment', 'serviceSearch', 'location', 'distance', 'distanceSearch'));

        } elseif ($distanceSearch === null || $distanceSearch === '') {

            $servicesArray = $servicesArray
                ->sortBy('distance');

            return view('pages.search')->with(compact('search', 'servicesArray', 'treatment', 'serviceSearch', 'location', 'distance', 'distanceSearch'));

        }

        // sort services by distance
        $servicesArray = $servicesArray
            ->where('distance', '<=', $distanceSearch)
            ->sortBy('distance');

        return view('pages.search')->with(compact('search', 'servicesArray', 'treatment', 'serviceSearch', 'location', 'distance', 'distanceSearch'));

    }

    public function getHelp()
    {
        return view('pages.frequently-asked-questions');
    }


    public function getListYourBusiness()
    {
        return view('pages.list-your-business');
    }

    public function getSupplierRegistration()
    {

        $step = \Request::get('step');
        $user = Auth::user();
        $treatments = Treatment::all();
        $days = Day::all();

        $treatment = [];
        $day = [];

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        foreach ($days as $dayName) {
            $day[$dayName->id] = $dayName->name;
        }

        if ($user) {

            $supplier = Supplier::where('user_id', $user->id)->first();

            if ($supplier !== null) {
                $step = $supplier->step;

                return view('supplier.register')->with(compact('user', 'step', 'supplier', 'treatment', 'day'));
            }

            return view('supplier.register')->with(compact('user', 'step', 'supplier', 'treatment', 'day'));

        }

        return view('supplier.register')->with(compact('user', 'step'));

    }
}
