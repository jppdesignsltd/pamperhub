<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Supplier;
use App\Treatment;
use Illuminate\Http\Request;
use App\Service;
use Purifier;
use Image;
use Auth;
use Storage;
use File;
use DateTime;
use Session;
use App\User;

class ServiceController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicesPaginate = Service::orderBy('id', 'desc')->paginate(10);

        return view('service.index')->with(compact('servicesPaginate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();

        // Validate the Data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'slug' => 'required|alpha_dash|min:1|max:255|unique:services,slug',
            'status' => 'required',
            'price' => 'required|between:0,99.99',
            'description' => 'sometimes',
            'service_type' => 'required',
            'live_date' => 'required',
            'cover_image' => 'sometimes|mimes:jpeg,jpg,bmp,png,gif|max:5000',
            'promotion' => 'required|boolean',
            'promotion_price' => 'sometimes|required_with:promotion|less_than_field:price',
        ));

        $service = new Service;

        $service->title = $request->title;
        $service->slug = $request->slug;
        $service->price = $request->price;
        $service->service_type = $request->service_type;
        $service->description = Purifier::clean($request->description);
        $service->status = $request->status;
        $service->promotion = $request->promotion;
        $service->promotion_price = $request->promotion_price;

        if ($request->live_date == '') {
            $service->live_date = date('Y-m-d H:i:s');
        } else {
            // The value from the datepicker
            $dpValue = $request->live_date;

            $date = DateTime::createFromFormat('d/m/Y H:i', $dpValue);

            // Using the parsed date we can create a
            // new one with a formatting of our choosing
            $dateForTable = $date->format('Y-m-d H:i:s');

            $service->live_date = $dateForTable;
        }

        // Save image for the service
        if ($request->hasFile('cover_image')) {

            $image = $request->file('cover_image');
            $filename = strtolower($request->service_type) . '_' . strtolower($supplier->id) . '_image.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/services/' . $filename);

            Image::make($image)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($location);

            $service->cover_image = $filename;
        }

        // Attribute to supplier who is creating the service
        $service->supplier_id = $supplier->id;

        $service->save();

        if (isset($request->tags)) {
            $service->tags()->sync($request->tags, false);
        } else {
            $service->tags()->sync([]);
        }

//        $service->suppliers()->attach($supplier);

        // Redirect to Page
        // Create Flash Session Message
        Session::flash('success', 'The service listing was successfully saved!');
        return redirect()->route('supplier.services');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        $treatments = Treatment::all();

        return view('service.show')->with(compact('service', 'treatments'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        $treatments = Treatment::all();

        return view('service.edit')->with(compact('service', 'treatments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $service = Service::find($id);
        $supplier = Supplier::where('user_id', $service->supplier->id)->first();
        $admin = $request->get('page');


        // Validate the Data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'slug' => "required|alpha_dash|min:1|max:255|unique:services,slug,$id",
            'status' => 'required',
            'price' => 'required|between:0,99.99',
            'description' => 'sometimes',
            'service_type' => 'sometimes',
            'live_date' => 'sometimes',
            'cover_image' => 'sometimes|mimes:jpeg,jpg,bmp,png,gif|max:5000',
            'promotion' => 'required|boolean',
            'promotion_price' => 'sometimes|required_with:promotion|less_than_field:price',
        ));

        $service->title = $request->title;
        $service->slug = $request->slug;
        $service->price = $request->price;
        $service->status = $request->status;

        if (isset($request->service_type)) {
            $service->service_type = $request->service_type;
        }

        $service->description = Purifier::clean($request->description);
        $service->status = $request->status;
        $service->promotion = $request->promotion;

        if ($request->promotion === 0) {
            $service->promotion_price = '';
        } else {
            $service->promotion_price = $request->promotion_price;
        }


        if ($service->live_date == '') {
            $service->live_date = date('Y-m-d H:i:s');
        }

        if (isset($request->live_date)){
            // The value from the datepicker
            $dpValue = $request->live_date;

//            dd($dpValue);

            // Parse a date using a user-defined format
            $date = DateTime::createFromFormat('d/m/Y H:i', $dpValue);

//			dd($date);

            // Using the parsed date we can create a
            // new one with a formatting of our choosing
            $dateForTable = $date->format('Y-m-d H:i:s');

            $service->live_date = $dateForTable;
        }

        // Save image for the service
        if ($request->hasFile('cover_image')) {

//            if ($admin === 'admin') {
//                $supplier->id = $service->supplier->id;
//
//                dd($supplier->id);
//            }

            $image = $request->file('cover_image');
            $filename = strtolower($service->service_type) . '_' . strtolower($supplier->id) . '_image.' . $image->getClientOriginalExtension();
            $location = public_path('uploads/services/' . $filename);

            Image::make($image)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($location);

            // Grab old file
            $oldFilename = $service->cover_image;

            // Update the Database
            $service->cover_image = $filename;

            if ($oldFilename !== $filename) {
                // Delete old Photo
                File::delete('uploads/services/' . $oldFilename);
            }

        }

        if ($request->remove_image) {

            $oldFilename = $service->cover_image;

            $service->cover_image = null;

            // Delete old Photo
            File::delete('uploads/services/' . $oldFilename);
        }

        $service->save();

        if (isset($request->tags)) {
            $service->tags()->sync($request->tags, false);
        } else {
            $service->tags()->sync([]);
        }

        // Redirect to Page
        // Create Flash Session Message
        Session::flash('success', 'The service listing was successfully saved!');

        if ($admin === 'admin') {
            return redirect()->route('service.show', $id);
        }

        return redirect()->route('supplier.services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);

        $service->tags()->detach();

        // Grab old file
        $oldFilename = $service->cover_image;

        // Delete old Photo
        File::delete('uploads/services/' . $oldFilename);

        $service->delete();

        Session::flash('success', 'The service was successfully deleted.');

        return redirect()->route('supplier.services');
    }
}
