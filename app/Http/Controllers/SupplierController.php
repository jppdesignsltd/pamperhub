<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Holiday;
use App\Service;
use App\Setting;
use App\Tag;
use App\Day;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Auth;
use App\Supplier;
use App\User;
use App\Treatment;
use Illuminate\Support\Facades\Mail;
use Image;
use File;
use Session;
use App\Library\MapFunctions as MapFunctions;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:supplier|super-admin|customer-service-rep');
    }

    public function index(Request $request)
    {

        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();
        $step = \Request::get('step');
        $treatments = Treatment::all();
        $times = Day::all();

        $treatment = [];

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        $time = [];

        foreach ($times as $timeName) {
            $time[$timeName->id] = $timeName->name;
        }

        if ($supplier === null) {

            return view('supplier.register')->with(compact('user', 'step', 'supplier', 'treatment', 'time'));

        }

        if ($supplier->signup_complete === 0) {
            $step = $supplier->step;

            return view('supplier.register')->with(compact('user', 'step', 'supplier', 'treatment', 'time'));

        }

        return view('supplier.index')->with(compact('user', 'supplier', 'treatment'));

    }

    public function getAvailability()
    {

        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();
        $step = null;

        $treatments = Treatment::all();
        $days = Day::all();

        $treatment = [];
        $day = [];

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        foreach ($days as $dayName) {
            $day[$dayName->id] = $dayName->name;
        }

        if ($supplier === null) {
            return view('supplier.register')->with(compact('user', 'step', 'supplier'));
        }

        return view('supplier.availability')->with(compact('user', 'supplier', 'day', 'treatment'));

    }

	public function getHoliday()
	{

		$user = Auth::user();
		$supplier = Supplier::where('user_id', $user->id)->first();
		$holidays = Holiday::where('supplier_id', $supplier->id)
			->orderBy('date_from')
			->paginate(10);


		return view('supplier.holiday')->with(compact('user', 'supplier', 'holidays'));

	}

    public function getAppointments(Request $request)
    {

		$today = Carbon::today();

//		dd($today);

		$page = $request->get('page');
        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();
        $next_appointment = Appointment::where('supplier_id', $supplier->id)
						->orderBy('date')
						->where('date', '>=', $today)
						->where('canceled', 0)
						->first();
        $appointments = Appointment::where('supplier_id', $supplier->id)
                        ->orderBy('date')
						->where('date', '>=', $today)
						->where('payment_received', '1')
//						->where('canceled', 0)
                        ->paginate(3);

        if ($supplier === null) {
            return view('supplier.register')->with(compact('user', 'step', 'supplier'));
        }

//        dd($appointments);

        return view('supplier.appointment.index')->with(compact('user', 'supplier','appointments','next_appointment', 'page'));

    }

	public function getShowAppointment($id)
	{
		$user = Auth::user();
		$appointment = Appointment::find($id);
		$supplier = Supplier::where('user_id', $user->id)->first();


		return view('supplier.appointment.show')->with(compact('appointment', 'supplier'));
	}

	public function getEditAppointment($id)
	{
		$user = Auth::user();
		$appointment = Appointment::find($id);
		$supplier = Supplier::where('user_id', $user->id)->first();
		$holidays = Holiday::where('supplier_id', $supplier->id)->get();
		$tod = [];
		$appointments = Appointment::where('date', $appointment->date)
			->where('payment_received', 1)
			->where('supplier_id', $supplier->id)
			->get();

		if ($appointment->canceled === 1) {

			Session::flash('error', 'You cannot edit a canceled appointment.');

			return view('supplier.appointment.show')->with(compact('appointment', 'supplier'));

		}


			if ($appointments->count() > 0) {

				foreach ($appointments as $appointmenting) {
					$tod[$appointmenting->id] = $appointmenting->time_of_day;
				}

				if (count($tod) >= 3) {

					Session::flash('error', 'There is no more availability on this day. You can only adjust the time.');

					return view('supplier.appointment.edit')->with(compact('appointment', 'supplier','holidays', 'tod'));

				}
			}


		return view('supplier.appointment.edit')->with(compact('appointment', 'supplier','holidays', 'tod'));
	}

    public function getServices()
    {

        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();
        $step = null;
        $treatments = $supplier->treatments;

        if ($supplier === null) {
            return view('supplier.register')->with(compact('user', 'step', 'supplier'));
        }

        return view('supplier.services')->with(compact('user', 'supplier', 'treatments'));

    }

    public function getCreateListing()
    {

        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();
        $step = null;

        if ($supplier->services->count() !== $supplier->treatments->count()) {

            $datetime = date('Y-m-d H:i:s');

            $serviceSlug = '{{ serviceSlug | slugify }}-' . strtolower($user->firstname) . '-' . $supplier->id;
            $tagSlug = '{{ tagSlug | slugify }}';

            $data = [];
            $data['slug'] = $serviceSlug;
            $data['tagSlug'] = $tagSlug;

            $treatments = $supplier->treatments;

            $treatment = [];

            foreach ($treatments as $treatmentName) {
                $treatment[$treatmentName->id] = $treatmentName->name;
            }

            $tags = Tag::all();

            $tag = [];

            foreach ($tags as $tagName) {
                $tag[$tagName->id] = $tagName->name;
            }

            if ($supplier === null) {
                return view('supplier.register')->with(compact('user', 'step', 'supplier'));
            }

            $usedID = [];

            foreach ($supplier->services as $serv) {
                $usedID[$serv->service_type] = $serv->service_type;
            }

            return view('supplier.create')->with(compact('user', 'supplier', 'data', 'datetime', 'treatment', 'treatments', 'tag', 'usedID'));

        }


        Session::flash('error', 'You have created listing for all of your service item.');

        return redirect()->route('supplier.services');


    }

    public function getServiceEdit($id)
    {

        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();
        $service = Service::where('id', $id)->first();
        $treatment = Treatment::where('id', $service->service_type)->first();
        $step = null;

        $datetime = date('Y-m-d H:i:s');

        $serviceSlug = '{{ serviceSlug | slugify }}-' . strtolower($user->firstname) . '-' . $supplier->id;
        $tagSlug = '{{ tagSlug | slugify }}';

        $data = [];
        $data['slug'] = $serviceSlug;
        $data['tagSlug'] = $tagSlug;

        $treat = [];

        $treatments = $supplier->treatments;

        foreach ($treatments as $treatmentName) {
            $treat[$treatmentName->id] = $treatmentName->name;
        }

        $tags = Tag::all();

        $tag = [];

        foreach ($tags as $tagName) {
            $tag[$tagName->id] = $tagName->name;
        }

        if ($supplier === null) {
            return view('supplier.register')->with(compact('user', 'step', 'supplier'));
        }

        return view('supplier.edit')->with(compact('user', 'supplier', 'data', 'datetime', 'treatment', 'treatments', 'service', 'tag'));

    }


    public function show(Request $request, $id)
    {
        $user = Auth::user();
        $supplier = Supplier::find($id);

        return view('supplier.show')->with(compact('user', 'supplier'));
    }

    public function store(Request $request, $user_id)
    {

        $user = User::find($user_id);
        $supplierExist = Supplier::where('user_id', $user->id)->first();
        $step = $request->get('step');
        $treatments = Treatment::all();

        $treatment = [];

        foreach ($treatments as $treatmentName) {
            $treatment[$treatmentName->id] = $treatmentName->name;
        }

        if ($step === null) {

            $this->validate($request, [
                'dob' => 'required|date',
                'contact_number' => 'required|max:11|min:11',
                'company' => 'required|max:255|unique:suppliers',
                'website' => 'sometimes|max:255',
                'address1' => 'required|max:255',
                'address2' => 'sometimes|max:255',
                'city' => 'required|max:255',
                'postal_code' => 'required|max:10'
            ]);

            $supplier = new Supplier();

//            $supplier->dob = $request->dob;
			$dpValue = $request->dob;

			// Parse a date using a user-defined format
			$date = DateTime::createFromFormat('d/m/Y', $dpValue);

			// Using the parsed date we can create a
			// new one with a formatting of our choosing
			$dateForTable = $date->format('Y-m-d');

//		dd($dateForTable);

			$supplier->dob = $dateForTable;

            $supplier->contact_number = $request->contact_number;

            $supplier->company = $request->company;
            $supplier->address1 = $request->address1;

            if (isset($request->address2)) {
                $supplier->address2 = $request->address2;
            }

            $supplier->city = $request->city;
            $supplier->postal_code = $request->postal_code;
            $supplier->step = '2';

            // build up the address
            if (isset($request->address2)) {
                $fullAddress = $request->address1 . '+' . $request->address2 . '+' . $request->city . '+' . $request->postal_code;
            } else {
                $fullAddress = $request->address1 . '+' . $request->city . '+' . $request->postal_code;
            }

//			dd($fullAddress);

            // get and store latitude and longitude
//			$clientLatAndLong = MapFunctions::getLatitudeAndLongitude($fullAddress);
            $clientLatAndLong = MapFunctions::getLongitudeAndLatitude($fullAddress);

            $supplier->longitude = $clientLatAndLong[0];
            $supplier->latitude = $clientLatAndLong[1];

            if ($supplierExist === null) {
                $supplier->user()->associate($user);

                $supplier->save();
            }

            return redirect()->route('supplier.register', ['step' => 2])->with(compact('supplier', 'treatment'));
        }

    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $supplier = Supplier::where('user_id', $user->id)->first();
        $step = $request->get('step');
		$settings = Setting::first();

        if ($supplier !== null && $step === '2') {

            $this->validate($request, [
                'years_exp' => 'required'
            ]);

            $supplier->years_exp = $request->years_exp;
            $supplier->step = '3';

            $supplier->save();

            if (isset($request->treatments)) {
                $supplier->treatments()->sync($request->treatments, false);
            } else {
                $supplier->treatments()->sync([]);
            }

            return redirect()->route('supplier.register', ['step' => 3])->with(compact('supplier'));
        }

        if ($supplier !== null && $step === '3') {

            $this->validate($request, [
                'opening_time' => 'required',
                'closing_time' => 'required',
                'work_eligibility' => 'required',
                'referee1_name' => 'required|max:255|different:referee2_name',
                'referee1_contact_number' => 'required|max:255|different:referee2_contact_number',
                'referee1_email' => 'required|max:255|email|different:referee2_email',
                'referee2_name' => 'required|max:255|different:referee1_name',
                'referee2_contact_number' => 'required|max:255|different:referee1_contact_number',
                'referee2_email' => 'required|max:255|email|different:referee1_email',
                'signup_complete' => 'required|boolean|max:1'
            ]);


            $supplier->opening_time = $request->opening_time;
            $supplier->closing_time = $request->closing_time;
            $supplier->work_eligibility = $request->work_eligibility;
            $supplier->referee1_name = $request->referee1_name;
            $supplier->referee1_contact_number = $request->referee1_contact_number;
            $supplier->referee1_email = $request->referee1_email;
            $supplier->referee2_name = $request->referee2_name;
            $supplier->referee2_contact_number = $request->referee2_contact_number;
            $supplier->referee2_email = $request->referee2_email;
            $supplier->signup_complete = $request->signup_complete;

            $supplier->step = '4';

            $supplier->save();

            if (isset($request->days)) {
                $supplier->days()->sync($request->days, false);
            } else {
                $supplier->days()->sync([]);
            }

			$data = [
				'from' => 'no-reply@pamperhub.com',
				'email' => $supplier->user->email,
				'firstname' => $supplier->user->firstname,
				'lastname' => $supplier->user->lastname,
				'subject' => 'Registration Complete',
				'admin_email' => 'admin@pamperhub.com',
				'admin_subject' => 'Supplier Registration Complete ' . $supplier->user->email
			];

			Mail::send('emails.supplier-signup-complete', $data, function($message) use ($data, $settings){
				$message->to($data['email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			Mail::send('emails.supplier-signup-admin', $data, function($message) use ($data, $settings){
				$message->to($data['admin_email']);
				$message->subject($data['admin_subject']);
				$message->from($data['from'], $settings->site_name);
			});

            return redirect()->route('supplier.register', ['step' => 4])->with(compact('supplier'));
        }

        $page = $request->get('page');
        $supplier = Supplier::find($id);
		$today = Carbon::today();


        if ($page === 'deactivate') {

        	$appointments = Appointment::where('supplier_id', $supplier->id)
				->where('date', '>=', date('Y-m-d', strtotime($today)))
				->get();

        	if (count($appointments) !== 0) {

//        		dd();

        		$checkAppointments = Appointment::where('supplier_id', $supplier->id)
					->where('date', '>=', date('Y-m-d', strtotime($today)))
					->where('canceled', 0)
					->get();

				if(count($checkAppointments) > 0) {
					Session::flash('error', 'Supplier cannot be deactivated. Appointments are scheduled');

					return redirect()->route('admin.suppliers');
				}
			}

            $this->validate($request, [
                'active' => 'required|boolean'
            ]);

            $supplier->active = $request->active;

            $supplier->save();

            if ($request->active === '1') {
                Session::flash('success', 'Supplier Activated.');
            } else {
                Session::flash('success', 'Supplier Deactivated.');
            }

            if ($request->active === '1') {

				$data = [
					'from' => 'no-reply@pamperhub.com',
					'email' => $supplier->user->email,
					'firstname' => $supplier->user->firstname,
					'lastname' => $supplier->user->lastname,
					'subject' => 'Your account has been Activated!',
				];

				Mail::send('emails.supplier-activated', $data, function($message) use ($data, $settings){
					$message->to($data['email']);
					$message->subject($data['subject']);
					$message->from($data['from'], $settings->site_name);
				});

			} else {

				$data = [
					'from' => 'no-reply@pamperhub.com',
					'email' => $supplier->user->email,
					'firstname' => $supplier->user->firstname,
					'lastname' => $supplier->user->lastname,
					'subject' => 'Your account has been Deactivated',
				];

				Mail::send('emails.supplier-deactivated', $data, function($message) use ($data, $settings){
					$message->to($data['email']);
					$message->subject($data['subject']);
					$message->from($data['from'], $settings->site_name);
				});

			}

            return redirect()->route('admin.suppliers');
        }

        if ($page === 'basic') {

            $this->validate($request, [
            	'bio' => 'sometimes',
                'company' => 'sometimes|max:255',
                'address1' => 'required|max:255',
                'address2' => 'sometimes|max:255',
                'city' => 'required|max:255',
                'postal_code' => 'required|max:10'
            ]);

            $supplier->company = $request->company;
            $supplier->address1 = $request->address1;

            if (isset($request->address2)) {
                $supplier->address2 = $request->address2;
            }

            $supplier->city = $request->city;
            $supplier->postal_code = $request->postal_code;

            if (isset($request->address2)) {
                $fullAddress = $request->address1 . '+' . $request->address2 . '+' . $request->city . '+' . $request->postal_code;
            } else {
                $fullAddress = $request->address1 . '+' . $request->city . '+' . $request->postal_code;
            }

            // get and store latitude and longitude
            $clientLatAndLong = MapFunctions::getLongitudeAndLatitude($fullAddress);

            $supplier->longitude = $clientLatAndLong[0];
            $supplier->latitude = $clientLatAndLong[1];

			if (isset($request->bio)) {
				$supplier->bio = $request->bio;
			}

            $supplier->save();

            if (isset($request->treatments)) {
                $supplier->treatments()->sync($request->treatments, true);
            } else {
                $supplier->treatments()->sync([]);
            }

            Session::flash('success', 'Supplier Details updated.');

            return redirect()->route('supplier');

        }

        if ($page === 'availability') {

            $this->validate($request, [
                'opening_time' => 'required',
                'closing_time' => 'required',
            ]);

            $supplier->opening_time = $request->opening_time;
            $supplier->closing_time = $request->closing_time;

            $supplier->save();

            if (isset($request->days)) {
                $supplier->days()->sync($request->days, true);
            } else {
                $supplier->days()->sync([]);
            }

            Session::flash('success', 'Availability information updated.');

            return redirect()->route('supplier.availability');

        }

        if ($page === 'admin') {

            $this->validate($request, [
                'contact_number' => 'required|max:11|min:11',
                'company' => 'required|max:255',
                'website' => 'sometimes|max:255',
                'address1' => 'required|max:255',
                'address2' => 'sometimes|max:255',
                'city' => 'required|max:255',
                'postal_code' => 'required|max:10',
                'years_exp' => 'required',
                'opening_time' => 'required',
                'closing_time' => 'required',
                'work_eligibility' => 'required',
                'referee1_name' => 'required|max:255|different:referee2_name',
                'referee1_contact_number' => 'required|max:255|different:referee2_contact_number',
                'referee1_email' => 'required|max:255|email|different:referee2_email',
                'referee2_name' => 'required|max:255|different:referee1_name',
                'referee2_contact_number' => 'required|max:255|different:referee1_contact_number',
                'referee2_email' => 'required|max:255|email|different:referee1_email',
            ]);

            $supplier->contact_number = $request->contact_number;

            $supplier->company = $request->company;
            $supplier->address1 = $request->address1;

            if ($request->address2 !== '') {
                $supplier->address2 = $request->address2;
            }

            $supplier->city = $request->city;
            $supplier->postal_code = $request->postal_code;
            $supplier->years_exp = $request->years_exp;
            $supplier->opening_time = $request->opening_time;
            $supplier->closing_time = $request->closing_time;
            $supplier->work_eligibility = $request->work_eligibility;
            $supplier->referee1_name = $request->referee1_name;
            $supplier->referee1_contact_number = $request->referee1_contact_number;
            $supplier->referee1_email = $request->referee1_email;
            $supplier->referee2_name = $request->referee2_name;
            $supplier->referee2_contact_number = $request->referee2_contact_number;
            $supplier->referee2_email = $request->referee2_email;

            $supplier->save();

            if (isset($request->days)) {
                $supplier->days()->sync($request->days, true);
            } else {
                $supplier->days()->sync([]);
            }

            if (isset($request->treatments)) {
                $supplier->treatments()->sync($request->treatments, true);
            } else {
                $supplier->treatments()->sync([]);
            }

            Session::flash('success', 'Supplier information updated.');

            return redirect()->route('admin.suppliers.show', $supplier->id);

        }

        // Update Image
        if ($page === 'image') {

            // Validate the Data
            $this->validate($request, array(
                'display_picture' => 'required|mimes:jpeg,jpg,bmp,png,gif|max:2000',
            ));

            if ($request->hasFile('display_picture')) {

                $image = $request->file('display_picture');
                $filename = strtolower($user->firstname) . '_' . strtolower($user->lastname) . '_' . strtolower($user->id) . '_avatar.' . $image->getClientOriginalExtension();
                $location = public_path('uploads/suppliers/' . $filename);

                Image::make($image)->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->crop(400, 400)->save($location);

				// Grab old file
				$oldFilename = $supplier->display_picture;

                $supplier->display_picture = $filename;

                $supplier->save();

				if ($oldFilename !== $filename) {
					// Delete old Photo
					File::delete('uploads/services/' . $oldFilename);
				}

                Session::flash('success', 'Image Updated');

                return back();

            } elseif(!$request->hasFile('display_picture')) {

                Session::flash('error', 'No Image chosen');

                return back();

            }

        }


    }
}
