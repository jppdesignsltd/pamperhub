<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Treatment;
use Session;

class TreatmentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('role:super-admin|customer-service-rep');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('treatments.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name'       => 'required|max:255',
        ));

        // Create Tag in DB
        $treatment = new Treatment;
        $treatment->name = $request->name;

        $treatment->save();

        // Create Flash Session Message
        Session::flash('success', 'Treatment Type Created');

        // Redirect to Page
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$treatment = Treatment::find($id);

        return view('treatments.edit')->with(compact('treatment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$treatment = Treatment::find($id);

		$this->validate($request, array(
			'name'       => 'required|max:255',
		));

		$treatment->name = $request->name;

		$treatment->save();

		// Create Flash Session Message
		Session::flash('success', 'Treatment Type Updated');

		return redirect()->route('admin.treatments');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$treatment = Treatment::find($id);

		if ($treatment->suppliers()->count() === 0) {

			$treatment->delete();

			Session::flash('success', 'The Treatment type was successfully deleted.');

			return redirect()->route('admin.treatments');

		}

		Session::flash('error', 'This treatment type cannot be deleted.');

		return redirect()->route('admin.treatments');

//
    }

}
