<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Setting;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Auth;
use App\Customer;
use Illuminate\Support\Facades\Mail;
use Session;
use Image;
use File;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:customer|super-admin');

    }

    public function index() {

        $user = Auth::user();
        $customer = Customer::where('user_id', $user->id)->first();

        return view('customer.index')->with(compact('user', 'customer'));

    }

	public function getBookings(Request $request) {

		$today = Carbon::today();
		$page = $request->get('page');
		$user = Auth::user();
		$customer = Customer::where('user_id', $user->id)->first();

		$next_appointment = Appointment::where('customer_id', $customer->id)
			->where('supplier_confirmed', 1)
			->where('canceled', 0)
			->where('date', '>=', $today)
			->orderBy('date')
			->first();

		$appointments = Appointment::where('customer_id', $customer->id)
			->orderBy('date')
			->where('payment_received', 1)
			->where('date', '>=', $today)
//			->where('canceled', 0)
			->paginate(2);

		return view('customer.bookings')->with(compact('user', 'customer','appointments','next_appointment', 'page'));

	}

    public function update(Request $request, $id)
    {

		$user = Auth::user();
		$page = $request->get('page');
		$customer = Customer::find($id);
		$settings = Setting::first();

		if ($request->ajax()) {

			try {
//				 Validate the Data
				$this->validate($request, array(
					'address1'       => 'sometimes|max:255',
					'address2'       => 'sometimes|max:255',
					'city'       => 'sometimes|max:255',
					'postal_code'       => 'sometimes|max:255',
				));

				if (isset($request->address1)) {
					$customer->address1 = $request->address1;
				}

				if ($request->address2 !== '') {
					$customer->address2 = $request->address2;
				}

				if (isset($request->city)) {
					$customer->city = $request->city;
				}

				if (isset($request->postal_code)) {
					$customer->postal_code = $request->postal_code;
				}

				$customer->save();

				return response()->json($customer);

			}
			catch(\Exception $e) {

				return response()->json($e->getMessage(), 422);
			}
		}

		// Update Image
		if ($page === 'image') {

			// Validate the Data
			$this->validate($request, array(
				'display_picture' => 'required|mimes:jpeg,jpg,bmp,png,gif|max:2000',
			));

			if ($request->hasFile('display_picture')) {

				$image = $request->file('display_picture');
				$filename = strtolower($user->firstname) . '_' . strtolower($user->lastname) . '_' . strtolower($user->id) . '_avatar.' . $image->getClientOriginalExtension();
				$location = public_path('uploads/customers/' . $filename);

				Image::make($image)->resize(400, null, function ($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->crop(400, 400)->save($location);

				// Grab old file
				$oldFilename = $customer->display_picture;

				$customer->display_picture = $filename;

				$customer->save();

				if ($oldFilename !== $filename) {
					// Delete old Photo
					File::delete('uploads/customers/' . $oldFilename);
				}

				Session::flash('success', 'Image Updated');

				return back();

			} elseif(!$request->hasFile('display_picture')) {

				Session::flash('error', 'No Image chosen');

				return back();

			}

		}

    	if ($page === 'deactivate') {

			$today = Carbon::today();

			$appointments = Appointment::where('customer_id', $customer->id)
				->where('date', '>=', date('Y-m-d', strtotime($today)))
				->get();

			if (count($appointments) !== 0) {

//        		dd();

				$checkAppointments = Appointment::where('supplier_id', $customer->id)
					->where('date', '>=', date('Y-m-d', strtotime($today)))
					->where('canceled', 0)
					->get();

				if(count($checkAppointments) > 0) {
					Session::flash('error', 'Customer cannot be deactivated. Appointments are scheduled');

					return redirect()->route('admin.customers');
				}
			}

    		$this->validate($request, [
    			'active' => 'required|boolean'
			]);

    		$customer->active = $request->active;

    		$customer->save();

    		if ($request->active === '1') {
				Session::flash('success', 'Customer Activated.');
			} else {
				Session::flash('success', 'Customer Deactivated.');
			}

			if ($request->active === '1') {

				$data = [
					'from' => 'no-reply@pamperhub.com',
					'email' => $customer->user->email,
					'firstname' => $customer->user->firstname,
					'lastname' => $customer->user->lastname,
					'subject' => 'Your account has been Activated!',
				];

				Mail::send('emails.customer-activated', $data, function($message) use ($data, $settings){
					$message->to($data['email']);
					$message->subject($data['subject']);
					$message->from($data['from'], $settings->site_name);
				});

			} else {

				$data = [
					'from' => 'no-reply@pamperhub.com',
					'email' => $customer->user->email,
					'firstname' => $customer->user->firstname,
					'lastname' => $customer->user->lastname,
					'subject' => 'Your account has been Deactivated',
				];

				Mail::send('emails.customer-deactivated', $data, function($message) use ($data, $settings){
					$message->to($data['email']);
					$message->subject($data['subject']);
					$message->from($data['from'], $settings->site_name);
				});

			}


			return redirect()->route('admin.customers');
		}

        $this->validate($request, [
            'dob' => 'required',
            'contact_number' => 'required|max:11|min:11',
            'address1' => 'required|max:255',
            'address2' => 'sometimes|max:255',
            'city' => 'required|max:255',
            'postal_code' => 'required|max:10'
        ]);

		$dpValue = $request->dob;

		// Parse a date using a user-defined format
		$date = DateTime::createFromFormat('d/m/Y', $dpValue);

		// Using the parsed date we can create a
		// new one with a formatting of our choosing
		$dateForTable = $date->format('Y-m-d');

//		dd($dateForTable);

        $customer->dob = $dateForTable;
        $customer->contact_number = $request->contact_number;
        $customer->address1 = $request->address1;

        if ($request->address2 !== '') {
            $customer->address2 = $request->address2;
        }

        $customer->city = $request->city;
        $customer->postal_code = $request->postal_code;

        $customer->save();

        Session::flash('success', 'Custom Details updated.');

		if ($page === 'admin') {
			return redirect()->route('admin.customers.show', $id);
		}

        return redirect()->route('customer.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $customer = Customer::find($id);
//        $customer->delete();
//
//        Session::flash('success', 'The Customer was successfully deleted.');
//
//        return redirect()->route('admin.customers');
    }
}
