<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Session;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:super-admin|customer-service-rep|supplier');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$tagSlug = '{{ tagSlug | slugify }}';

		$data = [];
		$data['slug'] = $tagSlug;

		return view('tags.index')->with(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if ($request->ajax()) {
			try {
				// Validate the Data
				$this->validate($request, array(
					'name'       => 'required|max:255',
					'slug'       => 'required|alpha_dash|min:1|max:255|unique:tags,slug',
				));

				// Store the Data in the Database
				$tag = new Tag;

				$tag->name = $request->name;
				$tag->slug = $request->slug;

				$tag->save();

				return response()->json($tag);

			}
			catch(\Exception $e) {

				return response()->json($e->getMessage(), 422);
			}
		}

		$this->validate($request, array(
			'name'       => 'required|max:255',
			'slug'        => 'required|alpha_dash|min:1|max:255|unique:tags,slug',
		));

		// Create Tag in DB
		$tag = new Tag;
		$tag->name = $request->name;
		$tag->slug = $request->slug;

		$tag->save();

		// Create Flash Session Message
		Session::flash('success', 'Treatment Type Created');

		// Redirect to Page
		return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$tag = Tag::find($id);

		return view('tags.edit')->with(compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$tag = Tag::find($id);

		$this->validate($request, array(
			'name'       => 'required|max:255',
			'slug'        => "required|alpha_dash|min:1|max:255|unique:tags,slug,$id",
		));

		$tag->name = $request->name;
		$tag->slug = $request->slug;

		$tag->save();

		// Create Flash Session Message
		Session::flash('success', 'Tag Updated');

		return redirect()->route('admin.tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

		$tag = Tag::find($id);

		$tag->services()->detach();
//		$tag->posts()->detach();

		$tag->delete();

		Session::flash('success', 'The Tag was successfully deleted.');

		return redirect()->route('admin.tags');
    }
}
