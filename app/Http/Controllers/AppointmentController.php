<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Customer;
use App\Holiday;
use App\Service;
use App\Setting;
use App\Supplier;
use App\Treatment;
use DateTime;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Mail;
use Session;

class AppointmentController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$appointmentsPaginate = Appointment::orderBy('id', 'desc')->paginate(10);

		return view('admin.appointments')->with(compact('appointmentsPaginate'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		$user = Auth::user();
		$s = $request->get('s');
		$customer = Customer::where('user_id', $user->id)->first();
		$service = Service::where('id', $s)->first();
		$supplier = Supplier::where('id', $service->supplier_id)->first();
		$holidays = Holiday::where('supplier_id', $supplier->id)->get();
		$tod = [];
		$lockdate = false;
		$dpValue = '';
		$dateForTable = '';

		return view('booking.create')->with(compact('customer', 'service', 'dateForTable', 'tod', 'lockdate', 'dpValue', 'holidays'));
	}

//	public function checkBooking(Request $request)
//	{
//
//		$dpValue = $request->date;
//		$date = DateTime::createFromFormat('d/m/Y', $dpValue);
//		$dateForTable = $date->format('Y-m-d');
//
//		$appointment = Appointment::where('date', $dateForTable)
//			->where('time_of_day', $request->time_of_day)
//			->first();
//
//		if ($appointment) {
//
//			$customer = Session::get('customer');
//			$service = Session::get('service');
//
//			Session::flash('error', 'That time of day is not available');
//
//			return Redirect::back();
//		}
//
//	}

//	/**
//	 * Display the specified resource.
//	 *
//	 * @param  int $id
//	 * @return \Illuminate\Http\Response
//	 */
//	public function show($id)
//	{
//		// We will show this in the admin portal
//
//		$appointment = Appointment::where('id', $id)->first();
//
//		return view('admin.appointment.edit')->with(compact('appointment'));
//
//	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		// We will show this in the admin portal the other edit functionality is in the supplier controller

		$appointment = Appointment::where('id', $id)->first();
		$supplier = Supplier::where('id', $appointment->supplier_id)->first();
		$holidays = Holiday::where('supplier_id', $supplier->id)->get();
		$tod = [];
		$appointments = Appointment::where('date', $appointment->date)
			->where('payment_received', 1)
			->where('supplier_id', $supplier->id)
			->get();


		if ($appointments->count() > 0) {

			foreach ($appointments as $appointmenting) {
				$tod[$appointmenting->id] = $appointmenting->time_of_day;
			}

			if (count($tod) >= 3) {

				Session::flash('error', 'There is no more availability on this day. You can only adjust the time.');
			}
		}

//		dd($tod);

		return view('admin.appointment.edit')->with(compact('appointment', 'holidays', 'tod'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$method = $request->get('m');
		$user = Auth::user();
		$appointment = Appointment::where('id', $id)->first();
		$supplier = Supplier::where('id', $appointment->supplier_id)->first();
		$customer = Customer::where('id', $appointment->customer_id)->first();
		$settings = Setting::first();

		$existingTime = $appointment->time;

//		dd($supplier);

//		dd($existingTime);

		if ($method === 'cancel') {

			$appointment->canceled = 1;

			$appointment->save();

			$data = [
				'from' => 'support@pamperhub.com',
				'customer_email' => $customer->user->email,
				'customer_firstname' => $customer->user->firstname,
				'customer_lastname' => $customer->user->lastname,
				'supplier_email' => $supplier->user->email,
				'supplier_firstname' => $supplier->user->firstname,
				'supplier_lastname' => $supplier->user->lastname,
				'admin_email' => 'admin@pamperhub.com',
				'orderNo' => 'ORD-0' . $appointment->id,
				'treatmentName' => $appointment->treatment->name,
				'date' => $appointment->date,
				'subject' => 'Appointment Canceled: ORD-0' . $appointment->id
			];

			// Customer Email
			Mail::send('emails.appointment-canceled-customer', $data, function($message) use ($data, $settings){
				$message->to($data['customer_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			// Supplier Email
			Mail::send('emails.appointment-canceled-supplier', $data, function($message) use ($data, $settings){
				$message->to($data['supplier_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			// Admin Email
			Mail::send('emails.appointment-canceled-admin', $data, function($message) use ($data, $settings){
				$message->to($data['admin_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			Session::flash('success', 'Appointment updated. We have notified all relevant parties.');

			return redirect()->route('admin.appointments');

		}

		if ($method === 'remove_cancel') {

			$appointment->canceled = 0;

			$appointment->save();

			$data = [
				'from' => 'no-reply@pamperhub.com',
				'customer_email' => $customer->user->email,
				'customer_firstname' => $customer->user->firstname,
				'customer_lastname' => $customer->user->lastname,
				'supplier_email' => $supplier->user->email,
				'supplier_firstname' => $supplier->user->firstname,
				'supplier_lastname' => $supplier->user->lastname,
				'admin_email' => 'admin@pamperhub.com',
				'orderNo' => 'ORD-0' . $appointment->id,
				'treatmentName' => $appointment->treatment->name,
				'date' => $appointment->date,
				'subject' => 'Appointment Restored: ORD-0' . $appointment->id
			];

			// Customer Email
			Mail::send('emails.appointment-restored-customer', $data, function($message) use ($data, $settings){
				$message->to($data['customer_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			// Supplier Email
			Mail::send('emails.appointment-restored-supplier', $data, function($message) use ($data, $settings){
				$message->to($data['supplier_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			// Admin Email
			Mail::send('emails.appointment-restored-admin', $data, function($message) use ($data, $settings){
				$message->to($data['admin_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			Session::flash('success', 'Appointment updated. We have notified all relevant parties.');

//			return redirect()->route('admin.appointments');
			return back();

		}

		$this->validate($request, [
			'date' => 'sometimes',
			'time' => 'sometimes',
			'time_of_day' => 'sometimes',
		]);


		if (isset($request->time)) {

			// The value from the datepicker
			// Parse a date using a user-defined format
			$time = DateTime::createFromFormat('H:i A', $request->time);

//		dd($time);

			// Using the parsed date we can create a
			// new one with a formatting of our choosing
			$timeForTable = $time->format('H:i:s');

//		dd($timeForTable);

			$appointment->time = $timeForTable;

		}

		$appointment->supplier_confirmed = 1;

//		dd($time);

		if (isset($request->time_of_day)){
			$appointment->time_of_day = $request->time_of_day;
		}

		if (isset($request->date)){

			// Parse a date using a user-defined format
			$date = DateTime::createFromFormat('d/m/Y', $request->date);

			// Using the parsed date we can create a
			// new one with a formatting of our choosing
			$dateForTable = $date->format('Y-m-d');

			$appointment->date = $dateForTable;
		}

		$appointment->save();

//		dd();

		// ICS
		$event[0] = "BEGIN:VCALENDAR";
		$event[1] = "METHOD:PUBLISH";
		$event[2] = "VERSION:2.0";
		$event[3] = "X-WR-CALNAME:" . $appointment->treatment->name . " Appointment";
		$event[4] = "PRODID:-//PamperHub//Appointment Invite//EN";
		$event[5] = "X-WR-TIMEZONE:Europe/London";
		$event[6] = "CALSCALE:GREGORIAN";
		$event[7] = "NAME:" . $appointment->treatment->name . " Appointment";
		$event[8] = "BEGIN:VEVENT";
		$event[9] = "ORGANIZER;CN=PamperHub Ltd:MAILTO:bookings@pamperhub.com";
		$event[10] = "TRANSP:TRANSPARENT";
		$event[11] = "DTEND:" . date('Ymd', strtotime($appointment->date)) . "T" . date('His', strtotime($appointment->time)) . "Z";
		$event[12] = "UID:" . md5(uniqid(mt_rand(), true));
		$event[13] = "DTSTAMP:" . gmdate('Ymd') . 'T' . gmdate('His') . "Z";
		$event[14] = "LOCATION:" . $appointment->customer->address1 . ' ' . $appointment->customer->city . ' ' . $appointment->customer->postal_code;
		$event[15] = "X-WR-CALDESC:You have made a booking for " . date('d/m/Y', strtotime($appointment->date)) . " at " . date('h:i A', strtotime($appointment->time)) . ". Your therapist is " . $appointment->supplier->user->firstname . " " . $appointment->supplier->user->lastname . ". Their contact email is " . $appointment->supplier->user->email . ". Further details can be found in your bookings section in your account. Regards PamperHub";
		$event[16] = "DESCRIPTION:You have made a booking for " . date('d/m/Y', strtotime($appointment->date)) . " at " . date('h:i A', strtotime($appointment->time)) . ". Your therapist is " . $appointment->supplier->user->firstname . " " . $appointment->supplier->user->lastname . ". Their contact email is " . $appointment->supplier->user->email . ". Further details can be found in your bookings section in your account. Regards PamperHub";
		$event[17] = "SEQUENCE:0";
		$event[18] = "CATEGORIES:PamperHub";
		$event[19] = "SUMMARY;LANGUAGE=en-GB:PamperHub: " . $appointment->treatment->name . " Appointment.";
		$event[20] = "TIMEZONE-ID:Europe/London";
		$event[21] = "DTSTART:" . date('Ymd', strtotime($appointment->date)) . "T" . date('His', strtotime($appointment->time) - 60*60) . "Z";
		$event[22] = "ATTENDEE:" . $appointment->customer->user->email;
		$event[23] = "ATTENDEE:" . $appointment->supplier->user->email;
		$event[24] = "END:VEVENT";
		$event[25] = "END:VCALENDAR";

		$event = implode("\r\n", $event);

//		dd($event);

		if ($existingTime !== null) {

			$data = [
				'from' => 'no-reply@pamperhub.com',
				'customer_email' => $customer->user->email,
				'customer_firstname' => $customer->user->firstname,
				'customer_lastname' => $customer->user->lastname,
				'supplier_email' => $supplier->user->email,
				'supplier_firstname' => $supplier->user->firstname,
				'supplier_lastname' => $supplier->user->lastname,
				'admin_email' => 'admin@pamperhub.com',
				'orderNo' => 'ORD-0' . $appointment->id,
				'treatmentName' => $appointment->treatment->name,
				'date' => $appointment->date,
				'time' => $appointment->time,
				'subject' => 'Appointment Updated: ORD-0' . $appointment->id
			];

			// Customer Email
			Mail::send('emails.appointment-updated-customer', $data, function($message) use ($data, $event, $settings){
				$message->to($data['customer_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
				$message->attachData($event, 'pamperhub_event.ics', [
					'mime' => 'text/calendar',
				]);
			});

			// Supplier Email
			Mail::send('emails.appointment-updated-supplier', $data, function($message) use ($data, $event,$settings){
				$message->to($data['supplier_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
				$message->attachData($event, 'pamperhub_event.ics', [
					'mime' => 'text/calendar',
				]);
			});

			// Admin Email
			Mail::send('emails.appointment-updated-admin', $data, function($message) use ($data, $settings){
				$message->to($data['admin_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

		} else {

			$data = [
				'from' => 'no-reply@pamperhub.com',
				'customer_email' => $customer->user->email,
				'customer_firstname' => $customer->user->firstname,
				'customer_lastname' => $customer->user->lastname,
				'supplier_email' => $supplier->user->email,
				'supplier_firstname' => $supplier->user->firstname,
				'supplier_lastname' => $supplier->user->lastname,
				'admin_email' => 'admin@pamperhub.com',
				'orderNo' => 'ORD-0' . $appointment->id,
				'treatmentName' => $appointment->treatment->name,
				'date' => $appointment->date,
				'time' => $appointment->time,
				'subject' => 'Appointment Confirmed: ORD-0' . $appointment->id
			];

			// Customer Email
			Mail::send('emails.appointment-confirmed-customer', $data, function($message) use ($data, $settings){
				$message->to($data['customer_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			// Supplier Email
			Mail::send('emails.appointment-confirmed-supplier', $data, function($message) use ($data, $settings){
				$message->to($data['supplier_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

			// Admin Email
			Mail::send('emails.appointment-confirmed-admin', $data, function($message) use ($data, $settings){
				$message->to($data['admin_email']);
				$message->subject($data['subject']);
				$message->from($data['from'], $settings->site_name);
			});

		}

		Session::flash('success', 'Appointment updated. We have notified all relevant parties.');

		if ($method === 'admin') {
			return redirect()->route('admin.appointments');
		}

		return view('supplier.appointment.show')->with(compact('appointment', 'supplier'));
	}
}
