<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Session;

class SettingController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role:super-admin');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$setting = Setting::first();

        return view('settings.index')->with(compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

		$setting = Setting::find($id);

		$this->validate($request, [
			'site_name' => 'required',
			'tagline' => 'required',
			'home_tagline' => 'required',
			'email_address' => 'required',
			'default_page_author' => 'required',
			'default_keywords' => 'required',
			'default_description' => 'required',
			'breadcrumbs' => 'required',
			'services_per_page' => 'required',
			'services_excerpt' => 'required',
		]);

		$setting->site_name = $request->site_name;
		$setting->tagline = $request->tagline;
		$setting->home_tagline = $request->home_tagline;
		$setting->email_address = $request->email_address;
		$setting->default_page_author = $request->default_page_author;
		$setting->default_keywords = $request->default_keywords;
		$setting->default_description = $request->default_description;
		$setting->breadcrumbs = $request->breadcrumbs;
		$setting->services_per_page = $request->services_per_page;
		$setting->services_excerpt = $request->services_excerpt;

		$setting->save();

		Session::flash('success', 'Settings have been updated!');

		return redirect()->route('admin.settings');
    }

}
