<?php

namespace App\Http\ViewComposers;

use App\Appointment;
use App\Customer;
use App\Service;
use App\Setting;
use App\Supplier;
use App\Tag;
use App\Treatment;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Entrust;
use DateTime;

class SettingComposer
{

	public function __construct()
	{
		$this->user = Auth::user();
		$this->settings = Setting::first();
		$this->dateTime = date('Y-m-d H:i:s');

		if (Entrust::hasRole(['super-admin', 'customer-service-rep'])) {
			$this->suppliers = Supplier::all();
			$this->customers = Customer::all();
			$this->services = Service::all();
			$this->treatments = Treatment::all();
			$this->users = User::all();
			$this->tags = Tag::all();
			$this->appointments = Appointment::all();
		}
	}

	public function compose(View $view)
	{
		$view->with('user', $this->user);
		$view->with('dateTime', $this->dateTime);
		$view->with('settings', $this->settings);

		if (Entrust::hasRole(['super-admin', 'customer-service-rep'])) {
			$view->with('suppliers', $this->suppliers);
			$view->with('customers', $this->customers);
			$view->with('services', $this->services);
			$view->with('treatments', $this->treatments);
			$view->with('users', $this->users);
			$view->with('tags', $this->tags);
			$view->with('appointments', $this->appointments);
		}
	}

}