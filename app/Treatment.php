<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Suppliers That Belong to Treatments
     **/
    public function suppliers() {
        return $this->belongsToMany('App\Supplier');
    }

    /**
     * Services That Belong to Treatments
     **/
    public function services() {
        return $this->hasMany('App\Service', 'service_type');
    }

	public function appointments() {
		return $this->belongsToMany('App\Appointment');
	}
}
