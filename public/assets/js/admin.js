/*
* Main Compressed JS
*/

// Hide Alert Messages
$(document).ready(function () {
    function hideAlert() {
        $('.success_message').slideUp("slow");
    }
    setTimeout(hideAlert, 2000);
});

// -- Toggle Transition -- //
$(document).ready(function () {
    $("#menu-toggle").on("click", function() {
        $(this).toggleClass("active");
        $('#admin_menu-cont').toggleClass("active");
        $('body').toggleClass("active");
    });
});

function revealInput(hide, show) {
    $(show).toggleClass('hidden');
    $(hide).toggleClass('hidden');

    if (show === 'time') {
        $('#time-input').removeAttr('disabled');
    }
}

// Preview Image

var output = document.getElementById('image_preview');

function loadFile(event) {
    $('#image_preview-cont').toggleClass('hidden');
    $('#cover_image-cont').toggleClass('hidden');
    $('#clearImage').removeClass('hidden');

    if ($("#remove-image").length !== 0) {
        $("#remove-image").attr('disabled', 'disabled');
    }

    return output.src = URL.createObjectURL(event.target.files[0]);

}

function clearFilestyle() {
    $("#cover_image").filestyle('clear');
    $('#cover_image-cont').toggleClass('hidden');
    $('#clearImage').addClass('hidden');
    $('#image_preview-cont').toggleClass('hidden');
    output.src = '';



    if ($("#remove-image").length !== 0) {
        $("#remove-image").removeAttr('disabled');
    }
}