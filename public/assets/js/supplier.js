/**
 * Created by JoelPP on 10/04/2017.
 */

var output = document.getElementById('image_preview');

// Save Draft
$('#saveDraft').click(function () {
    $('#status').val('draft');
    // console.log('clicked draft');
});

// Save Draft
$('#publish').click(function () {
    $('#status').val('published');
    // console.log('clicked publish');
});

function revealInput(hide, show) {
    $(show).toggleClass('hidden').removeAttr('disabled');
    $('#' + show.id + ' :input').removeAttr('disabled');
    $(hide).toggleClass('hidden');

    // console.log(show.id);
}

function loadFile(event) {
    $('#image_preview-cont').toggleClass('hidden');
    $('#cover_image-cont').toggleClass('hidden');
    $('#clearImage').removeClass('hidden');

    if ($("#remove-image").length !== 0) {
        $("#remove-image").attr('disabled', 'disabled');
    }

    return output.src = URL.createObjectURL(event.target.files[0]);

}

function clearFilestyle() {
    $("#cover_image").filestyle('clear');
    $('#cover_image-cont').toggleClass('hidden');
    $('#clearImage').addClass('hidden');
    $('#image_preview-cont').toggleClass('hidden');
    output.src = '';



    if ($("#remove-image").length !== 0) {
        $("#remove-image").removeAttr('disabled');
    }
}
