/*
* Main Compressed JS
*/

var output = document.getElementById('image_preview');

// -- Toggle Transition -- //
$(document).ready(function () {
    $("#menu-toggle").on("click", function() {
        $(this).toggleClass("active");
        $('#menu').toggleClass("active");
        $('body').toggleClass("active");
    });
});

// Hide Alert Messages
$(document).ready(function () {
    function hideSuccessAlert() {
        $('.success_message').slideUp("slow");
    }

    function hideErrorAlert() {
        $('.error_message').slideUp("slow");
    }

    setTimeout(hideSuccessAlert, 2000);
    // setTimeout(hideErrorAlert, 4000);
});

function loadFile(event) {
    // $('#image_preview-cont').toggleClass('hidden');
    $('#cover_image-cont').toggleClass('hidden');
    $('#clearImage').removeClass('hidden');

    if ($("#remove-image").length !== 0) {
        $("#remove-image").attr('disabled', 'disabled');
    }

    return output.src = URL.createObjectURL(event.target.files[0]);
}

function clearFilestyle() {
    $("#cover_image").filestyle('clear');
    $('#cover_image-cont').toggleClass('hidden');
    $('#clearImage').addClass('hidden');
    $('#image_preview-cont').toggleClass('hidden');
    output.src = '';

    if ($("#remove-image").length !== 0) {
        $("#remove-image").removeAttr('disabled');
    }
}

// Supplier Account
function activateSubmit(elem, editBtn) {
    $('#'+ elem + 'Btn').toggleClass('hidden');
    $('#'+ editBtn).toggleClass('hidden');

    $('#' + elem + ' :input').prop("disabled", false);
}

function activateImgForm(elem, editBtn) {
    $('#'+ elem).toggleClass('hidden');
    $('#'+ editBtn).toggleClass('hidden');

    $('#' + elem + ' :input').prop("disabled", false);
}

// Tag AJAX create from Post and Portfolio
$('#tagForm').submit(function(e){
    e.preventDefault();
    if ( $(this).parsley().isValid() ) {
        var dataString = $(this).serialize();
        $('#tagLoader').toggleClass('fa-spin').toggleClass('hidden');
        $('#tagErrorResponse').children().remove();

        var promise = $.ajax({
            type: 'POST',
            url: '/tag',
            data: dataString
        });

        promise.done(function(response){
            $('#tagMultiple').append($("<option></option>").attr("value", response.id).attr("selected", '').text(response.name));
            $('#tagName').val('');
            $('#tagErrorResponse').children().remove();
            $('#tagLoader').toggleClass('fa-spin').toggleClass('hidden');
        });

        promise.fail(function(data){
            $('#tagErrorResponse').children().remove();
            // var errors = $.parseJSON(data.responseJSON);
            var errors = '';
            $('#tagErrorResponse').append($("<p class='text-danger small nmbottom'>" + errors + " Tag names must be unique</p>"))
            $('#tagLoader').toggleClass('fa-spin').toggleClass('hidden');
        });
    }
});