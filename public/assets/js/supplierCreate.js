angular.module("myApp", ["slugifier"]).

controller('tagController',['$scope',function($scope){
    $scope.tags = [
        {'title':'Create Tags', 'done':false}
    ];

    $scope.addTag = function() {
        $scope.tags.push({'title':$scope.newTag, 'done':false})
        $scope.newTag = ''
    }

    $scope.clearTag = function() {
        $scope.tags = $scope.tags.filter(function(item){
            return !item.done
        })
    }

}])

function MyCtrl($scope, Slug) {
    $scope.slugify = function(input) {
        $scope.mySlug = Slug.slugify(input);
    };
}
