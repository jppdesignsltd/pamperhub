<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes
Auth::routes();

// Page Routes
Route::get('/', ['as' => '/', 'uses' => 'PagesController@getIndex']);
Route::get('about', ['as' => 'about', 'uses' => 'PagesController@getAbout']);
Route::get('frequently-asked-questions', ['as' => 'help', 'uses' => 'PagesController@getHelp']);
Route::get('search', ['as' => 'search.index', 'uses' => 'PagesController@getSearch']);
Route::get('list-your-business', ['as' => 'list-your-business', 'uses' => 'PagesController@getListYourBusiness']);
Route::get('terms-and-conditions', ['as' => 'terms', 'uses' => 'PagesController@getTerms']);
Route::get('privacy-policy', ['as' => 'privacy', 'uses' => 'PagesController@getPrivacy']);
Route::get('cookie-policy', ['as' => 'cookie', 'uses' => 'PagesController@getCookie']);

// Admin Routes

// Admin Main Nan Routes
Route::get('admin', ['as' => 'admin', 'uses' => 'AdminController@getIndex']);
Route::get('admin/services', ['as' => 'admin.services', 'uses' => 'ServiceController@index']);
Route::get('admin/treatments', ['as' => 'admin.treatments', 'uses' => 'TreatmentController@index']);
Route::get('admin/tags', ['as' => 'admin.tags', 'uses' => 'TagController@index']);
Route::get('admin/appointments', ['as' => 'admin.appointments', 'uses' => 'AppointmentController@index']);
Route::get('admin/settings', ['as' => 'admin.settings', 'uses' => 'SettingController@index']);

// Admin User Routes
Route::get('admin/users', ['as' => 'admin.users', 'uses' => 'UserController@index']);
Route::get('admin/users/edit/{id}', ['as' => 'admin.users.edit', 'uses' => 'UserController@edit']);
Route::get('admin/user/create', ['as' => 'admin.users.create', 'uses' => 'UserController@create']);
Route::post('admin/user/create', ['as' => 'user.store', 'uses' => 'UserController@store']);
Route::put('user/{id}', ['as' => 'user.update', 'uses' => 'UserController@update']);

// Admin Supplier Routes
Route::get('admin/suppliers', ['as' => 'admin.suppliers', 'uses' => 'AdminController@getSuppliers']);
Route::get('admin/suppliers/edit/{id}', ['as' => 'admin.suppliers.edit', 'uses' => 'AdminController@getSupplierEdit']);
Route::get('admin/suppliers/show/{id}', ['as' => 'admin.suppliers.show', 'uses' => 'AdminController@getSupplierShow']);

// Admin Customer Routes
Route::get('admin/customers', ['as' => 'admin.customers', 'uses' => 'AdminController@getCustomers']);
Route::get('admin/customers/edit/{id}', ['as' => 'admin.customers.edit', 'uses' => 'AdminController@getCustomerEdit']);
Route::get('admin/customers/show/{id}', ['as' => 'admin.customers.show', 'uses' => 'AdminController@getCustomerShow']);

// Admin Appointments Routes
Route::get('admin/appointment/edit/{id}', ['as' => 'admin.appointment.edit', 'uses' => 'AppointmentController@edit']);

// Customer Routes
Route::get('customer', ['as' => 'customer.index', 'uses' => 'CustomerController@index']);
Route::get('customer/bookings', ['as' => 'customer.bookings', 'uses' => 'CustomerController@getBookings']);
Route::put('customer/{id}', ['uses' => 'CustomerController@update', 'as' => 'customer.update']);

// Booking Routes & Paypal Routes
Route::resource('booking', 'AppointmentController', ['except' => ['destroy', 'create', 'index', 'store']]);

Route::get('booking', ['as' => 'booking.create', 'uses' => 'AppointmentController@create']);
//Route::post('booking/create', ['as' => 'booking.create', 'uses' => 'AppointmentController@create']);

Route::get('booking-status', ['as' => 'booking.status','uses' => 'PayPalController@getPayWithPaypal']);

Route::post('booking', ['as' => 'addmoney.paypal','uses' => 'PayPalController@postPayWithPaypal']);
Route::get('paypal', ['as' => 'payment.status','uses' => 'PayPalController@getPaymentStatus']);

// Tag Routes
Route::resource('tag', 'TagController', ['except' => ['show', 'create', 'index']]);

// Treatment Routes
Route::resource('treatment', 'TreatmentController', ['except' => ['create', 'show', 'index']]);

// Service Routes
Route::resource('service', 'ServiceController', ['except' => ['create', 'index']]);

// Supplier Routes
Route::get('supplier', ['as' => 'supplier', 'uses' => 'SupplierController@index']);
Route::get('supplier/register', ['as' => 'supplier.register', 'uses' => 'PagesController@getSupplierRegistration']);
Route::get('supplier/availability', ['as' => 'supplier.availability', 'uses' => 'SupplierController@getAvailability']);
Route::get('supplier/holidays', ['as' => 'supplier.holiday', 'uses' => 'SupplierController@getHoliday']);

Route::get('supplier/services', ['as' => 'supplier.services', 'uses' => 'SupplierController@getServices']);
Route::get('supplier/create', ['as' => 'supplier.create', 'uses' => 'SupplierController@getCreateListing']);
Route::get('supplier/edit/{id}', ['as' => 'supplier.edit', 'uses' => 'SupplierController@getServiceEdit']);

Route::post('supplier/register', ['as' => 'supplier.register', 'uses' => 'Auth\RegisterController@register']);
Route::post('supplier/{user_id}', ['uses' => 'SupplierController@store', 'as' => 'supplier.store']);
Route::put('supplier/{id}', ['uses' => 'SupplierController@update', 'as' => 'supplier.update']);

Route::get('supplier/appointments', ['as' => 'supplier.appointments', 'uses' => 'SupplierController@getAppointments']);

Route::get('supplier/appointment/{id}', ['as' => 'supplier.appointment.show', 'uses' => 'SupplierController@getShowAppointment']);
Route::get('supplier/appointment/edit/{id}', ['as' => 'supplier.appointment.edit', 'uses' => 'SupplierController@getEditAppointment']);

// Holiday Routes
Route::resource('holiday', 'HolidayController', ['except' => ['index', 'update', 'edit', 'create', 'show']]);

// Setting Routes
Route::resource('setting', 'SettingController', ['except' => ['create', 'show', 'index', 'edit', 'store', 'destroy']]);